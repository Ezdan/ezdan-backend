﻿using ezdan_admin.ViewModels;
using ezdan_entities;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace ezdan_admin.Controllers
{
    public class PushNotificationsController : Controller
    {
        private Entities db = new Entities();


        // GET: PushNotifications
        public ActionResult Index()
        {
            var pushNotifications = db.PushNotifications;
            return View(pushNotifications);
        }

        // GET: Holidays/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Holidays/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Create(PushNotificationViewModel pnvm)
        {
            if (ModelState.IsValid)
            {
                JArray customData = new JArray();
                PushNotification pushNotification = new PushNotification();
                pushNotification.Application = "qa.ezdanrealestate.app";
                pushNotification.Title = pnvm.Title + "";
                pushNotification.Body = pnvm.Message + "";
                pushNotification.Recipient = "all";
                pushNotification.CustomData = "";
                pushNotification.ApiKey = "AAAA4shvFss:APA91bGCt3HIXZbh8oQWbMM57YnGZDZH1MkX1JFdfVp6csB0ikL5PB9vPt27q7GRW5nYfGrURPpVsN2ss2CQfTx0ZPLvm51BmI-jccOWwOLcfp9XFEnCS7Z94HI6V-LjMHx4X8K_uVhg";
                pushNotification.IsTopic = true;
                pushNotification.CreatedAt = DateTime.Now;
                pushNotification.CreatedBy = User.Identity.GetUserId();
                pushNotification.IsTopic = true;
                JObject obj = new JObject
            {
                { "recipient", pushNotification.Recipient },
                { "isTopic", pushNotification.IsTopic },
                { "title",  pushNotification.Title },
                { "body", pushNotification.Body },
                {"apiKey",  pushNotification.ApiKey},
                { "application",pushNotification.Application },
                { "customData", customData }
            };
                db.PushNotifications.Add(pushNotification);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    ezdan_logger.LogManager.Log(ezdan_logger.LogType.error, e.Message, e);
                }
                var data = JsonConvert.DeserializeObject(obj.ToString()) + "";
                var result = new WebClient().UploadString("https://cordova-plugin-fcm.appspot.com/push/freesend", "post", data);
            }
            return RedirectToAction("Index");
        }
    }
}
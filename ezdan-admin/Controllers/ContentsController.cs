﻿using System.Data;
using System.IO;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ezdan_entities;
using ezdan_admin.ViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using ezdan_logger;

namespace ezdan_admin.Controllers
{
    [Authorize]
    public class ContentsController : Controller
    {
        private Entities db;

        public ContentsController()
        {
            db = new Entities();
        }

        // GET: Contents
        public ActionResult Index(int? id)
        {
            if (id == null || id == 1 || id == 3 || id == 4 || id == 5 || id == 170) return RedirectToAction("Index", "Home");

            var content = db.Contents.Find(id);
            var contents = db.Contents.Include(c => c.Content2).Where(x => x.ParentId == id);
            ViewBag.CanDelete = false;

            if (id == 2 || id == 9 || id == 10 || id == 26 || id == 170 || id == 182)
            {
                if (id == 182 || id == 170 || id == 26 || id == 10 || id == 2)
                {
                    ViewBag.CanDelete = true;
                }
                ViewBag.CanCreate = true;
            }
            else
            {
                ViewBag.CanCreate = false;
            }

            switch(id)
            {
                case 2:
                    ViewBag.Title = "Promotions";
                    break;
                case 10:
                    ViewBag.Title = "Upcoming Events";
                    break;
                default:
                    ViewBag.Title = content.EnglishName;
                    break;
            }
            ViewBag.ContentIdTemp = content.ContentId;
            return View(contents.ToList());
        }

        // GET: Contents/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Content content = db.Contents.Find(id);
            if (content == null)
            {
                return HttpNotFound();
            }
            if (content.ParentId == 1 || content.ParentId == 3 || content.ParentId == 4 || content.ParentId == 5 || content.ParentId == 170)
            {
                return RedirectToAction("Index", "Home");
            }
            return View(content);
        }

        // GET: Contents/Create
        public ActionResult Create(int? id)
        {
            ViewBag.ParentId = new SelectList(db.Contents.Where(x => x.ContentId < 157), "ContentId", "EnglishName");
            switch (id)
            {
                //case 1:
                //    return RedirectToAction("CreateFaq", new { @id = id });
                case 2:
                    return RedirectToAction("CreateAnnouncement", new { @id = id });
                case 3:
                    return RedirectToAction("CreateAbout", new { @id = id });
                //case 4:
                //    return RedirectToAction("CreateClassified", new { @id = id });
                case 10:
                    return RedirectToAction("CreateEvent", new { @id = id });
                case 26:
                    return RedirectToAction("CreateEzdanPlus", new { @id = id });
                case 182:
                    return RedirectToAction("CreateSakinLocation", new { @id = id });
                default:
                    return RedirectToAction("Index", "Home");
            }
        }

        // GET: Contents/Create
        public ActionResult CreateFaq(int? id)
        {
            ViewBag.ParentId = new SelectList(db.Contents.Where(x => x.ContentId < 4), "ContentId", "EnglishName");
            return View();
        }

        // GET: Contents/Create
        public ActionResult CreateAnnouncement(int? id)
        {
            ViewBag.ParentId = new SelectList(db.Contents.Where(x => x.ContentId < 4), "ContentId", "EnglishName");
            return View();
        }

        // GET: Contents/Create
        public ActionResult CreateAbout(int? id)
        {
            ViewBag.ParentId = new SelectList(db.Contents.Where(x => x.ContentId < 4), "ContentId", "EnglishName");
            return View();
        }

        // GET: Contents/Create
        public ActionResult CreateClassified(int? id)
        {
            ViewBag.ParentId = new SelectList(db.Contents.Where(x => x.ContentId < 4), "ContentId", "EnglishName");
            return View();
        }

        // GET: Contents/Create
        public ActionResult CreateEzdanPlus(int? id)
        {
            ViewBag.ParentId = new SelectList(db.Contents.Where(x => x.ContentId < 27), "ContentId", "EnglishName");
            return View();
        }

        public ActionResult CreateFutureProject(int? id)
        {
            ViewBag.ParentId = new SelectList(db.Contents.Where(x => x.ContentId < 27), "ContentId", "EnglishName");
            return View();
        }

        // GET: Contents/Create
        public ActionResult CreateEvent(int? id)
        {
            ViewBag.ParentId = new SelectList(db.Contents.Where(x => x.ContentId < 27), "ContentId", "EnglishName");
            return View();
        }

        // GET: Contents/Create
        public ActionResult CreateSakinLocation(int? id)
        {
            ViewBag.ParentId = new SelectList(db.Contents.Where(x => x.ContentId < 27), "ContentId", "EnglishName");
            return View();
        }

        // POST: Contents/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateFaq(ContentFaqViewModel contentFaq)
        {
            if (ModelState.IsValid)
            {
                Content content = new Content();
                content.ParentId = 1;
                content.ContentType = "textlist";
                content.EnglishName = contentFaq.EnglishQuestion;
                content.ArabicName = contentFaq.ArabicQuestion;
                content.CreatedOn = DateTime.UtcNow;
                db.Contents.Add(content);
                ContentProperty cp = new ContentProperty();
                cp.EnglishValue = contentFaq.EnglishAnswer;
                cp.ArabicValue = contentFaq.ArabicAnswer;
                cp.Content = content;
                cp.PropertyId = 1;
                db.ContentProperties.Add(cp);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ParentId = new SelectList(db.Contents.Where(x => x.ContentId < 4), "ContentId", "EnglishName", 1);
            return View(contentFaq);
        }

        // POST: Contents/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateAnnouncement(ContentAnnounceViewModel contentAnnouncement)
        {
            if (ModelState.IsValid)
            {
                Content content = new Content();
                content.ParentId = 2;
                content.EnglishName = contentAnnouncement.EnglishTitle;
                content.ArabicName = contentAnnouncement.ArabicTitle;
                content.ContentType = "detailview";
                content.CreatedOn = DateTime.UtcNow;
                db.Contents.Add(content);

                ContentProperty image = new ContentProperty();
                var imagesDir = (ezdan_admin.Models.Enum.ContentImgsDir)content.ParentId;
                try
                {
                    HttpPostedFileBase imageFile = Request.Files[0];
                    image.EnglishValue = image.ArabicValue = SaveContentImage(imageFile, imagesDir.ToString());
                }
                catch (Exception e)
                {
                    LogManager.Log(LogType.error, e.Message, e);
                    ViewBag.imageProblem = true;
                    return View(contentAnnouncement);
                }
                image.Content = content;
                image.PropertyId = 3;

                ContentProperty thumb = new ContentProperty();
                try
                {
                    HttpPostedFileBase thumbFile = Request.Files[1];
                    thumb.EnglishValue = thumb.ArabicValue = SaveContentImage(thumbFile, imagesDir.ToString());
                }
                catch (Exception e)
                {
                    LogManager.Log(LogType.error, e.Message, e);
                    ViewBag.thumbProblem = true;
                    return View(contentAnnouncement);
                }
                thumb.Content = content;
                thumb.PropertyId = 4;

                ContentProperty details = new ContentProperty();
                details.EnglishValue = contentAnnouncement.EnglishDesc;
                details.ArabicValue = contentAnnouncement.ArabicDesc;
                details.Content = content;
                details.PropertyId = 1;

                db.ContentProperties.Add(image);
                db.ContentProperties.Add(thumb);
                db.ContentProperties.Add(details);

                try
                {
                    db.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }
                return RedirectToAction("Index", new { @id = 2 });
            }
            ViewBag.ParentId = new SelectList(db.Contents.Where(x => x.ContentId < 4), "ContentId", "EnglishName", 2);
            return View(contentAnnouncement);
        }

        // POST: Contents/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateAbout(ContentAboutViewModel contentAbout)
        {
            if (ModelState.IsValid)
            {
                Content content = new Content();
                content.ParentId = 3;
                content.EnglishName = contentAbout.EnglishTitle;
                content.ArabicName = contentAbout.ArabicTitle;
                content.ContentType = "textlist";
                content.CreatedOn = DateTime.UtcNow;
                db.Contents.Add(content);

                ContentProperty cp = new ContentProperty();
                cp.EnglishValue = contentAbout.EnglishDesc;
                cp.ArabicValue = contentAbout.ArabicDesc;
                cp.Content = content;
                cp.PropertyId = 1;
                db.ContentProperties.Add(cp);
                db.SaveChanges();
                return RedirectToAction("Index", new { @id = 3 });
            }
            ViewBag.ParentId = new SelectList(db.Contents.Where(x => x.ContentId < 4), "ContentId", "EnglishName", 3);
            return View(contentAbout);
        }

        // POST: Contents/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateClassified(ContentClassifiedViewModel contentClassified)
        {
            if (ModelState.IsValid)
            {
                Content content = new Content();
                content.ParentId = 4;
                content.EnglishName = contentClassified.EnglishCategory;
                content.ArabicName = contentClassified.ArabicCategory;
                content.ContentType = "classifiedlist"; 
                content.CreatedOn = DateTime.UtcNow;
                db.Contents.Add(content);
                db.SaveChanges();
                return RedirectToAction("Index", new { @id = 4 });
            }
            ViewBag.ParentId = new SelectList(db.Contents.Where(x => x.ContentId < 4), "ContentId", "EnglishName", 4);
            return View(contentClassified);
        }

        // POST: Contents/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateEzdanPlus(ContentEzdanPlusViewModel contentEzdanPlus)
        {
            if (ModelState.IsValid)
            {
                Content content = new Content();
                content.ParentId = 26;
                content.EnglishName = contentEzdanPlus.EnglishTitle;
                content.ArabicName = contentEzdanPlus.ArabicTitle;
                content.ContentType = "-";
                content.CreatedOn = DateTime.UtcNow;
                db.Contents.Add(content);

                ContentProperty image = new ContentProperty();
                var imagesDir = (ezdan_admin.Models.Enum.ContentImgsDir)content.ParentId;
                try
                {
                    HttpPostedFileBase imageFile = Request.Files[0];
                    image.EnglishValue = image.ArabicValue = SaveContentImage(imageFile, imagesDir.ToString());
                }
                catch (Exception e)
                {
                    LogManager.Log(LogType.error, e.Message, e);
                    ViewBag.imageProblem = true;
                    return View(contentEzdanPlus);
                }
                //image.EnglishValue = contentEzdanPlus.ImageUrl;
                //image.ArabicValue = contentEzdanPlus.ImageUrl;
                image.Content = content;
                image.PropertyId = 3;

                ContentProperty details = new ContentProperty();
                details.EnglishValue = contentEzdanPlus.EnglishDesc;
                details.ArabicValue = contentEzdanPlus.ArabicDesc;
                details.Content = content;
                details.PropertyId = 1;

                db.ContentProperties.Add(image);
                db.ContentProperties.Add(details);

                db.SaveChanges();
                return RedirectToAction("Index", new { @id = 26 });
            }
            ViewBag.ParentId = new SelectList(db.Contents.Where(x => x.ContentId < 157), "ContentId", "EnglishName", 26);
            return View(contentEzdanPlus);
        }

        // POST: Contents/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateFutureProject(ContentFutureProjectViewModel contentFutureProject)
        {
            if (ModelState.IsValid)
            {
                Content content = new Content();
                content.ParentId = 170;
                content.EnglishName = contentFutureProject.EnglishTitle;
                content.ArabicName = contentFutureProject.ArabicTitle;
                content.ContentType = "detailview";
                content.CreatedOn = DateTime.UtcNow;
                db.Contents.Add(content);

                ContentProperty image = new ContentProperty();
                try
                {
                    HttpPostedFileBase imageFile = Request.Files[0];
                    image.EnglishValue = image.ArabicValue = SaveContentImage(imageFile, "future projects");
                }
                catch (Exception e)
                {
                    LogManager.Log(LogType.error, e.Message, e);
                    ViewBag.imageProblem = true;
                    return View(contentFutureProject);
                }
                image.Content = content;
                image.PropertyId = 3;

                ContentProperty thumb = new ContentProperty();
                try
                {
                    HttpPostedFileBase thumbFile = Request.Files[1];
                    thumb.EnglishValue = thumb.ArabicValue = SaveContentImage(thumbFile, "future projects");
                }
                catch (Exception e)
                {
                    LogManager.Log(LogType.error, e.Message, e);
                    ViewBag.thumbProblem = true;
                    return View(contentFutureProject);
                }
                thumb.Content = content;
                thumb.PropertyId = 4;

                ContentProperty details = new ContentProperty();
                details.EnglishValue = contentFutureProject.EnglishDesc;
                details.ArabicValue = contentFutureProject.ArabicDesc;
                details.Content = content;
                details.PropertyId = 1;

                db.ContentProperties.Add(image);
                db.ContentProperties.Add(thumb);
                db.ContentProperties.Add(details);

                db.SaveChanges();
                return RedirectToAction("Index", new { @id = 170 });
            }
            ViewBag.ParentId = new SelectList(db.Contents.Where(x => x.ContentId < 157), "ContentId", "EnglishName", 170);
            return View(contentFutureProject);
        }

        // POST: Contents/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateEvent(ContentEventViewModel contentEvent)
        {
            if (ModelState.IsValid)
            {
                Content content = new Content();
                content.ParentId = 10;
                content.EnglishName = contentEvent.EnglishTitle;
                content.ArabicName = contentEvent.ArabicTitle;
                content.ContentType = "detailview";
                content.CreatedOn = DateTime.UtcNow;
                db.Contents.Add(content);

                ContentProperty image = new ContentProperty();
                var imagesDir = (ezdan_admin.Models.Enum.ContentImgsDir)content.ParentId;
                try
                {
                    HttpPostedFileBase imageFile = Request.Files[0];
                    image.EnglishValue = image.ArabicValue = SaveContentImage(imageFile, imagesDir.ToString());
                }
                catch (Exception e)
                {
                    LogManager.Log(LogType.error, e.Message, e);
                    ViewBag.imageProblem = true;
                    return View(contentEvent);
                }
                image.Content = content;
                image.PropertyId = 3;

                ContentProperty thumb = new ContentProperty();
                try
                {
                    HttpPostedFileBase thumbFile = Request.Files[1];
                    thumb.EnglishValue = thumb.ArabicValue = SaveContentImage(thumbFile, imagesDir.ToString());
                }
                catch (Exception e)
                {
                    LogManager.Log(LogType.error, e.Message, e);
                    ViewBag.thumbProblem = true;
                    return View(contentEvent);
                }
                thumb.Content = content;
                thumb.PropertyId = 4;

                ContentProperty venue = new ContentProperty();
                venue.EnglishValue = contentEvent.EnglishVenue;
                venue.ArabicValue = contentEvent.ArabicVenue;
                venue.Content = content;
                venue.PropertyId = 16;

                ContentProperty eventStartingDate = new ContentProperty();
                eventStartingDate.EnglishValue = eventStartingDate.ArabicValue = contentEvent.EventStartingDate.ToString("yyyy/MM/dd H:mm:ss");
                eventStartingDate.Content = content;
                eventStartingDate.PropertyId = 26;

                ContentProperty eventEndingDate = new ContentProperty();
                eventEndingDate.EnglishValue = eventEndingDate.ArabicValue = contentEvent.EventEndingDate.ToString("yyyy/MM/dd H:mm:ss");
                eventEndingDate.Content = content;
                eventEndingDate.PropertyId = 27;

                ContentProperty details = new ContentProperty();
                details.EnglishValue = contentEvent.EnglishDesc;
                details.ArabicValue = contentEvent.ArabicDesc;
                details.Content = content;
                details.PropertyId = 1;

                db.ContentProperties.Add(image);
                db.ContentProperties.Add(thumb);
                db.ContentProperties.Add(venue);
                db.ContentProperties.Add(eventStartingDate);
                db.ContentProperties.Add(eventEndingDate);
                db.ContentProperties.Add(details);

                db.SaveChanges();
                return RedirectToAction("Index", new { @id = 10 });
            }
            ViewBag.ParentId = new SelectList(db.Contents.Where(x => x.ContentId < 157), "ContentId", "EnglishName", 10);
            return View(contentEvent);
        }

        // POST: Contents/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateSakinLocation(ContentSakinLocationViewModel contentLocation)
        {
            if (ModelState.IsValid)
            {
                Content content = new Content();
                content.ParentId = 182;
                content.EnglishName = contentLocation.EnglishTitle;
                content.ArabicName = contentLocation.ArabicTitle;
                content.ContentType = "map";
                content.CreatedOn = DateTime.UtcNow;
                db.Contents.Add(content);

                ContentProperty latitude = new ContentProperty();
                latitude.Content = content;
                latitude.EnglishValue = latitude.ArabicValue = contentLocation.Latitude;
                latitude.PropertyId = 13;

                ContentProperty longitude = new ContentProperty();
                longitude.Content = content;
                longitude.EnglishValue = longitude.ArabicValue = contentLocation.Longitude;
                longitude.PropertyId = 14;

                db.ContentProperties.Add(latitude);
                db.ContentProperties.Add(longitude);

                db.SaveChanges();
                return RedirectToAction("Index", new { @id = 182 });
            }
            ViewBag.ParentId = new SelectList(db.Contents.Where(x => x.ContentId < 157), "ContentId", "EnglishName", 10);
            return View(contentLocation);
        }

        public string SaveContentImage(HttpPostedFileBase file, string dir)
        {
            try
            {
                var virtuaContentDirName = "~/Assets/Images/" + dir;
                string fullDirPath = HttpContext.Server.MapPath(virtuaContentDirName);
                string fullImgPath = HttpContext.Server.MapPath(Path.Combine(virtuaContentDirName, file.FileName));
                var contentDir = System.IO.Directory.CreateDirectory(fullDirPath);
                
                //if (System.IO.File.Exists(fullImgPath))
                //{
                //    System.GC.Collect();
                //    System.GC.WaitForPendingFinalizers();
                //    System.IO.File.SetAttributes(fullImgPath, FileAttributes.Normal);
                //    System.IO.File.Delete(fullImgPath);
                //}
                FileStream fileToSave = new FileStream(fullImgPath, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);
                Stream stream = file.InputStream;
                stream.CopyTo(fileToSave);
                stream.Close();
                string baseURL = string.Format("{0}{1}/", Request.Url.GetLeftPart(UriPartial.Authority), Request.ApplicationPath.TrimEnd('/'));
                return baseURL + "Assets/Images/" + dir + "/" + file.FileName;
            }
            catch (Exception e)
            {
                LogManager.Log(LogType.error, e.Message, e);
                throw e;
            }
        }

        //public string SaveContentImage(HttpPostedFileBase file, string dir)
        //{
        //    string virtuaContentDirName;
        //    string fullDirPath;
        //    string fullImgPath;
        //    try
        //    {
        //        virtuaContentDirName = "~/Assets/Images/" + dir;
        //        fullDirPath = HttpContext.Server.MapPath(virtuaContentDirName);
        //    }
        //    catch (Exception e)
        //    {
        //        return "full directory path problem: " + e.Message.ToString();
        //    }
        //    try
        //    {
        //        fullImgPath = HttpContext.Server.MapPath(Path.Combine(virtuaContentDirName, file.FileName));
        //    }
        //    catch (Exception e)
        //    {
        //        return "full image path problem: " + e.Message.ToString();
        //    }
        //    try
        //    {
        //        var contentDir = System.IO.Directory.CreateDirectory(fullDirPath);
        //    }
        //    catch (Exception e)
        //    {
        //        return "directory creation problem: " + e.Message.ToString();
        //    }
        //    try
        //    {
        //        if (System.IO.File.Exists(fullImgPath))
        //        {
        //            System.GC.Collect();
        //            System.GC.WaitForPendingFinalizers();
        //            try
        //            {
        //                System.IO.File.SetAttributes(fullImgPath, FileAttributes.Normal);
        //                System.IO.File.Delete(fullImgPath);
        //            }
        //            catch (Exception e)
        //            {
        //                return "file deletion problem: " + e.Message.ToString();
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        return "file checking problem: " + e.Message.ToString();
        //    }
        //    try
        //    {
        //        FileStream fileToSave = new FileStream(fullImgPath, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);
        //        Stream stream = file.InputStream;
        //        stream.CopyTo(fileToSave);
        //        stream.Close();
        //    }
        //    catch (Exception e)
        //    {
        //        return "file creation problem: " + e.Message.ToString();
        //    }
        //    try
        //    {
        //        string baseURL = string.Format("{0}{1}/", Request.Url.GetLeftPart(UriPartial.Authority), Request.ApplicationPath.TrimEnd('/'));
        //        return baseURL + "Assets/Images/" + dir + "/" + file.FileName;
        //    }
        //    catch (Exception e)
        //    {
        //        return "response problem: " + e.Message.ToString();
        //    }
        //    //catch (Exception e)
        //    //{
        //    //    LogManager.Log(LogType.error, e.Message, e);
        //    //    throw e;
        //    //}
        //}

        // GET: Contents/Edit/5
        public ActionResult Edit(int? id)
        {
            ViewBag.ParentId = new SelectList(db.Contents.Where(x => x.ContentId < 4), "ContentId", "EnglishName");
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Content content = db.Contents.Find(id);
            if (content == null)
                return HttpNotFound();
            if(content.ParentId == 1 || content.ParentId == 3 || content.ParentId == 4 || content.ParentId == 170)
            {
                return RedirectToAction("Index", "Home");
            }
            var faqAnswer = content.ContentProperties.ToList();
            var model = new ContentEditViewModel()
            {
                Content = content,
                ContentId = content.ContentId,
                ContentEnglishName = content.EnglishName,
                ContentArabicName = content.ArabicName,
                ContentPropertiesList = faqAnswer.Select(x => new ContentProperty()
                {
                    ContentPropertyId = x.ContentPropertyId,
                    Content = x.Content,
                    PropertyId = x.PropertyId,
                    Property = x.Property,
                    ContentId = x.ContentId,
                    EnglishValue = x.EnglishValue,
                    ArabicValue = x.ArabicValue
                }).ToList()
            };
            if(model.ContentId == 6)
            {
                List<int> contactUsPropIds = new List<int>() { 10, 17, 37, 18, 13, 14, 19, 20, 21, 22, 23, 24, 25, 34, 39, 35, 36 };
                List<ContentProperty> orderedProps = model.ContentPropertiesList.OrderBy(cpl => contactUsPropIds.IndexOf(cpl.PropertyId)).ToList();
                model.ContentPropertiesList = orderedProps;
            }
            switch (content.ParentId)
            {
                case 2:
                    ViewBag.Title = "Promotion";
                    break;
                case 10:
                    ViewBag.Title = "Upcoming Event";
                    break;
            }
            ViewBag.nonMLProperties = new int[] { 10, 17, 19, 20, 21, 22, 23, 24, 25, 37, 39 };
            return View(model);
        }

        // POST: Contents/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ContentEditViewModel model)
        {
            if (ModelState.IsValid)
            {
                Content content = db.Contents.Find(model.ContentId);
                content.EnglishName = model.ContentEnglishName;
                content.ArabicName = model.ContentArabicName;
                if (model.ContentPropertiesList != null)
                {
                    foreach (ContentProperty cp in model.ContentPropertiesList)
                    {
                        if((content.ParentId == 26 || content.ParentId == 2 || content.ParentId == 10 || content.ParentId == 170 || content.ContentId == 186) 
                            && (cp.PropertyId == 3 || cp.PropertyId == 4))
                        {
                            int fileIndex = cp.PropertyId == 3 ? 0 : 1;
                            try
                            {
                                HttpPostedFileBase imageFile = Request.Files[fileIndex];
                            }
                            catch (Exception e)
                            {
                                LogManager.Log(LogType.error, e.Message, e);
                                continue;
                            }
                            try
                            {
                                if (Request.Files[fileIndex] != null && !string.IsNullOrEmpty(Request.Files[fileIndex].FileName))
                                {
                                    var imgsDir = (ezdan_admin.Models.Enum.ContentImgsDir)content.ParentId;
                                    cp.EnglishValue = cp.ArabicValue = SaveContentImage(Request.Files[fileIndex], imgsDir.ToString());
                                }
                            }
                            catch (Exception e)
                            {
                                LogManager.Log(LogType.error, e.Message, e);
                                ViewBag.imageProblem = true;
                                ViewBag.ParentId = new SelectList(db.Contents, "ContentId", "EnglishName", model.ContentId);
                                return RedirectToAction("Edit", new { @id = model.ContentPropertiesList.FirstOrDefault().ContentId });
                            }
                            finally
                            {
                                db.Entry(cp).State = EntityState.Modified;
                            }
                        }
                        if (cp.PropertyId == 29 || cp.PropertyId == 30)
                        {
                            DateTime sakinOpeningTime;
                            DateTime.TryParse(cp.EnglishValue, out sakinOpeningTime);
                            if (sakinOpeningTime != null)
                            {
                                CultureInfo arEg = new CultureInfo("ar-EG");
                                cp.ArabicValue = sakinOpeningTime.ToString("hh:mm tt", arEg);
                            }
                        }
                        if (cp.PropertyId == 32 || cp.PropertyId == 33)
                        {
                            CultureInfo arEg = new CultureInfo("ar-EG");
                            cp.ArabicValue = arEg.DateTimeFormat.GetDayName((DayOfWeek)Enum.Parse(typeof(DayOfWeek), cp.EnglishValue, true));
                        }
                        var nonMLProperties = new int[] { 5, 10, 13, 14, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 37, 39 };
                        if (Array.IndexOf(nonMLProperties, cp.PropertyId) > -1)
                        {
                            cp.ArabicValue = cp.EnglishValue;
                        }
                        db.Entry(cp).State = EntityState.Modified;
                    }
                }
                try
                {
                    db.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    LogManager.Log(LogType.error, e.Message, e);
                    throw e;
                }
            }
            ViewBag.ParentId = new SelectList(db.Contents, "ContentId", "EnglishName", model.ContentId);
            if(Request.Params["parentId"]!=null)
                return RedirectToAction("Index", new { @id = Request.Params["parentId"] });
            else
            {
                return RedirectToAction("Edit", new { @id = model.ContentPropertiesList.FirstOrDefault().ContentId });
            }
        }

        // GET: Contents/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Content content = db.Contents.Find(id);
            if (content == null)
            {
                return HttpNotFound();
            }
            return View(content);
        }

        // POST: Contents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Content content = db.Contents.Find(id);
            var parentId = content.ParentId;
            List<ContentProperty> contentProps = db.ContentProperties.Where(x => x.ContentId == content.ContentId).ToList();
            db.ContentProperties.RemoveRange(contentProps);
            db.Contents.Remove(content);
            db.SaveChanges();
            return RedirectToAction("Index", new { @id = parentId });
            //return Redirect(Request.UrlReferrer.ToString());
        }

        public ActionResult ToggleBlock(int id)
        {
            Content content = db.Contents.Find(id);
            content.Blocked = !content.Blocked;
            db.SaveChanges();
            return RedirectToAction("Index", new { @id = content.ParentId });
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

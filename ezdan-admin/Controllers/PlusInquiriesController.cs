﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ezdan_entities;

namespace ezdan_admin.Controllers
{
    [Authorize]
    public class PlusInquiriesController : Controller
    {
        private Entities db = new Entities();

        // GET: PlusInquiries
        public ActionResult Index()
        {
            var plusInquiries = db.PlusInquiries.Include(p => p.AspNetUser);
            return View(plusInquiries.Where(x=>x.IsReviewed != true).ToList());
        }

        // GET: PlusInquiries
        public ActionResult IndexReviewed()
        {
            var plusInquiries = db.PlusInquiries.Include(p => p.AspNetUser);
            return View(plusInquiries.Where(x => x.IsReviewed == true).ToList());
        }
        // GET: PlusInquiries/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PlusInquiry plusInquiry = db.PlusInquiries.Find(id);
            if (plusInquiry == null)
            {
                return HttpNotFound();
            }
            return View(plusInquiry);
        }

        // GET: PlusInquiries/Create
        public ActionResult Create()
        {
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "AccountId");
            return View();
        }

        // POST: PlusInquiries/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,UserId,Name,Email,ContactNumber,Details")] PlusInquiry plusInquiry)
        {
            if (ModelState.IsValid)
            {
                db.PlusInquiries.Add(plusInquiry);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "AccountId", plusInquiry.UserId);
            return View(plusInquiry);
        }

        // GET: PlusInquiries/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PlusInquiry plusInquiry = db.PlusInquiries.Find(id);
            if (plusInquiry == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "AccountId", plusInquiry.UserId);
            return View(plusInquiry);
        }

        // POST: PlusInquiries/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,UserId,Name,Email,ContactNumber,Details")] PlusInquiry plusInquiry)
        {
            if (ModelState.IsValid)
            {
                db.Entry(plusInquiry).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "AccountId", plusInquiry.UserId);
            return View(plusInquiry);
        }

        // GET: PlusInquiries/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PlusInquiry plusInquiry = db.PlusInquiries.Find(id);
            if (plusInquiry == null)
            {
                return HttpNotFound();
            }
            return View(plusInquiry);
        }

        // POST: PlusInquiries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PlusInquiry plusInquiry = db.PlusInquiries.Find(id);
            db.PlusInquiries.Remove(plusInquiry);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: PlusInquiries/Create
        public ActionResult SetReviewed(int? id)
        {
            PlusInquiry plusInquiry = db.PlusInquiries.Find(id);
            plusInquiry.IsReviewed = true;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: PlusInquiries/Create
        public ActionResult SetUnReviewed(int? id)
        {
            PlusInquiry plusInquiry = db.PlusInquiries.Find(id);
            plusInquiry.IsReviewed = false;
            db.SaveChanges();
            return RedirectToAction("IndexReviewed");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

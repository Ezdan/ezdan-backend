﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ezdan_admin.Models;
using ezdan_entities;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Net.Mail;
using System.Configuration;
using System.Globalization;
using ezdan_business.Helper;

namespace ezdan_admin.Controllers
{
    [Authorize]
    public class AppointmentController : Controller
    {
        private Entities db = new Entities();

        // GET: Appointment
        public ActionResult Index()
        {

            return View(db.Appointments.ToList());
        }

        #region Get All Appointments
        public List<Appointment> LoadAllAppointmentsInDateRange(DateTime start, DateTime end)
        {
            var rslt = db.Appointments.Where(s => s.StartDate >=
                start && s.EndDate <= end);
            List<Appointment> result = new List<Appointment>();
            foreach (var item in rslt)
            {
                Appointment rec = new Appointment();
                var startingDate = Convert.ToDateTime(item.StartDate?.ToString("s", CultureInfo.InvariantCulture));
                var startingDateOffset = TimeZone.CurrentTimeZone.GetUtcOffset(startingDate).Hours;
                var endingDate = Convert.ToDateTime(item.EndDate?.ToString("s", CultureInfo.InvariantCulture));
                var endingDateOffset = TimeZone.CurrentTimeZone.GetUtcOffset(endingDate).Hours;
                rec.AppointmentId = item.AppointmentId;
                rec.StartDate = Convert.ToDateTime(item.StartDate?.ToString("s", CultureInfo.InvariantCulture)).AddHours(startingDateOffset);
                rec.EndDate = Convert.ToDateTime(item.EndDate?.ToString("s", CultureInfo.InvariantCulture)).AddHours(endingDateOffset);
                rec.Title = item.Title;
                rec.StatusColor = item.StatusColor;
                rec.Email = item.Email;
                rec.Mobile = item.Mobile;
                result.Add(rec);
            }
            return result;

        }
        public JsonResult GetDiaryEvents(DateTime start, DateTime end)
        {
            var ApptListForDate = LoadAllAppointmentsInDateRange(start, end);
            var eventList = from e in ApptListForDate
                            select new
                            {
                                id = e.AppointmentId,
                                title = e.Title,
                                start = e.StartDate,
                                end = e.EndDate,
                                color = e.StatusColor,
                                allDay = false,
                                name = db.Appointments.Where(app => app.AppointmentId == e.AppointmentId).Select(app => app.AspNetUser.FullName).First(),
                                email = e.Email,
                                mobile=e.Mobile,
                                constraint = "businessHours"
                            };
            var rows = eventList.ToArray();
            return Json(rows, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Save Calendar Settings
        [HttpPost]
        public JsonResult SaveCalendarSettings(string first, string second, string startHour, string endHour, string breakFrom, string breakTo, string holidayFrom, string holidayTo)
        {
            var setting = new AppointmentsSetting();
            if (db.AppointmentsSettings.FirstOrDefault() == null)
            {
                setting.FirstWeekEnd = first;
                setting.SecondWeekEnd = second;
                setting.StartWorkingHours = startHour;
                setting.EndWorkingHours = endHour;
                setting.BreakHour = breakFrom;
                setting.HolidayFrom = holidayFrom;
                setting.HolidayTo = holidayTo;
                setting.BreakTo = breakTo;
                db.AppointmentsSettings.Add(setting);
            }
            else
            {
                setting = db.AppointmentsSettings.FirstOrDefault();
                setting.FirstWeekEnd = first;
                setting.SecondWeekEnd = second;
                setting.StartWorkingHours = startHour;
                setting.EndWorkingHours = endHour;
                setting.BreakHour = breakFrom;
                setting.HolidayFrom = holidayFrom;
                setting.HolidayTo = holidayTo;
                setting.BreakTo = breakTo;

            }
            JObject response = new JObject();
            try
            {
                db.SaveChanges();
                response.Add("Message", true);
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                response.Add("Message", false);
                return Json(response, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Confirm an appointment
        [HttpPost]
        [Route("api/Appointments/ConfirmAppointment")]
        public JObject ConfirmAppointment(string id, string name, string email)
        {
            string Subject = "Confirm Appointment";
            int AppointmentId = int.Parse(id);
            Appointment app = db.Appointments.Where(x => x.AppointmentId == AppointmentId).SingleOrDefault();
            JObject result = new JObject();
            try
            {
                app.StatusText = "Confirmed";
                app.StatusColor = "green";
                db.SaveChanges();
                MailHelper.SendEmail(new ezdan_entities.Models.EmailNotification { EmailTo = app.Email, Subject = Subject, Body = "Your appointment has been confirmed" });

                //string filename = System.Web.Hosting.HostingEnvironment.MapPath("~/Views/MailTemplates/test.html");
                //string mailbody = System.IO.File.ReadAllText(filename);
                //mailbody = mailbody.Replace("##NAME##", name);
                //SmtpClient SmtpServer = new SmtpClient(ConfigurationManager.AppSettings["Host"], int.Parse(ConfigurationManager.AppSettings["Port"]));
                //SmtpServer.UseDefaultCredentials = false;
                //SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                //SmtpServer.EnableSsl = true;
                //SmtpServer.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["UserName"], ConfigurationManager.AppSettings["Password"]);

                //MailMessage Mail = new MailMessage();
                //Mail.From = new MailAddress(ConfigurationManager.AppSettings["UserName"]);
                //Mail.To.Add(email);
                //Mail.IsBodyHtml = true;
                //Mail.Subject = Subject;
                //Mail.Body = mailbody;
                //SmtpServer.Send(Mail);
                result.Add("response", true);
                return result;
            }
            catch (Exception ex)
            {
                result.Add("response", false);
                return result;
            }
        }
        #endregion

        #region Cancel an appointment
        [HttpPost]
        [Route("/Appointment/CancelAppointment/")]
        public JObject CancelAppointment(string id, string name, string email)
        {
            string Subject = "Cancel Appointment";
            int AppointmentId = int.Parse(id);
            Appointment app = db.Appointments.Where(x => x.AppointmentId == AppointmentId).SingleOrDefault();
            JObject result = new JObject();
            try
            {
                db.Appointments.Remove(app);
                db.SaveChanges();
                MailHelper.SendEmail(new ezdan_entities.Models.EmailNotification { EmailTo = app.Email, Subject = Subject, Body = "Your appointment has been cancelled" });
                //string filename = System.Web.Hosting.HostingEnvironment.MapPath("~/Views/MailTemplates/test.html");
                //string mailbody = System.IO.File.ReadAllText(filename);
                //mailbody = mailbody.Replace("##NAME##", name);
                //SmtpClient SmtpServer = new SmtpClient(ConfigurationManager.AppSettings["Host"], int.Parse(ConfigurationManager.AppSettings["Port"]));
                //SmtpServer.UseDefaultCredentials = false;
                //SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                //SmtpServer.EnableSsl = true;
                //SmtpServer.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["UserName"], ConfigurationManager.AppSettings["Password"]);

                //MailMessage Mail = new MailMessage();
                //Mail.From = new MailAddress(ConfigurationManager.AppSettings["UserName"]);
                //Mail.To.Add(email);
                //Mail.IsBodyHtml = true;
                //Mail.Subject = Subject;
                //Mail.Body = mailbody;
                //SmtpServer.Send(Mail);
                result.Add("response", true);
                return result;
            }
            catch (Exception ex)
            {
                result.Add("response", false);
                return result;
            }
        }
        #endregion

        #region Reschedule an appointment
        [HttpPost]
        [Route("api/Appointments/EditAppointment")]
        public JObject EditAppointment(string id, string name, string email, string date)
        {
            string Subject = "";
            int AppointmentId = int.Parse(id);
            Appointment app = db.Appointments.Where(x => x.AppointmentId == AppointmentId).SingleOrDefault();
            JObject result = new JObject();
            try
            {
                app.StartDate = Convert.ToDateTime(date);
                app.EndDate = Convert.ToDateTime(date).AddHours(1);
                db.SaveChanges();
                MailHelper.SendEmail(new ezdan_entities.Models.EmailNotification { EmailTo = app.Email, Subject = Subject, Body = "Your appointment has been rescheduled to: "+date });
                //string filename = System.Web.Hosting.HostingEnvironment.MapPath("~/Views/MailTemplates/test.html");
                //string mailbody = System.IO.File.ReadAllText(filename);
                //mailbody = mailbody.Replace("##NAME##", name);
                //SmtpClient SmtpServer = new SmtpClient(ConfigurationManager.AppSettings["Host"], int.Parse(ConfigurationManager.AppSettings["Port"]));
                //SmtpServer.UseDefaultCredentials = false;
                //SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                //SmtpServer.EnableSsl = true;
                //SmtpServer.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["UserName"], ConfigurationManager.AppSettings["Password"]);

                //MailMessage Mail = new MailMessage();
                //Mail.From = new MailAddress(ConfigurationManager.AppSettings["UserName"]);
                //Mail.To.Add(email);
                //Mail.IsBodyHtml = true;
                //Mail.Subject = Subject;
                //Mail.Body = mailbody;
                //SmtpServer.Send(Mail);
                result.Add("response", true);
                return result;
            }
            catch (Exception ex)
            {
                result.Add("response", false);
                return result;
            }
        }
        #endregion

        #region Get Weekends and business hours
        public JsonResult getWeekEnds()
        {
            string first = db.AppointmentsSettings.FirstOrDefault().FirstWeekEnd;
            string second = db.AppointmentsSettings.FirstOrDefault().SecondWeekEnd;
            List<int> weekEnds = new List<int>();
            weekEnds.Add(int.Parse(first));
            weekEnds.Add(int.Parse(second));
            return Json(weekEnds, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getBusinessHours()
        {
            var setting = db.AppointmentsSettings.FirstOrDefault();
            string start = setting.StartWorkingHours;
            string end = setting.EndWorkingHours;
            string breakFrom = setting.BreakHour;
            string breakTo = setting.BreakTo;
            string holidayFrom = setting.HolidayFrom;
            string holidayTo = setting.HolidayTo;
            List<string> hours = new List<string>();
            hours.Add(start);
            hours.Add(end);
            hours.Add(breakFrom);
            hours.Add(breakTo);
            hours.Add(holidayFrom);
            hours.Add(holidayTo);
            return Json(hours, JsonRequestBehavior.AllowGet);
        }
        #endregion

       
    }
}

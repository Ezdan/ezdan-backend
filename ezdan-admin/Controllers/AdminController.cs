﻿using ezdan_entities;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;

using ezdan_admin.Models;


namespace ezdan_admin.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {

        private Entities db;
        public AdminController()
        {
            db = new Entities();
        }
        public ActionResult Index()
        {
            string userId = User.Identity.GetUserId();
            List<AdminUser> lstAdminUsers = new List<AdminUser>();
            lstAdminUsers = db.AdminUsers.Where(u=>u.UserId != userId).ToList();
            return View(lstAdminUsers);
        }
        public ActionResult Create()
        {
            return View();
        }
        public ActionResult Deactivate(string Id)
        {
            var admin = db.AdminUsers.Where(a => a.UserId == Id).FirstOrDefault();
            admin.IsActive = false;

            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public ActionResult Activate(string Id)
        {
            var admin = db.AdminUsers.Where(a => a.UserId == Id).FirstOrDefault();
            admin.IsActive = true;

            db.SaveChanges();
            return RedirectToAction("Index");
        }
        [System.Web.Mvc.HttpPost]
        [System.Web.Mvc.Authorize]
        public async Task<ActionResult> Create(CreateAdminViewModel vm)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = vm.UserName, Email = vm.Email,PhoneNumber = vm.PhoneNumber , IsActive = true, IsSuperAdmin = false };
                var result = await UserManager.CreateAsync(user, vm.Password);
                if (result.Succeeded)
                {

                    return RedirectToAction("index");
                }
                AddErrors(result);
            }
            return View();
            
        }
        public ActionResult Details(string Id)
        {
            var AdminUserVm = db.AdminUsers.Where(a => a.UserId == Id).FirstOrDefault();
            return View(AdminUserVm);
        }
        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
    }
}

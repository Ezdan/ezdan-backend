﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Globalization;
using System.Data.Entity.Design.PluralizationServices;
using ezdan_entities;

namespace ezdan_admin.Controllers
{
    [Authorize]
    public class FeedbacksController : Controller 
    {
        private Entities db = new Entities();

        // GET: Feedbacks
        public ActionResult Index(int? id)
        {
            if (id == null) return RedirectToAction("Index", "Home");
            var feedbacks = db.Feedbacks.Where(fbs => fbs.Type == id).Include(fbs => fbs.AspNetUser);
            PluralizationService ps = PluralizationService.CreateService(CultureInfo.GetCultureInfo("en-us"));
            var typeKey = (ezdan_admin.Models.Enum.SuggestionTypes)id;
            int invalidTypeKey;
            if(int.TryParse(typeKey.ToString(), out invalidTypeKey))
            {
                ViewBag.type = null;
            }
            else
            {
                ViewBag.type = ps.Pluralize(typeKey.ToString());
            }
            return View(feedbacks.OrderByDescending(f => f.id).ToList());
        }

        // GET: Feedbacks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Feedback feedback = db.Feedbacks.Find(id);
            if (feedback == null)
            {
                return HttpNotFound();
            }
            return View(feedback);
        }

        // GET: Feedbacks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Feedback feedback = db.Feedbacks.Find(id);
            if (feedback == null)
            {
                return HttpNotFound();
            }
            return View(feedback);
        }

        // POST: Feedbacks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Feedback feedback = db.Feedbacks.Find(id);
            db.Feedbacks.Remove(feedback);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

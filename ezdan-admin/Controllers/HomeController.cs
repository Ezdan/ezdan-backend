﻿using System;
using System.IO;
using System.Web;
using System.Web.Mvc;
using ezdan_logger;

namespace ezdan_admin.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        [Authorize]
        public ActionResult Index()
        {
            return RedirectToAction("Index", "AspNetUsers");
            //return View();
        }


        [HttpPost]
        public ActionResult Index(HttpPostedFileBase file, string filename)
        {
            if (file != null && file.ContentLength > 0)
            {
                try
                {
                    var x = SaveHomeImages(file, filename);
                    if(x=="OK")
                    ViewBag.Message = "File uploaded successfully";
                    else
                    ViewBag.Message = x;
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "ERROR:" + ex.Message.ToString() + "MARWAN";
                }
            }
            else
            {
                ViewBag.Message = "You have not specified a file.";
            }
            return RedirectToAction("Index");
            //return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        [HttpGet]

        public ActionResult UploadFile()
        {
            return View();
        }

        public string SaveHomeImages(HttpPostedFileBase file, string filename)
        {
            try
            {
                string fullpath = HttpContext.Server.MapPath("~/" + "Assets/Images/home" + "/" + filename);
                if (System.IO.File.Exists(fullpath))
                {
                    System.GC.Collect();
                    System.GC.WaitForPendingFinalizers();
                    System.IO.File.Delete(fullpath);
                }
                FileStream fileToSave = new FileStream(fullpath, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);
                Stream stream = file.InputStream;
                stream.CopyTo(fileToSave);
                stream.Close();
                return "OK";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
    }
}
﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ezdan_admin.Startup))]
namespace ezdan_admin
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

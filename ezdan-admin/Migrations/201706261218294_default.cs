namespace ezdan_admin.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _default : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AdminRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AdminUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AdminRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AdminUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AdminUsers",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.UserId)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AdminUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AdminUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AdminUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AdminUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AdminUserRoles", "UserId", "dbo.AdminUsers");
            DropForeignKey("dbo.AdminUserLogins", "UserId", "dbo.AdminUsers");
            DropForeignKey("dbo.AdminUserClaims", "UserId", "dbo.AdminUsers");
            DropForeignKey("dbo.AdminUserRoles", "RoleId", "dbo.AdminRoles");
            DropIndex("dbo.AdminUserLogins", new[] { "UserId" });
            DropIndex("dbo.AdminUserClaims", new[] { "UserId" });
            DropIndex("dbo.AdminUsers", "UserNameIndex");
            DropIndex("dbo.AdminUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AdminUserRoles", new[] { "UserId" });
            DropIndex("dbo.AdminRoles", "RoleNameIndex");
            DropTable("dbo.AdminUserLogins");
            DropTable("dbo.AdminUserClaims");
            DropTable("dbo.AdminUsers");
            DropTable("dbo.AdminUserRoles");
            DropTable("dbo.AdminRoles");
        }
    }
}

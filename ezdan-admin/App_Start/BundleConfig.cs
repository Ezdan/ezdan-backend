﻿using System.Web.Optimization;

namespace ezdan_admin
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            BundleTable.EnableOptimizations = true;
            bundles.UseCdn = true;

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryuijs").Include(
                "~/Content/js/plugins/jquery-ui/jquery-ui.min.js",
                "~/Scripts/jquery-ui-1.10.4.custom/js/jquery-ui-1.10.4.custom.min.js",
                "~/Scripts/moment.min.js"
            ));

            bundles.Add(new StyleBundle("~/bundles/jqueryuicss").Include(
                "~/Content/js/plugins/jquery-ui/jquery-ui.min.css"
            ));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Scripts/bootstrap.min.js",
                "~/Scripts/respond.min.js",
                "~/content/js/plugins/bootstrap-maxlength/src/bootstrap-maxlength.js"
            ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap.css",
                "~/Content/site.css"));
            bundles.Add(new StyleBundle("~/Content/fullcalendarcss").Include(
                 "~/Content/themes/jquery.ui.all.css",
                 "~/Content/themes/jquery-ui-1.10.4.custom.css"
             ));

            //Calendar Script file
            

            bundles.Add(new ScriptBundle("~/bundles/fullcalendarjs").Include(
                "~/Scripts/jquery-ui-1.10.4.js",
                "~/Scripts/jquery-ui-1.10.4.custom/js/jquery-ui-1.10.4.custom.min.js",
                "~/Scripts/moment.min.js"
            ));

            var mapsCdn = "http://maps.google.com/maps/api/js?key=AIzaSyCAklqvnDgNmaNljMXeOEoXm23HKH2FTEY&sensor=false&libraries=places";

            //Maps bundle
            bundles.Add(new ScriptBundle("~/bundles/locationpicker", mapsCdn).Include(
                "~/Content/js/plugins/jquery-locationpicker/dist/locationpicker.jquery.js"
            ));
        }
    }
}

﻿using ezdan_entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ezdan_admin.ViewModels
{
    public class ContentClassifiedViewModel
    {
        public int ContentId { get; set; }

        public int? ParentId { get; set; }

        [Required]
        public string EnglishCategory { get; set; }

        [Required]
        public string ArabicCategory { get; set; }

    }
}
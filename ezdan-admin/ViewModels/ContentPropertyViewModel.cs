﻿using ezdan_entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ezdan_admin.ViewModels
{
    public class ContentPropertyViewModel
    {
        public Content contentObj;
        public List<SelectListItem> availableProperties { get; set; }
        public string selectedProperty { get; set; }
    }


    public class MultiSelectModelProperty
    {
        public IEnumerable<SelectListItem> Properties { set; get; }
        public string[] selectedProperties { set; get; }
        public Content content { set; get; }
        public List<SelectListItem> propertiesListWithNames { get; set; }
    }

    public class AvailableProperties
    {
        public int PropertyId { get; set; }
        public string PropertyName { get; set; }
        public bool Assigned { get; set; }
    }
}
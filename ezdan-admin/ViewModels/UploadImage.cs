﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ezdan_admin.ViewModels
{
    public class UploadImage
    {
        [Required]
        public HttpPostedFileBase File { get; set; }
    }
}
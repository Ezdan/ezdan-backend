﻿using ezdan_entities;
using System.Collections.Generic;

namespace ezdan_admin.ViewModels
{
    public class ContentEditViewModel
    {
        public int ContentId { get; set; }
        public string ContentEnglishName { get; set; }
        public string ContentArabicName { get; set; }
        public Content Content { get; set; }
        public List<ContentProperty> ContentPropertiesList { get; set; }
    }
}
﻿using ezdan_entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ezdan_admin.ViewModels
{
    public class ContentAnnounceViewModel
    {
        public int ContentId { get; set; }

        public int? ParentId { get; set; }

        [Required]
        public string ImageUrl { get; set; }

        [Required]
        public string ThumbUrl { get; set; }

        [Required]
        public string EnglishTitle { get; set; }

        [Required]
        public string ArabicTitle { get; set; }

        [Required]
        public string EnglishDesc { get; set; }

        [Required]
        public string ArabicDesc { get; set; }
    }
}
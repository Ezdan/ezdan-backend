﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ezdan_admin.ViewModels
{
    public class PushNotificationViewModel
    {
        [Required]
        public string Title { get; set; }

        [Required]
        public string Message { get; set; }
    }
}
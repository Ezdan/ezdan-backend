﻿using ezdan_entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ezdan_admin.ViewModels
{
    public class ContentFaqViewModel
    {
        public int ContentId { get; set; }

        public int? ParentId { get; set; }

        [Required]
        public string EnglishQuestion { get; set; }

        [Required]
        public string ArabicQuestion { get; set; }

        [Required]
        public string EnglishAnswer { get; set; }

        [Required]
        public string ArabicAnswer { get; set; }

    }
}
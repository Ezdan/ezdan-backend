﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ezdan_admin.Models
{
    public class Enum
    {
        public enum Properties
        {
            text = 1,
            html = 2,
            image = 3,
            thumb = 4,
            date = 5,
            postedBy = 6,
            postedOn = 7,
            ifSold = 8,
            contactEmail = 9,
            contactPhone = 10,
            price = 11
        };

        public enum MainContents
        {
            Faq = 1,
            Announcments = 2,
            Discover = 3,
            Classified = 4,
            Sakin = 5,
            Callus = 6,
            AboutEzdan = 7,
            MissionAndVision = 8,
            Objectives = 9,
            SocialEvents = 10,
            Awards = 11,
            WelcomeToQatar = 12,
            Mission = 13,
            Vision = 14, 
            Create = 15,
            Contribute = 16,
            Participate = 17,
            Build = 18,
            Expand = 19
        };

        public enum SuggestionTypes
        {
            Suggestion = 1,
            Complaint = 2,
            Inquiry = 3
        }

        public enum ContentImgsDir
        {
            home = 186,
            promotions = 2,
            events = 10,
            plus = 26
        }

    }
}
﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ezdan_admin.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public bool? IsSuperAdmin { get; set; }
        public bool IsActive { get; set; }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            if (IsSuperAdmin != null) userIdentity.AddClaim(new Claim("IsSuperAdmin", IsSuperAdmin.ToString()));
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            modelBuilder.Entity<ApplicationUser>().ToTable("AdminUsers").Property(p => p.Id).HasColumnName("UserId");
            modelBuilder.Entity<IdentityUserRole>().ToTable("AdminUserRoles");
            modelBuilder.Entity<IdentityUserLogin>().ToTable("AdminUserLogins");
            modelBuilder.Entity<IdentityUserClaim>().ToTable("AdminUserClaims");
            modelBuilder.Entity<IdentityRole>().ToTable("AdminRoles");
        }

        public System.Data.Entity.DbSet<ezdan_entities.Appointment> Appointments { get; set; }

        public System.Data.Entity.DbSet<ezdan_entities.Holiday> Holidays { get; set; }
    }
}
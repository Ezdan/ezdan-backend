﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;

namespace ezdan_admin.Models
{
    public static class IdentityExtensions
    {
        public static string IsSuperAdmin(this IIdentity identity)
        {
            var claim = ((ClaimsIdentity)identity).FindFirst("IsSuperAdmin");
            return (claim != null) ? claim.Value : string.Empty;
        }
    }
}
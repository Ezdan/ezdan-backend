﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ezdan_business.ezdanProperties {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.CollectionDataContractAttribute(Name="ArrayOfString", Namespace="http://ezdanrealestate.qa/", ItemName="string")]
    [System.SerializableAttribute()]
    public class ArrayOfString : System.Collections.Generic.List<string> {
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="PropertyValueType", Namespace="http://ezdanrealestate.qa/")]
    public enum PropertyValueType : int {
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        PropertyLocation = 0,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        PropertyType = 1,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        PropertyFurnishing = 2,
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://ezdanrealestate.qa/", ConfigurationName="ezdanProperties.PropertiesWSSoap")]
    public interface PropertiesWSSoap {
        
        // CODEGEN: Generating message contract since element name buildingNumber from namespace http://ezdanrealestate.qa/ is not marked nillable
        [System.ServiceModel.OperationContractAttribute(Action="http://ezdanrealestate.qa/GetPropertyImages", ReplyAction="*")]
        ezdan_business.ezdanProperties.GetPropertyImagesResponse GetPropertyImages(ezdan_business.ezdanProperties.GetPropertyImagesRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://ezdanrealestate.qa/GetPropertyImages", ReplyAction="*")]
        System.Threading.Tasks.Task<ezdan_business.ezdanProperties.GetPropertyImagesResponse> GetPropertyImagesAsync(ezdan_business.ezdanProperties.GetPropertyImagesRequest request);
        
        // CODEGEN: Generating message contract since element name buildingNumber from namespace http://ezdanrealestate.qa/ is not marked nillable
        [System.ServiceModel.OperationContractAttribute(Action="http://ezdanrealestate.qa/GetPropertyThumbnailImages", ReplyAction="*")]
        ezdan_business.ezdanProperties.GetPropertyThumbnailImagesResponse GetPropertyThumbnailImages(ezdan_business.ezdanProperties.GetPropertyThumbnailImagesRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://ezdanrealestate.qa/GetPropertyThumbnailImages", ReplyAction="*")]
        System.Threading.Tasks.Task<ezdan_business.ezdanProperties.GetPropertyThumbnailImagesResponse> GetPropertyThumbnailImagesAsync(ezdan_business.ezdanProperties.GetPropertyThumbnailImagesRequest request);
        
        // CODEGEN: Generating message contract since element name GetValuesForMobileResult from namespace http://ezdanrealestate.qa/ is not marked nillable
        [System.ServiceModel.OperationContractAttribute(Action="http://ezdanrealestate.qa/GetValuesForMobile", ReplyAction="*")]
        ezdan_business.ezdanProperties.GetValuesForMobileResponse GetValuesForMobile(ezdan_business.ezdanProperties.GetValuesForMobileRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://ezdanrealestate.qa/GetValuesForMobile", ReplyAction="*")]
        System.Threading.Tasks.Task<ezdan_business.ezdanProperties.GetValuesForMobileResponse> GetValuesForMobileAsync(ezdan_business.ezdanProperties.GetValuesForMobileRequest request);
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetPropertyImagesRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="GetPropertyImages", Namespace="http://ezdanrealestate.qa/", Order=0)]
        public ezdan_business.ezdanProperties.GetPropertyImagesRequestBody Body;
        
        public GetPropertyImagesRequest() {
        }
        
        public GetPropertyImagesRequest(ezdan_business.ezdanProperties.GetPropertyImagesRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://ezdanrealestate.qa/")]
    public partial class GetPropertyImagesRequestBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string buildingNumber;
        
        public GetPropertyImagesRequestBody() {
        }
        
        public GetPropertyImagesRequestBody(string buildingNumber) {
            this.buildingNumber = buildingNumber;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetPropertyImagesResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="GetPropertyImagesResponse", Namespace="http://ezdanrealestate.qa/", Order=0)]
        public ezdan_business.ezdanProperties.GetPropertyImagesResponseBody Body;
        
        public GetPropertyImagesResponse() {
        }
        
        public GetPropertyImagesResponse(ezdan_business.ezdanProperties.GetPropertyImagesResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://ezdanrealestate.qa/")]
    public partial class GetPropertyImagesResponseBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public ezdan_business.ezdanProperties.ArrayOfString GetPropertyImagesResult;
        
        public GetPropertyImagesResponseBody() {
        }
        
        public GetPropertyImagesResponseBody(ezdan_business.ezdanProperties.ArrayOfString GetPropertyImagesResult) {
            this.GetPropertyImagesResult = GetPropertyImagesResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetPropertyThumbnailImagesRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="GetPropertyThumbnailImages", Namespace="http://ezdanrealestate.qa/", Order=0)]
        public ezdan_business.ezdanProperties.GetPropertyThumbnailImagesRequestBody Body;
        
        public GetPropertyThumbnailImagesRequest() {
        }
        
        public GetPropertyThumbnailImagesRequest(ezdan_business.ezdanProperties.GetPropertyThumbnailImagesRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://ezdanrealestate.qa/")]
    public partial class GetPropertyThumbnailImagesRequestBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string buildingNumber;
        
        public GetPropertyThumbnailImagesRequestBody() {
        }
        
        public GetPropertyThumbnailImagesRequestBody(string buildingNumber) {
            this.buildingNumber = buildingNumber;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetPropertyThumbnailImagesResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="GetPropertyThumbnailImagesResponse", Namespace="http://ezdanrealestate.qa/", Order=0)]
        public ezdan_business.ezdanProperties.GetPropertyThumbnailImagesResponseBody Body;
        
        public GetPropertyThumbnailImagesResponse() {
        }
        
        public GetPropertyThumbnailImagesResponse(ezdan_business.ezdanProperties.GetPropertyThumbnailImagesResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://ezdanrealestate.qa/")]
    public partial class GetPropertyThumbnailImagesResponseBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string GetPropertyThumbnailImagesResult;
        
        public GetPropertyThumbnailImagesResponseBody() {
        }
        
        public GetPropertyThumbnailImagesResponseBody(string GetPropertyThumbnailImagesResult) {
            this.GetPropertyThumbnailImagesResult = GetPropertyThumbnailImagesResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetValuesForMobileRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="GetValuesForMobile", Namespace="http://ezdanrealestate.qa/", Order=0)]
        public ezdan_business.ezdanProperties.GetValuesForMobileRequestBody Body;
        
        public GetValuesForMobileRequest() {
        }
        
        public GetValuesForMobileRequest(ezdan_business.ezdanProperties.GetValuesForMobileRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://ezdanrealestate.qa/")]
    public partial class GetValuesForMobileRequestBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public ezdan_business.ezdanProperties.PropertyValueType propertyValueType;
        
        public GetValuesForMobileRequestBody() {
        }
        
        public GetValuesForMobileRequestBody(ezdan_business.ezdanProperties.PropertyValueType propertyValueType) {
            this.propertyValueType = propertyValueType;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetValuesForMobileResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="GetValuesForMobileResponse", Namespace="http://ezdanrealestate.qa/", Order=0)]
        public ezdan_business.ezdanProperties.GetValuesForMobileResponseBody Body;
        
        public GetValuesForMobileResponse() {
        }
        
        public GetValuesForMobileResponse(ezdan_business.ezdanProperties.GetValuesForMobileResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://ezdanrealestate.qa/")]
    public partial class GetValuesForMobileResponseBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string GetValuesForMobileResult;
        
        public GetValuesForMobileResponseBody() {
        }
        
        public GetValuesForMobileResponseBody(string GetValuesForMobileResult) {
            this.GetValuesForMobileResult = GetValuesForMobileResult;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface PropertiesWSSoapChannel : ezdan_business.ezdanProperties.PropertiesWSSoap, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class PropertiesWSSoapClient : System.ServiceModel.ClientBase<ezdan_business.ezdanProperties.PropertiesWSSoap>, ezdan_business.ezdanProperties.PropertiesWSSoap {
        
        public PropertiesWSSoapClient() {
        }
        
        public PropertiesWSSoapClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public PropertiesWSSoapClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public PropertiesWSSoapClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public PropertiesWSSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        ezdan_business.ezdanProperties.GetPropertyImagesResponse ezdan_business.ezdanProperties.PropertiesWSSoap.GetPropertyImages(ezdan_business.ezdanProperties.GetPropertyImagesRequest request) {
            return base.Channel.GetPropertyImages(request);
        }
        
        public ezdan_business.ezdanProperties.ArrayOfString GetPropertyImages(string buildingNumber) {
            ezdan_business.ezdanProperties.GetPropertyImagesRequest inValue = new ezdan_business.ezdanProperties.GetPropertyImagesRequest();
            inValue.Body = new ezdan_business.ezdanProperties.GetPropertyImagesRequestBody();
            inValue.Body.buildingNumber = buildingNumber;
            ezdan_business.ezdanProperties.GetPropertyImagesResponse retVal = ((ezdan_business.ezdanProperties.PropertiesWSSoap)(this)).GetPropertyImages(inValue);
            return retVal.Body.GetPropertyImagesResult;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ezdan_business.ezdanProperties.GetPropertyImagesResponse> ezdan_business.ezdanProperties.PropertiesWSSoap.GetPropertyImagesAsync(ezdan_business.ezdanProperties.GetPropertyImagesRequest request) {
            return base.Channel.GetPropertyImagesAsync(request);
        }
        
        public System.Threading.Tasks.Task<ezdan_business.ezdanProperties.GetPropertyImagesResponse> GetPropertyImagesAsync(string buildingNumber) {
            ezdan_business.ezdanProperties.GetPropertyImagesRequest inValue = new ezdan_business.ezdanProperties.GetPropertyImagesRequest();
            inValue.Body = new ezdan_business.ezdanProperties.GetPropertyImagesRequestBody();
            inValue.Body.buildingNumber = buildingNumber;
            return ((ezdan_business.ezdanProperties.PropertiesWSSoap)(this)).GetPropertyImagesAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        ezdan_business.ezdanProperties.GetPropertyThumbnailImagesResponse ezdan_business.ezdanProperties.PropertiesWSSoap.GetPropertyThumbnailImages(ezdan_business.ezdanProperties.GetPropertyThumbnailImagesRequest request) {
            return base.Channel.GetPropertyThumbnailImages(request);
        }
        
        public string GetPropertyThumbnailImages(string buildingNumber) {
            ezdan_business.ezdanProperties.GetPropertyThumbnailImagesRequest inValue = new ezdan_business.ezdanProperties.GetPropertyThumbnailImagesRequest();
            inValue.Body = new ezdan_business.ezdanProperties.GetPropertyThumbnailImagesRequestBody();
            inValue.Body.buildingNumber = buildingNumber;
            ezdan_business.ezdanProperties.GetPropertyThumbnailImagesResponse retVal = ((ezdan_business.ezdanProperties.PropertiesWSSoap)(this)).GetPropertyThumbnailImages(inValue);
            return retVal.Body.GetPropertyThumbnailImagesResult;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ezdan_business.ezdanProperties.GetPropertyThumbnailImagesResponse> ezdan_business.ezdanProperties.PropertiesWSSoap.GetPropertyThumbnailImagesAsync(ezdan_business.ezdanProperties.GetPropertyThumbnailImagesRequest request) {
            return base.Channel.GetPropertyThumbnailImagesAsync(request);
        }
        
        public System.Threading.Tasks.Task<ezdan_business.ezdanProperties.GetPropertyThumbnailImagesResponse> GetPropertyThumbnailImagesAsync(string buildingNumber) {
            ezdan_business.ezdanProperties.GetPropertyThumbnailImagesRequest inValue = new ezdan_business.ezdanProperties.GetPropertyThumbnailImagesRequest();
            inValue.Body = new ezdan_business.ezdanProperties.GetPropertyThumbnailImagesRequestBody();
            inValue.Body.buildingNumber = buildingNumber;
            return ((ezdan_business.ezdanProperties.PropertiesWSSoap)(this)).GetPropertyThumbnailImagesAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        ezdan_business.ezdanProperties.GetValuesForMobileResponse ezdan_business.ezdanProperties.PropertiesWSSoap.GetValuesForMobile(ezdan_business.ezdanProperties.GetValuesForMobileRequest request) {
            return base.Channel.GetValuesForMobile(request);
        }
        
        public string GetValuesForMobile(ezdan_business.ezdanProperties.PropertyValueType propertyValueType) {
            ezdan_business.ezdanProperties.GetValuesForMobileRequest inValue = new ezdan_business.ezdanProperties.GetValuesForMobileRequest();
            inValue.Body = new ezdan_business.ezdanProperties.GetValuesForMobileRequestBody();
            inValue.Body.propertyValueType = propertyValueType;
            ezdan_business.ezdanProperties.GetValuesForMobileResponse retVal = ((ezdan_business.ezdanProperties.PropertiesWSSoap)(this)).GetValuesForMobile(inValue);
            return retVal.Body.GetValuesForMobileResult;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ezdan_business.ezdanProperties.GetValuesForMobileResponse> ezdan_business.ezdanProperties.PropertiesWSSoap.GetValuesForMobileAsync(ezdan_business.ezdanProperties.GetValuesForMobileRequest request) {
            return base.Channel.GetValuesForMobileAsync(request);
        }
        
        public System.Threading.Tasks.Task<ezdan_business.ezdanProperties.GetValuesForMobileResponse> GetValuesForMobileAsync(ezdan_business.ezdanProperties.PropertyValueType propertyValueType) {
            ezdan_business.ezdanProperties.GetValuesForMobileRequest inValue = new ezdan_business.ezdanProperties.GetValuesForMobileRequest();
            inValue.Body = new ezdan_business.ezdanProperties.GetValuesForMobileRequestBody();
            inValue.Body.propertyValueType = propertyValueType;
            return ((ezdan_business.ezdanProperties.PropertiesWSSoap)(this)).GetValuesForMobileAsync(inValue);
        }
    }
}

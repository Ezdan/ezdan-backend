﻿using ezdan_business.Helper;
using ezdan_entities.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ezdan_business.Commands.User
{
    public class AddUserProfileImageCommand : BaseCommand
    {
        private DataHelper db;
        public HttpPostedFile file;
        public string filename;
        public string foldername;
        public string userId;
        private string DBImageURL;
        public JObject result;

        public AddUserProfileImageCommand()
        {
            db = new DataHelper();
            result = new JObject();
        }

        protected override void DoAction()
        {
            var user = db.DbEntities.AspNetUsers.Where(d => d.Id == userId).FirstOrDefault();

            DBImageURL = ImageHelper.SaveImage(file, filename, foldername, userId);

            user.ImageURL = DBImageURL;
            var status = db.SaveChanges();

            result.Add("status", status.ToString());
            result.Add("imageURL", DBImageURL);
        }

        protected override bool DoValidate()
        {
            return true;
        }
    }
}

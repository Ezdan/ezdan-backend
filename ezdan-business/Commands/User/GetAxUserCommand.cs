﻿using ezdan_business.Helper;
using ezdan_business.PITMobileIntergration;
using ezdan_entities.Models;
using ezdan_logger;
using System;

namespace ezdan_business.Commands.User
{
    public class GetAxUserCommand : BaseCommand
    {
        public string ContractNumber;
        public string PhoneNumber;
        private PITCustomerServiceClient client;
        private string SVCUsername;
        private string SVCPassword;
        public string AccountNumber;
        public bool isLogin;

        public bool? IsValid { get; private set; }
        public bool? InvalidPhone { get; private set; }
        public AxUser axUser { get; private set; }

        public GetAxUserCommand()
        {
            isLogin = false;
            InvalidPhone = false;
        }

        public bool IsEnableIntegration
        {
            get
            {
                return Convert.ToBoolean(SettingHelper.GetAppSettingValue("IsEnableIntegration"));
            }
        }

        protected override void DoAction()
        {
            if (IsEnableIntegration)
            {
                try
                {
                    #region initialization
                    SVCUsername = SettingHelper.GetAppSettingValue("SVCUsername").ToString();
                    SVCPassword = SettingHelper.GetAppSettingValue("SVCPassword").ToString();
                    client = new PITCustomerServiceClient();
                    client.ClientCredentials.Windows.ClientCredential.UserName = SVCUsername;
                    client.ClientCredentials.Windows.ClientCredential.Password = SVCPassword;
                    #endregion

                    CallContext context = new CallContext() { Company = "ERE" };

                    PITFlxContractEntity[] contracts =
                        client.ListContract(context, ContractNumber, AccountNumber);
                    if (contracts != null && contracts.Length > 0 && contracts[0].parmContractStatus.ToString() == "Active")
                    {
                        if (!isLogin)
                        {
                            //customer found
                            PITCustomerEntity[] customers =
                            client.ListCustomer(context, "", ContractNumber);
                            if ((customers != null && customers.Length > 0))
                            {
                                var nationalPhonePart = PhoneNumber.Replace(SettingHelper.GetAppSettingValue("DialCode"), "");
                                if (customers[0].parmPhone != nationalPhonePart)
                                {
                                    InvalidPhone = true;
                                }
                                axUser = new AxUser()
                                {
                                    AccountNumber = customers[0].parmAccountNum,
                                    ContractNumber = customers[0].parmContract,
                                    PhoneNumber = customers[0].parmPhone,
                                    UnitId = customers[0].parmUnitId,
                                    PartyRecId = customers[0].parmPartyRecId,
                                    City = customers[0].parmCity,

                                };
                            }
                        }

                        IsValid = true;
                    }
                    else
                    {
                        IsValid = false;
                    }
                }
                catch (Exception ex)
                {
                    LogManager.Log(LogType.error, ex.Message, ex);
                    IsValid = null;
                }
            }
            else
            {
                IsValid = ContractNumber == "123";
            }
        }

        protected override bool DoValidate()
        {
            return !string.IsNullOrEmpty(ContractNumber);
        }
    }
}

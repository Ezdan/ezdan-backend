﻿using ezdan_business.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ezdan_entities;


namespace ezdan_business.Commands.User
{
    public class VerifyResetPasswordCodeCommand : BaseCommand
    {
        private ezdan_entities.Entities db;
        public string Email { get; set; }
        public string UserId { get; set; }
        public string VerificationCode { get; set; }
        public bool codeMatched { get; set; }

        public VerifyResetPasswordCodeCommand()
        {
            db = new ezdan_entities.Entities();
        }

        protected override void DoAction()
        {
            ResetVerification resetVerification = db.ResetVerifications.Where(rv => rv.UserId == UserId && rv.IsUsed == false).FirstOrDefault();
            codeMatched = (resetVerification != null && VerificationCode == resetVerification.Code) ? true : false;
            if (codeMatched)
            {
                resetVerification.IsUsed = true;
                db.SaveChanges();
            }
        }
        protected override bool DoValidate()
        {
            return !string.IsNullOrEmpty(UserId);
        }
    }
}

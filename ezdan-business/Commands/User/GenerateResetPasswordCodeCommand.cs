﻿using ezdan_business.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ezdan_entities;


namespace ezdan_business.Commands.User
{
    public class GenerateResetPasswordCodeCommand : BaseCommand
    {
        private ezdan_entities.Entities db;
        private ResetVerification resetVerification;
        public string Email { get; set; }
        public string UserId { get; set; }
        public string phoneAuthCode { get; set; }
        public int addedEntryId { get; set; }
        public bool phoneAuth { get; set; }

        public GenerateResetPasswordCodeCommand()
        {
            phoneAuth = false;
            db = new ezdan_entities.Entities();
        }
        public bool codeCreated { get; set; }
        public bool codeSent { get; set; }

        protected override void DoAction()
        {
            Random random = new Random();
            int value = random.Next(1000, 9999);
            codeCreated = value > 0 ? true : false;
            var userResetCodes = db.ResetVerifications.Where(u => u.UserId == UserId && u.IsUsed == false).ToList();
            foreach (var userReset in userResetCodes)
            {
                userReset.IsUsed = true;
            }
            resetVerification = new ResetVerification();
            resetVerification.UserId = UserId;
            resetVerification.CreationDate = DateTime.UtcNow;
            resetVerification.ExpiryDate = DateTime.UtcNow.AddHours(1);
            if(phoneAuth)
            {
                resetVerification.Code = phoneAuthCode = value.ToString();
            }
            else
            {
                resetVerification.Code = value.ToString();
            }
            db.ResetVerifications.Add(resetVerification);
            db.SaveChanges();
            addedEntryId = resetVerification.ResetVerificationId;
            if(!phoneAuth)
                MailHelper.SendEmail(new ezdan_entities.Models.EmailNotification { EmailTo = Email, Subject = "Ezdan Password Reset Code", Body = "Your reset verification code is : "+value });

        }
        protected override bool DoValidate()
        {
            if(!phoneAuth)
                return !string.IsNullOrEmpty(UserId);
            return true;
        }
    }
}

﻿using ezdan_business.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ezdan_business.Commands.User
{
    public class ValidEmailCommand : BaseCommand
    {
        DataHelper db;
        public string Id { get; set; }
        public string Email { get; set; }

        public ValidEmailCommand()
        {
            db = new DataHelper();
        }
        public bool IsExist { get; set; }

        protected override void DoAction()
        {
            IsExist = db.DbEntities.AspNetUsers.Any(u => u.Email.ToLower() == Email.ToLower() && u.Id != Id);
            //var user
            //     = db.DbEntities.AspNetUsers.Where(u => u.Email.ToLower() == Email.ToLower()).FirstOrDefault();
            //IsExist = user == null ? true : false;
        }
        protected override bool DoValidate()
        {
            return !string.IsNullOrEmpty(Email);
        }
    }
}

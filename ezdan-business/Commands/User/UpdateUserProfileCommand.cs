﻿using ezdan_entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ezdan_business.Commands.User
{
    public class UpdateUserProfileCommand : BaseCommand
    {
        private Entities db;
        public JObject userModel { get; set; }
        public bool UpdateStatus { get; set; }
        public JObject UpdatedInfo { get; set; }
        public string UserId { get; set; }

        public UpdateUserProfileCommand()
        {
            db = new Entities();
            UpdatedInfo = new JObject();
        }

        protected override void DoAction()
        {
            var user = db.AspNetUsers.Where(u => u.Id == UserId).FirstOrDefault();
            var Json = (JObject)JsonConvert.DeserializeObject(userModel.ToString());
            string name = Convert.ToString(Json["name"]);
            string email = Convert.ToString(Json["email"]);
            string mobile = Convert.ToString(Json["mobile"]);

            user.FullName = name;
            user.Email = email;
            user.UserName = email;
            user.PhoneNumber = mobile;

            UpdatedInfo.Add("fullName", name);
            UpdatedInfo.Add("email", email);
            UpdatedInfo.Add("mobile", mobile);
            
            int result = db.SaveChanges();
            UpdateStatus = Convert.ToBoolean(result);
        }

        protected override bool DoValidate()
        {
            return !string.IsNullOrEmpty(UserId);
        }
    }
}

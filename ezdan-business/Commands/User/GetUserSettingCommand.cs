﻿using ezdan_entities;
using ezdan_entities.Helper;
using ezdan_entities.Models;
using System.Linq;

namespace ezdan_business.Commands.User
{
    public class GetUserSettingCommand : BaseCommand
    {
        private ezdan_entities.Entities db;
        private string UserId;
        public UserSetting UserSetting { get; set; }
        public GetUserSettingCommand(string _UserId):base()
        {
            db = new ezdan_entities.Entities();
            UserId = _UserId;
        }
        protected override void DoAction()
        {
            var user = db.AspNetUsers.Where(d => d.Id == UserId).FirstOrDefault();

            UserSetting = new UserSetting()
            {
                UserLanguage = (AppLanguage)user.UserLanguage,
                IsEnableNotification = user.EnableNotification
            };
        }

        protected override bool DoValidate()
        {
            return !string.IsNullOrEmpty(UserId);
        }
    }
}

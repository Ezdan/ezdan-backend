﻿using ezdan_business.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ezdan_business.Commands.User
{
    public class ValidContractNumberCommand : BaseCommand
    {
        DataHelper db;
        public string ContractNumber { get; set; }

        public ValidContractNumberCommand()
        {
            db = new DataHelper();
        }
        public bool IsExist { get; set; }

        protected override void DoAction()
        {
            IsExist = db.DbEntities.AspNetUsers.Any(u => u.ContractNumber.ToLower() == ContractNumber.ToLower());
        }
        protected override bool DoValidate()
        {
            return !string.IsNullOrEmpty(ContractNumber);
        }
    }
}

﻿using ezdan_entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;

namespace ezdan_business.Commands.User
{
    public class UpdateUserSettingCommand : BaseCommand
    {
        private Entities db;
        public bool UpdateStatus { get; set; }
        public UpdateUserSettingCommand()
        {
            db = new Entities();
        }

        public JObject UpdatedValue { get; set; }
        public string UserId { get; set; }
        protected override void DoAction()
        {
            var user = db.AspNetUsers.Where(u => u.Id == UserId).FirstOrDefault();
            var Json = (JObject)JsonConvert.DeserializeObject(UpdatedValue.ToString());
            int ChangedSetting = Convert.ToInt32(Json["ChangedSetting"]);
            switch (ChangedSetting)
            {
                case 1:
                    int newLang = Convert.ToInt32(Json["UpdatedValue"]);
                    user.UserLanguage = newLang;
                    break;
                case 2:
                    bool UpdatedSettingValue = Convert.ToBoolean(Json["UpdatedValue"]);
                    user.EnableNotification = UpdatedSettingValue;
                    break;
                default:
                    break;
            }
            int result = db.SaveChanges();
            UpdateStatus = Convert.ToBoolean(result);
        }

        protected override bool DoValidate()
        {
            return !string.IsNullOrEmpty(UserId);
        }
    }
}

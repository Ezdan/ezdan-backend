﻿namespace ezdan_business.Commands
{
    public class Invoker
    {
        private BaseCommand Command;
        public void SetCommand(BaseCommand _Command)
        {
            Command = _Command;
        }
        public void ExecuteCommand()
        {
            Command.Execute();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ezdan_business.Commands
{
    public static class EventBroker
    {
        public delegate void actionEventHandler(dynamic actionState, EventArgs e);


        private static event actionEventHandler _PreExecute;
        private static event actionEventHandler _PostExecute;

        public static List<Type> _PreCommands;
        public static List<Type> _PostCommands;

        static EventBroker()
        {
            _PreCommands = new List<Type>();
            _PostCommands = new List<Type>();
            _PreExecute += EventBroker__onPreFired;

            _PostExecute += EventBroker__onPostFired;
        }

        private static void EventBroker__onPreFired(dynamic actionState, EventArgs e)
        {
            foreach (Type sub in _PreCommands)
            {
                BaseCommand instance = (BaseCommand)Activator.CreateInstance(sub, actionState);

                instance.Execute();
            }
        }

        private static void EventBroker__onPostFired(dynamic actionState, EventArgs e)
        {
            //look for subs in configuration & instantiate objects
            foreach (Type sub in _PostCommands)
            {
                BaseCommand instance = (BaseCommand)Activator.CreateInstance(sub, actionState);

                instance.Execute();
            }


        }

        public static void FirePrintedOrderPaidEvent(dynamic actionState, EventArgs e)
        {
            EventBroker__onPreFired(actionState, e);
        }

        public static void FireEmailOrderPaidEvent(dynamic actionState, EventArgs e)
        {
            EventBroker__onPostFired(actionState, e);
        }
    }
}

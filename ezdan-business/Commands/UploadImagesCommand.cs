﻿using ezdan_business.Helper;
using ezdan_business.PITMobileIntergration;
using ezdan_logger;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ezdan_business.Commands.Service
{
    public class UploadImagesCommand : BaseCommand
    {
        public JObject resp;
        private HttpPostedFile file;
        private string dir;
        private string filename;
        private string foldername;
        private string userId;
        private string DBImageURL;
        public bool imageUploaded;

        public UploadImagesCommand(HttpPostedFile file, string dir, string userId)
        {
            resp = new JObject();
            this.file = file;
            this.filename = this.file.FileName;
            this.dir = dir;
            //foldername = "cases";
            this.userId = userId;
            DBImageURL = null;
            imageUploaded = false;
        }

        protected override void DoAction()
        {
            try {
                userId = userId == null ? "guest" : userId;
                DBImageURL = ImageHelper.SaveImage(file, filename, dir, userId);
                if(DBImageURL != null)
                {
                    resp.Add("imageURL", DBImageURL);
                    imageUploaded = true;
                }
            }
            catch (Exception ex)
            {
                LogManager.Log(LogType.error, ex.Message, ex);
            }
        }

        protected override bool DoValidate()
        {
            return file != null;
        }

    }
}

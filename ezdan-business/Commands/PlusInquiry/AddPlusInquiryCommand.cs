﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ezdan_entities;
using Newtonsoft.Json.Linq;
using System.Web;
using System.Net.Mail;
using System.Configuration;
using System.Diagnostics;
using Newtonsoft.Json;
using ezdan_logger;
using ezdan_business.Helper;
using System.Text;
using System.Threading.Tasks;

namespace ezdan_business.Commands.PlusInquiry
{
    public class AddPlusInquiryCommand : BaseCommand
    {
        DataHelper db;
        public ezdan_entities.PlusInquiry plusInquiry { get; set; }
        public bool Added { get; set; }
        public AddPlusInquiryCommand()
        {
            db = new DataHelper();
        }
        protected override void DoAction()
        {
            plusInquiry.Date = DateTime.Now;
            db.DbEntities.PlusInquiries.Add(plusInquiry);
            Added = db.SaveChanges();
            string FileName = HttpContext.Current.Server.MapPath("~/Views/MailTemplates/plusMailTemplate.html");
            string MailBody = System.IO.File.ReadAllText(FileName);
            MailBody = MailBody.Replace("##NAME##", plusInquiry.Name);
            MailBody = MailBody.Replace("##EMAIL##", plusInquiry.Email);
            MailBody = MailBody.Replace("##DATE##", DateTime.Now.ToString());
            MailBody = MailBody.Replace("##ID##", plusInquiry.Id.ToString());
            MailHelper.SendEmail(new ezdan_entities.Models.EmailNotification {
                EmailTo = "ezdan.company@gmail.com",
                Subject = "New Ezdan Plus Inquiry",
                Body = MailBody
            });
        }

        protected override bool DoValidate()
        {
            return plusInquiry != null;
        }
    }
}

﻿using ezdan_logger;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ezdan_business.Commands.Photogallery
{
    public class GetAlbumsCommand : BaseCommand
    {
        public JObject allAlbumsResult { get; set; }
        public string lang { get; set; }

        protected override void DoAction()
        {
            string html = string.Empty;
            string JSONresult = string.Empty;
            //Regex yourRegex = new Regex(@"<[^>]+>|&nbsp");
            //Regex yourRegex = new Regex(@"<[^>]+>");
            Regex yourRegex = new Regex(@"<pre>|</pre>");
            string url = @"http://ezdanrealestate.qa/json/photogallery-albums.aspx?lang=" + lang;

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.AutomaticDecompression = DecompressionMethods.GZip;
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    html = reader.ReadToEnd();
                    JSONresult = yourRegex.Replace(html, "");
                    allAlbumsResult = JObject.Parse(JSONresult);
                }
            }
            catch (Exception ex)
            {
                LogManager.Log(LogType.error, ex.Message, ex);
                allAlbumsResult = null;
            }
        }

        protected override bool DoValidate()
        {
            return true;
        }
    }
}

﻿using ezdan_business.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ezdan_business.Commands.Feedback
{
    public class AddFeedbackCommand : BaseCommand
    {
        private ezdan_entities.Feedback feedback;
        public string userId { get; set; }
        public string title { get; set; }
        public string contactNumber { get; set; }
        public string message { get; set; }
        public int rate { get; set; }
        public byte type { get; set; }



        //public HttpPostedFile file;
        public string imageUrl;
        public string filename;
        public string foldername;
        private string DBImageURL;



        public bool IsAdded { get; set; }
        DataHelper db;
        public AddFeedbackCommand()
        {
            db = new DataHelper();
            feedback = new ezdan_entities.Feedback();
            imageUrl = null;
        }
        protected override void DoAction()
        {
            feedback.UserId = userId;
            feedback.Title = title;
            feedback.ContactNumber = contactNumber;
            feedback.Message = message;
            feedback.Rating = rate;
            feedback.Type = type;

            if (imageUrl != null)
            {
                //DBImageURL = ImageHelper.SaveImage(file, filename, foldername, userId);
                //if (DBImageURL != null)
                //{
                feedback.ImageURL = imageUrl;
                //}
            }

            feedback.Date = DateTime.Now;

            db.DbEntities.Feedbacks.Add(feedback);

            IsAdded = db.SaveChanges();

            if(IsAdded)
            {
                var type = (Models.Enum.SuggestionTypes)feedback.Type;
                string determiner = type.ToString().ToLower() == "inquiry" ? "An" : "A";
                string filename = HttpContext.Current.Server.MapPath("~/Views/MailTemplates/feedbackMailTemplate.html");
                string mailbody = System.IO.File.ReadAllText(filename);
                if(userId != null)
                {
                    ezdan_entities.AspNetUser user = db.DbEntities.AspNetUsers.Find(userId); 
                    mailbody = mailbody.Replace("##USERTYPE##", "the following user");
                    mailbody = mailbody.Replace("##FULLNAME_ELEM##", "<p style='color: white'>Full Name: <span style='color:#f17077'>" + user.FullName + "</span></p>");
                    mailbody = mailbody.Replace("##EMAIL_ELEM##", "<p style='color: white'>Email Address: <span style='color:#f17077'>" + user.Email + "</span></p>");
                    mailbody = mailbody.Replace("##LOCATION_ELEM##", "<p style='color: white'>Location: <span style='color:#f17077'>" + user.Address + "</span></p>");
                    mailbody = mailbody.Replace("##UNIT_ELEM##", "<p style='color: white'>Unit: <span style='color:#f17077'>" + user.UnitId + "</span></p>");
                }
                else
                {
                    mailbody = mailbody.Replace("##USERTYPE##", "a guest");
                    mailbody = mailbody.Replace("##FULLNAME_ELEM##", "");
                    mailbody = mailbody.Replace("##EMAIL_ELEM##", "");
                    mailbody = mailbody.Replace("##LOCATION_ELEM##", "");
                    mailbody = mailbody.Replace("##UNIT_ELEM##", "");
                }
                mailbody = mailbody.Replace("##DETERMINER##", determiner);
                mailbody = mailbody.Replace("##TYPE##", type.ToString().ToLower());
                mailbody = mailbody.Replace("##MOBILE##", contactNumber);
                mailbody = mailbody.Replace("##SUBJECT##", title);
                mailbody = mailbody.Replace("##MESSAGE##", message);
                string salesDptEmail = db.DbEntities.ContentProperties.Where(cps => cps.ContentId == 6 && cps.PropertyId == 36).Select(cps => cps.EnglishValue).FirstOrDefault();
                if (string.IsNullOrEmpty(salesDptEmail))
                {
                    salesDptEmail = SettingHelper.GetAppSettingValue("SalesDptEmail");
                }
                MailHelper.SendEmail(new ezdan_entities.Models.EmailNotification { EmailTo = salesDptEmail, Subject = "New Feedback", Body = mailbody });
            }
        }

        protected override bool DoValidate()
        {
            return feedback != null;
        }
    }
}

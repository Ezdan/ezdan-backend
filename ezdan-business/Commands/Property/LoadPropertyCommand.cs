﻿using System;
using System.Collections.Generic;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;
using ezdan_logger;
using ezdan_business.Helper;

namespace ezdan_business.Commands.Property
{
    public class LoadPropertyCommand : BaseCommand
    {
        public static List<ezdan_entities.Models.Property> EzdanProperties { get; set; }
        public static DateTime _nextSyncDateTime;
        private static int TimerTicksInMinutes;

        private string Xmlpath
        {
            get
            {
                bool isLocal = Convert.ToBoolean(SettingHelper.GetAppSettingValue("LoadPropertyLocal"));
                if (isLocal)
                    return SettingHelper.GetAppSettingValue("LocalPropertyFile").ToString();
                else
                    return SettingHelper.GetAppSettingValue("PublicPropertyFile").ToString();
            }
        }

        public LoadPropertyCommand()
        {
            
        }
        static LoadPropertyCommand()
        {
            EzdanProperties = new List<ezdan_entities.Models.Property>();
            TimerTicksInMinutes = Convert.ToInt32(SettingHelper.GetAppSettingValue("TimerTicks").ToString());
        }
        private static XmlNamespaceManager _nsmgr;

        private static XmlNamespaceManager NsMgr
        {
            get
            {
                if (_nsmgr == null)
                {
                    _nsmgr = new XmlNamespaceManager(new NameTable());
                    _nsmgr.AddNamespace("msb", "http://schemas.microsoft.com/dynamics/2011/01/documents/Message");
                    _nsmgr.AddNamespace("unit", "http://schemas.microsoft.com/dynamics/2008/01/documents/EHGFlxUnit");
                    _nsmgr.AddNamespace("shard", "http://schemas.microsoft.com/dynamics/2008/01/sharedtypes");
                }
                return _nsmgr;
            }
        }

        protected override void DoAction()
        {
            try
            {
                LogManager.Log(LogType.trace, "mobile test get properties");
                LogManager.Log(LogType.trace, _nextSyncDateTime.ToString());
                if (_nextSyncDateTime == null
                    || (_nextSyncDateTime != null && _nextSyncDateTime < DateTime.Now))
                {
                    LogManager.Log(LogType.trace, "updating properties");
                    _nextSyncDateTime = DateTime.Now.AddMinutes(TimerTicksInMinutes);
                    EzdanProperties = new List<ezdan_entities.Models.Property>();
                   
                    XmlDocument xmlDocument = new XmlDocument();
                    xmlDocument.Load(Xmlpath);
                    LogManager.Log(LogType.trace, Xmlpath);
                    XmlNodeList xnList =
                        xmlDocument.SelectNodes("/msb:Envelope/msb:Body/msb:MessageParts/unit:EHGFlxUnit/unit:FlxBuildingUnit[@class='entity']", NsMgr);
                    foreach (XmlNode nodeItem in xnList)
                    {
                        if (nodeItem.HasChildNodes)
                        {
                            EzdanProperties.Add(MapToProperty(nodeItem));
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                LogManager.Log(LogType.error, ex.Message, ex);
                _nextSyncDateTime = DateTime.Now;
            }
        }

        private ezdan_entities.Models.Property MapToProperty(XmlNode nodeItem)
        {
            ezdan_entities.Models.Property CP = new ezdan_entities.Models.Property();

            try
            {
                #region variables
                int PGFlag = 0;
                string ID = "";
                #endregion

                foreach (XmlNode item in nodeItem.ChildNodes)
                {
                    if (item.Name == "_DocumentHash")
                        CP._DocumnetHash = item.InnerText;
                    else if (item.Name == "DefaultDimension")
                    {
                        foreach (XmlNode values in item)
                        {
                            foreach(XmlNode value in values)
                            {
                                foreach (XmlNode node in value)
                                {
                                    if (node.InnerText == "Propertygroup")
                                    {
                                        CP.PropertyGroup = node.NextSibling.InnerText;
                                    }
                                    else if (node.InnerText == "PropertyBuilding")
                                    {
                                        CP.PropertyBuilding = node.NextSibling.InnerText;
                                    }
                                    else if (node.InnerText == "Propertyunit")
                                    {
                                        CP.PropertyUnit = node.NextSibling.InnerText;
                                    }
                                }
                            }
                        }
                    }
                    else if (item.Name == "Furnished")
                        CP.Furnished = item.InnerText;
                    else if (item.Name == "PITUnitClassification")
                        CP.PropertyType = item.InnerText;
                    else if (item.Name == "PITNumberOfRooms")
                        CP.NumberOfBedrooms = Convert.ToInt32(item.InnerText);
                    else if (item.Name == "Propertygroup")
                        ID = CP.PropertyGroup = item.InnerText;
                    else if (item.Name == "thumb")
                        CP.thumb = item.InnerText;
                    else if (item.Name == "FlxBuildingTable")
                    {
                        foreach (XmlNode values in item)
                        {
                            if (values.Name == "FlxBuildingDefaults")
                            {
                                foreach (XmlNode PP in values)
                                {
                                    if (PP.Name == "Price")
                                    {
                                        CP.Price = PP.InnerText;
                                    }
                                    else if (PP.Name == "PricePerMonth")
                                    {
                                        if (PP.InnerText == "Yes")
                                        {
                                            CP.PriceFor = "Month";
                                        }
                                        else
                                        {
                                            CP.PriceFor = "Year";
                                        }
                                    }
                                    else if (PP.Name == "FlxBuildingGroupTable")
                                    {
                                        foreach (XmlNode InnerFlxFGroupTable in PP)
                                        {
                                            if (InnerFlxFGroupTable.Name == "City")
                                                CP.City = InnerFlxFGroupTable.InnerText;
                                            else if (InnerFlxFGroupTable.Name == "MarketingName")
                                                CP.MarketingName = InnerFlxFGroupTable.InnerText;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                CP.thumb = @"http://ezdanrealestate.qa" + "//images/properties2/" + CP.PropertyGroup + "/thumbnail-" + CP.PropertyGroup + ".jpg";
                return CP;
            }
            catch (Exception ex)
            {
                LogManager.Log(LogType.error, ex.Message, ex);
                return new ezdan_entities.Models.Property();
            }
            
        }
        protected override bool DoValidate()
        {
            return true;
        }
    }
}

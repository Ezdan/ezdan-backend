﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;
using ezdan_entities.Models;
using ezdan_logger;

namespace ezdan_business.Commands.Property
{
    public class FindPropertyCommand : BaseCommand
    {
        public List<ezdan_entities.Models.Property> FilteredList { get; set; }
        List<ezdan_entities.Models.Property> Properties;

        #region Filter Options
        public string Bedrooms { get; set; }
        public string City { get; set; }
        public string PropertyType { get; set; }
        public string IsFurnished { get; set; }
        #endregion
        public FindPropertyCommand()
        {
            FilteredList = new List<ezdan_entities.Models.Property>();
            Properties = new List<ezdan_entities.Models.Property>();
            PreExecute += FindPropertyCommand_PreExecute;
        }

        private void FindPropertyCommand_PreExecute()
        {
            LoadPropertyCommand loadPropertyCommand = new LoadPropertyCommand();
            loadPropertyCommand.Execute();
            Properties = LoadPropertyCommand.EzdanProperties;
        }

        protected override void DoAction()
        {
            //LogManager.Log(LogType.info, JsonConvert.SerializeObject(Properties));
            var query = Properties.AsQueryable();
            if (!string.IsNullOrEmpty(Bedrooms))
            {
                query = query.Where(p => p.NumberOfBedrooms == Convert.ToInt32(Bedrooms));
            }
            if (!string.IsNullOrEmpty(City))
            {
                query = query.Where(p => p.City != null && p.City.ToLower() == City.ToLower());
            }
            if (!string.IsNullOrEmpty(PropertyType))
            {
                query = query.Where(p => p.PropertyType != null 
                    && p.PropertyType.ToLower() == PropertyType.ToLower());
            }
            if (!string.IsNullOrEmpty(IsFurnished))
            {
                query = query.Where(p => p.Furnished != null && p.Furnished.ToLower() == IsFurnished.ToLower());
            }
            FilteredList = query.ToList();
        }

        protected override bool DoValidate()
        {
            return Properties != null && Properties.Count > 0;
        }
    }
}

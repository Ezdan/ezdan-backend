﻿using System;
using System.Collections.Generic;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ezdan_logger;
using ezdan_business.Helper;

namespace ezdan_business.Commands.Property
{
    public class GetPropertyFiltersCommand : BaseCommand
    {
        public static JObject Filters;
        public GetPropertyFiltersCommand() {

        }
        static GetPropertyFiltersCommand()
        {
            Filters = new JObject();
        }
        protected override void DoAction()
        {
            try
            {
                LogManager.Log(LogType.trace, "getting filters");

                //Check by referencing main property command next sync time, without incrementing
                //if (LoadPropertyCommand._nextSyncDateTime == null ||
                //    (LoadPropertyCommand._nextSyncDateTime != null
                //    && LoadPropertyCommand._nextSyncDateTime < DateTime.Now))
                //{
                    LogManager.Log(LogType.trace, "updating filters");
                    ezdanProperties.PropertiesWSSoapClient svcClient = new ezdanProperties.PropertiesWSSoapClient();
                    JObject LocationFilters = JObject.Parse(svcClient.GetValuesForMobile(ezdanProperties.PropertyValueType.PropertyLocation));
                    JObject TypeFilters = JObject.Parse(svcClient.GetValuesForMobile(ezdanProperties.PropertyValueType.PropertyType));
                    JObject FurnishingFilters = JObject.Parse(svcClient.GetValuesForMobile(ezdanProperties.PropertyValueType.PropertyFurnishing));
                    Filters.Add("Locations", LocationFilters["Table"]);
                    Filters.Add("Types", TypeFilters["Table"]);
                    Filters.Add("Furnishings", FurnishingFilters["Table"]);
                //}

            }
            catch (Exception ex)
            {
                LogManager.Log(LogType.error, ex.Message, ex);
            }
        }

        protected override bool DoValidate()
        {
            return true;
        }
    }
}

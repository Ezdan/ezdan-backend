﻿using ezdan_business.Helper;
using ezdan_logger;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace ezdan_business.Commands.Property
{
    public class GetPropertyCommand : BaseCommand
    {
        public string buildingNumber { get; set; }
        public string deviceId { get; set; }
        public ezdan_entities.Models.Property Property { get; set; }
        private List<ezdan_entities.Models.Property> Properties;
        public GetPropertyCommand()
        {
            Properties = new List<ezdan_entities.Models.Property>();
            PreExecute += GetPropertyCommand_PreExecute;
            Property = new ezdan_entities.Models.Property();
        }

        private void GetPropertyCommand_PreExecute()
        {
            LoadPropertyCommand loadPropertyCommand = new LoadPropertyCommand();
            loadPropertyCommand.Execute();
            Properties = LoadPropertyCommand.EzdanProperties;
        }

        protected override void DoAction()
        {
            try
            {
                //Get property data from list
                Property = Properties.Where(p => p._DocumnetHash == buildingNumber).FirstOrDefault();
                //get property images from service
                ezdanProperties.PropertiesWSSoapClient svcClient = new ezdanProperties.PropertiesWSSoapClient();
                ezdanProperties.ArrayOfString propertyImages = svcClient.GetPropertyImages(Property.PropertyGroup);
                if(deviceId != null)
                {
                    List<string> tempImages = new List<string>();
                    foreach (var image in propertyImages)
                    {
                        string tempImageName = "property-" + buildingNumber + "-image-" + new Random().Next(10000000) + "-client-" + deviceId;
                        string tempImage = ImageHelper.DownloadImage(image, "temp", tempImageName);
                        if (string.IsNullOrEmpty(tempImage))
                        {
                            tempImages.Add(image);
                        }
                        else
                        {
                            tempImages.Add(tempImage);
                        }
                    }
                    Property.PropertyImages = tempImages.ToList();
                }
                else
                {
                    Property.PropertyImages = propertyImages.ToList();
                }
                #region testdata
                //Property.PropertyImages = new List<string>() {
                //    "http://ezdanrealestate.qa//images/properties2/EB00/thumbnail-EB00.jpg",
                //    "http://ezdanrealestate.qa//images/properties2/EB01/thumbnail-EB01.jpg",
                //    "http://ezdanrealestate.qa//images/properties2/EB00/thumbnail-EB00.jpg",
                //    "http://ezdanrealestate.qa//images/properties2/EB01/thumbnail-EB01.jpg" };
                #endregion
            }
            catch (Exception ex)
            {
                LogManager.Log(LogType.error, ex.Message, ex);
            }
        }

        protected override bool DoValidate()
        {
            return !string.IsNullOrEmpty(buildingNumber);
        }
    }
}

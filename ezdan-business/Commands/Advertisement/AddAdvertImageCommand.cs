﻿using ezdan_business.Helper;
using ezdan_entities;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ezdan_business.Commands.Advertisement
{
    public class AddAdvertImageCommand : BaseCommand
    {
        private DataHelper db;
        public HttpPostedFile file;
        public string filename;
        public string foldername;
        public int classifiedId;
        public string userId;
        private string DBImageURL;
        public JObject result;
        private ContentProperty advImage;

        public AddAdvertImageCommand()
        {
            db = new DataHelper();
            advImage = new ContentProperty();
            result = new JObject();
        }

        protected override void DoAction()
        {

            DBImageURL = ImageHelper.SaveImage(file, filename, foldername, userId);

            advImage.ContentId = classifiedId;
            advImage.PropertyId = 3; //wrong, should be dynamic
            advImage.EnglishValue = DBImageURL;
            advImage.ArabicValue = DBImageURL;
            db.DbEntities.ContentProperties.Add(advImage);

            result.Add("status", db.SaveChanges().ToString());
            result.Add("imageURL", DBImageURL);
        }

        protected override bool DoValidate()
        {
            return true;
        }
    }
}

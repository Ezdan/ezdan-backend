﻿using ezdan_business.Helper;
using ezdan_entities;
using ezdan_entities.Helper;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ezdan_business.Commands.Advertisement
{
    public class GetCategoriesListCommand : BaseCommand
    {
        public AppLanguage language;
        public JArray result = new JArray();


        private DataHelper db;

        public GetCategoriesListCommand()
        {
            db = new DataHelper();
        }
        protected override void DoAction()
        {
            db.DbEntities.Configuration.LazyLoadingEnabled = true;
            List<Content> categories = db.DbEntities.Contents.Where(x => x.ParentId == 4).ToList();
            foreach(Content category in categories)
            {
                JObject categoryObj = new JObject();
                categoryObj.Add("id", category.ContentId);
                string nameValue = language == AppLanguage.Arabic ? category.ArabicName : category.EnglishName;
                categoryObj.Add("name", nameValue);

                result.Add(categoryObj);

            }
            db.DbEntities.Configuration.LazyLoadingEnabled = false;
        }

        protected override bool DoValidate()
        {
            return true;
        }
    }
}

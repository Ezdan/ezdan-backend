﻿using ezdan_business.Helper;
using ezdan_entities;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ezdan_business.Commands.Advertisement
{
    public class ReportAdvertisementCommand : BaseCommand
    {
        public int advId;
        private DataHelper db;
        public string UId;
        public JObject result;
        private UserReportContent reportedContent = new UserReportContent();

        public ReportAdvertisementCommand()
        {
            db = new DataHelper();
            result = new JObject();
        }

        protected override void DoAction()
        {
            Content advertisement = db.DbEntities.Contents.Where(x => x.ContentId == advId).FirstOrDefault();
            if (advertisement != null) {
                advertisement.Reported = true;
                reportedContent.ContentId = advId;
                reportedContent.UserId = UId;
                db.DbEntities.UserReportContents.Add(reportedContent);
                if (db.SaveChanges())
                {
                    result.Add("reported", true);
                } else
                {
                    result.Add("reported", false);
                }
            }

        }

        protected override bool DoValidate()
        {
            UserReportContent checkReport = db.DbEntities.UserReportContents.Where(x => x.ContentId == advId && x.UserId == UId).FirstOrDefault();
            if(checkReport == null)
            {
                return true;
            } else
            {
                result.Add("reported", false);
                return false;
            }
        }
    }
}

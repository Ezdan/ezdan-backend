﻿using ezdan_business.Helper;
using ezdan_entities;
using ezdan_entities.Helper;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ezdan_business.Commands.Advertisement
{
    public class GetUserAdvertisementsCommand : BaseCommand
    {
        public string UId;
        private DataHelper db;
        public AppLanguage language;
        public JArray result = new JArray();
        //private JArray result;
        public List<Content> advertisements = new List<Content>();

        public GetUserAdvertisementsCommand()
        {
            db = new DataHelper();
        }

        protected override void DoAction()
        {
            //db.DbEntities.Configuration.LazyLoadingEnabled = true;
            advertisements = db.DbEntities.Contents.Where(x => x.CreatedBy == UId).ToList();
            foreach (Content adv in advertisements)
            {
                JObject advObj = new JObject();
                advObj.Add("category_id", adv.Content2.ContentId);
                var categoryNameValue = language == AppLanguage.Arabic ? adv.Content2.ArabicName : adv.Content2.EnglishName;
                advObj.Add("category_name", categoryNameValue);
                JObject advertisement = new JObject();
                advertisement.Add("id",adv.ContentId);
                var advertisementNameValue = language == AppLanguage.Arabic ? adv.ArabicName : adv.EnglishName;
                advertisement.Add("name", advertisementNameValue);
                advertisement.Add("posted_on", adv.CreatedOn);
                JObject props = new JObject();
                JArray images = new JArray();
                foreach(ContentProperty prop in adv.ContentProperties)
                {
                    if (prop.Property.Name == "image")
                    {
                        JObject image = new JObject();
                        var imgsrc = language == AppLanguage.Arabic ? prop.ArabicValue : prop.EnglishValue;

                        image.Add("src", imgsrc);
                        images.Add(image);
                    }
                    else
                    {
                        var propValue = language == AppLanguage.Arabic ? prop.ArabicValue : prop.EnglishValue;
                        props.Add(prop.Property.Name, propValue);
                    }
                }
                if(images.Count != 0)
                    props.Add("images", images);
                advertisement.Add("properties", props);
                advObj.Add("advertisement", advertisement);
                result.Add(advObj);
                //    JObject categoryObj = new JObject();
                //    categoryObj.Add("id", category.ContentId);
                //    string nameValue = language == AppLanguage.Arabic ? category.ArabicName : category.EnglishName;
                //    categoryObj.Add("name", nameValue);

                //    result.Add(categoryObj);

            }
            //db.DbEntities.Configuration.LazyLoadingEnabled = false;
        }

        protected override bool DoValidate()
        {
            return true;
        }
    }
}

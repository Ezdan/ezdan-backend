﻿using ezdan_business.Helper;
using ezdan_entities;
using ezdan_logger;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ezdan_business.Commands.Advertisement
{
    public class EditClassifiedCommand : BaseCommand
    {
        private Entities db;
        public Content content;
        public List<ContentProperty> contentProps;
        public List<ContentProperty> deletedImages;

        public JObject result;


        public EditClassifiedCommand()
        {
            db = new Entities();
            content = new Content();
            contentProps = new List<ContentProperty>();
            deletedImages = new List<ContentProperty>();
            result = new JObject();
        }

        protected override void DoAction()
        {
            try
            {
                db.Entry(content).State = EntityState.Modified;

                foreach (var prop in contentProps)
                {
                    db.Entry(prop).State = EntityState.Modified;
                }

                if (deletedImages != null)
                {
                    foreach(ContentProperty img in deletedImages)
                    {
                        string imgPath = img.EnglishValue;
                        int id = img.ContentPropertyId;

                        int startIndex = imgPath.IndexOf("Assets");
                        int endIndex = imgPath.Length - startIndex;
                        String substring = imgPath.Substring(startIndex, endIndex);
                        if (substring != null)
                        {
                            if (System.IO.File.Exists(HttpContext.Current.Server.MapPath("~/" + substring)))
                                System.IO.File.Delete(HttpContext.Current.Server.MapPath("~/" + substring));
                            
                        }
                        ContentProperty imgObj = db.ContentProperties.Find(id);
                        db.ContentProperties.Remove(imgObj);
                    }
                }

                db.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ezdan_logger.LogManager.Log(LogType.trace, "Property: {0} Error: {1} " + validationError.PropertyName.ToString() + " " + validationError.ErrorMessage.ToString());
                    }
                }
            }
        }

        protected override bool DoValidate()
        {
            //data validation missing
            return true;
        }
    }
}

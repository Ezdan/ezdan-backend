﻿using ezdan_business.Helper;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ezdan_business.Commands.Advertisement
{
    public class CreateNewAdvertisementCommand : BaseCommand
    {
        public string price;
        public int categoryId;
        public string postedBy;
        public DateTime postedOn;
        public string ArabicName;
        public string EnglishName;
        public string englishDescription;
        public string arabicDescription;
        public string phone;
        public string email;
        public string ifSold;
        public bool reported;
        public bool blocked;

        public JObject result = new JObject();

        private DataHelper db;
        private ezdan_entities.Content advertisement = new ezdan_entities.Content();
        private ezdan_entities.ContentProperty advPrice = new ezdan_entities.ContentProperty();
        private ezdan_entities.ContentProperty advDescription = new ezdan_entities.ContentProperty();
        private ezdan_entities.ContentProperty advContactPhone = new ezdan_entities.ContentProperty();
        private ezdan_entities.ContentProperty advContactEmail = new ezdan_entities.ContentProperty();
        private ezdan_entities.ContentProperty advifSold = new ezdan_entities.ContentProperty();

        public CreateNewAdvertisementCommand()
        {
            db = new DataHelper();
        }

        protected override void DoAction()
        {
            advertisement.ArabicName = ArabicName;
            advertisement.EnglishName = EnglishName;
            advertisement.CreatedBy = postedBy;
            advertisement.CreatedOn = postedOn;
            advertisement.Blocked = blocked;
            advertisement.Reported = reported;
            advertisement.ContentType = "classifieddetail";
            advertisement.ParentId = categoryId;
            db.DbEntities.Contents.Add(advertisement);

            advPrice.ContentId = advertisement.ContentId;
            advPrice.PropertyId = 11; //wrong, should be dynamic
            advPrice.EnglishValue = price;
            advPrice.ArabicValue = price;
            db.DbEntities.ContentProperties.Add(advPrice);

            advDescription.ContentId = advertisement.ContentId;
            advDescription.PropertyId = 1; //wrong, should be dynamic
            advDescription.EnglishValue = englishDescription;
            advDescription.ArabicValue = arabicDescription;
            db.DbEntities.ContentProperties.Add(advDescription);
            
            advContactEmail.ContentId = advertisement.ContentId;
            advContactEmail.PropertyId = 9; //wrong, should be dynamic
            advContactEmail.EnglishValue = email;
            advContactEmail.ArabicValue = email;
            db.DbEntities.ContentProperties.Add(advContactEmail);

            advContactPhone.ContentId = advertisement.ContentId;
            advContactPhone.PropertyId = 10; //wrong, should be dynamic
            advContactPhone.EnglishValue = phone;
            advContactPhone.ArabicValue = phone;
            db.DbEntities.ContentProperties.Add(advContactPhone);

            advifSold.ContentId = advertisement.ContentId;
            advifSold.PropertyId = 8; //wrong, should be dynamic
            advifSold.EnglishValue = ifSold;
            advifSold.ArabicValue = ifSold;
            db.DbEntities.ContentProperties.Add(advifSold);


            result.Add("status", db.SaveChanges().ToString());
            result.Add("classifiedId", advertisement.ContentId);
        }

        protected override bool DoValidate()
        {
            //data validation missing
            return true;
        }
    }
    }

﻿using ezdan_business.EDCOffersWs;
using ezdan_logger;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ezdan_business.Commands.Card
{
    public class GetAllCardOffersCommand : BaseCommand
    {
        public static object EDCOffers { get; set; }
        private static DateTime _nextAllCardsSyncDateTime { get; set; }
        protected override void DoAction()
        {
            try
            {
                if(_nextAllCardsSyncDateTime != null && _nextAllCardsSyncDateTime < DateTime.Now)
                {
                    LogManager.Log(LogType.trace, string.Format("Getting From Service @ {0}", _nextAllCardsSyncDateTime));
                    _nextAllCardsSyncDateTime = DateTime.Now.AddDays(1);
                    EDCWSSoapClient wsClient = new EDCWSSoapClient();
                    EDCOffers = JsonConvert.DeserializeObject(wsClient.GetAllEDCOffers());
                }
                LogManager.Log(LogType.trace, string.Format("Ezdan Offers Length {0}", EDCOffers.ToString().Length));
            }
            catch (Exception ex)
            {
                LogManager.Log(LogType.error, ex.Message, ex);
                _nextAllCardsSyncDateTime = DateTime.Now;
                EDCOffers = "";
            }
        }

        protected override bool DoValidate()
        {
            return true;
        }
    }
}

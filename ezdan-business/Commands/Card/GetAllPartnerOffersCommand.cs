﻿using ezdan_business.EDCOffersWs;
using ezdan_logger;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ezdan_business.Commands.Card
{
    public class GetAllPartnerOffersCommand : BaseCommand
    {
        public static object PartnerOffers { get; set; }
        private static DateTime _nextPartnersCardsSyncDateTime { get; set; }

        protected override void DoAction()
        {
            try
            {
                if (_nextPartnersCardsSyncDateTime != null && _nextPartnersCardsSyncDateTime < DateTime.Now)
                {
                    LogManager.Log(LogType.trace, string.Format("Getting From Service @ {0}", _nextPartnersCardsSyncDateTime));
                    _nextPartnersCardsSyncDateTime = DateTime.Now.AddDays(1);
                    EDCWSSoapClient wsClient = new EDCWSSoapClient();
                    PartnerOffers = JsonConvert.DeserializeObject(wsClient.GetAllPartnerOffers());
                }
                LogManager.Log(LogType.trace, string.Format("Partner Offers Length {0}", PartnerOffers.ToString().Length));
            }
            catch (Exception ex)
            {
                LogManager.Log(LogType.error, ex.Message, ex);
                _nextPartnersCardsSyncDateTime = DateTime.Now;
                PartnerOffers = "";
            }
        }

        protected override bool DoValidate()
        {
            return true;
        }
    }
}

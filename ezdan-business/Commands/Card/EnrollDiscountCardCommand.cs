﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ezdan_entities;
using Newtonsoft.Json.Linq;
using System.Web;
using System.Net.Mail;
using System.Configuration;
using System.Diagnostics;
using Newtonsoft.Json;
using ezdan_logger;
using ezdan_business.Helper;
using ezdan_entities.Helper;
using ezdan_business.Commands.CMS;
using System.Text;
using System.Threading.Tasks;
namespace ezdan_business.Commands.Card
{
    public class EnrollDiscountCardCommand : BaseCommand
    {
        public string UserId { get; set; }
        public string ContactNumber { get; set; }
        public string Category { get; set; }
        public bool Result { get; set; }
        DataHelper db;
        public EnrollDiscountCardCommand()
        {
            db = new DataHelper();
        }
        protected override void DoAction()
        {
            UsersDiscountCard uDiscountCard = new UsersDiscountCard();
            uDiscountCard.UserId = UserId;
            uDiscountCard.ContactNumber = ContactNumber;
            uDiscountCard.CreateDate = DateTime.Now;
            uDiscountCard.AspNetUser = db.DbEntities.AspNetUsers.FirstOrDefault(u => u.Id == UserId);
            db.DbEntities.UsersDiscountCards.Add(uDiscountCard);
            Result =  db.SaveChanges();
            GetContentCommand mailNotifications = new GetContentCommand();
            mailNotifications.language = AppLanguage.English;
            mailNotifications.contentId = 6;
            mailNotifications.Execute();
            string EDCEmail = db.DbEntities.ContentProperties.Where(cps => cps.ContentId == 6 && cps.PropertyId == 35).Select(cps => cps.EnglishValue).FirstOrDefault();
            if(string.IsNullOrEmpty(EDCEmail))
            {
                EDCEmail = SettingHelper.GetAppSettingValue("EDCEmail");
            }
            string FileName = HttpContext.Current.Server.MapPath("~/Views/MailTemplates/cardsEnrollmentTemplate.html");
            string MailBody = System.IO.File.ReadAllText(FileName);
            MailBody = MailBody.Replace("##ID##", Convert.ToString(uDiscountCard.Id));
            MailBody = String.IsNullOrEmpty(Category) ? MailBody.Replace("##CATEGORY##", "Not specified") : MailBody.Replace("##CATEGORY##", Category);
            MailBody = MailBody.Replace("##NAME##", uDiscountCard.AspNetUser.FullName);
            MailBody = MailBody.Replace("##EMAIL##", uDiscountCard.AspNetUser.Email);
            MailBody = MailBody.Replace("##MOBILE##", uDiscountCard.AspNetUser.PhoneNumber);
            MailBody = MailBody.Replace("##DATE##", DateTime.Now.ToString());
            MailHelper.SendEmail(new ezdan_entities.Models.EmailNotification
            {
                EmailTo = EDCEmail,
                Subject = "New Ezdan Discount Cards Enrollment",
                Body = MailBody
            });
        }

        protected override bool DoValidate()
        {
            return !string.IsNullOrEmpty(ContactNumber);
        }
    }
}

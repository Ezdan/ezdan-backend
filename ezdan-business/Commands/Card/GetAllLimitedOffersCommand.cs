﻿using ezdan_business.EDCOffersWs;
using ezdan_logger;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ezdan_business.Commands.Card
{
    public class GetAllLimitedOffersCommand : BaseCommand
    {
        public static object LimitedOffers { get; set; }
        private static DateTime _nextLimitedCardsSyncDateTime { get; set; }

        protected override void DoAction()
        {
            try
            {
                if (_nextLimitedCardsSyncDateTime != null && _nextLimitedCardsSyncDateTime < DateTime.Now)
                {
                    LogManager.Log(LogType.trace, string.Format("Getting From Service @ {0}", _nextLimitedCardsSyncDateTime));
                    _nextLimitedCardsSyncDateTime = DateTime.Now.AddDays(1);
                    EDCWSSoapClient wsClient = new EDCWSSoapClient();
                    LimitedOffers = JsonConvert.DeserializeObject(wsClient.GetAllLimitedOffers());
                }

                LogManager.Log(LogType.trace, string.Format("Limited Offers Length {0}", LimitedOffers.ToString().Length));
            }
            catch (Exception ex)
            {
                LogManager.Log(LogType.error, ex.Message, ex);
                _nextLimitedCardsSyncDateTime = DateTime.Now;
                LimitedOffers = "";
            }
        }

        protected override bool DoValidate()
        {
            return true;
        }
    }
}

﻿using ezdan_business.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;

namespace ezdan_business.Commands.Search
{
    public class SearchCommand : BaseCommand
    {
        DataHelper db;
        public ezdan_entities.PlusInquiry plusInquiry { get; set; }
        public string Keyword { get; set; }
        public string usertType { get; set; }
        public bool SearchedInEnglish { get; set; }
        public JArray Results { get; set; }
        public SearchCommand()
        {
            db = new DataHelper();
            SearchedInEnglish = false;
            Results = new JArray();
            Keyword = "";
            usertType = "";
        }
        protected override void DoAction()
        {
            if (Regex.IsMatch(Keyword, @"^[a-zA-Z0-9-\s]+$"))
            {
                SearchedInEnglish = true;
            }
            var Results = db.DbEntities.Contents.Where(cts => (cts.EnglishName.Contains(Keyword) || cts.ArabicName.Contains(Keyword)) && cts.ParentId != null).ToList();
            if (Results != null)
            {
                foreach (var Result in Results)
                {
                    var ResultObj = new JObject();
                    if (Result.Content2.ParentId == 4 || Result.ContentId == 4 || Result.ParentId == 4)
                    {
                        if (usertType == "loggedIn")
                        {
                            ResultObj.Add("Href", "#/content/" + Result.ContentId);
                            ResultObj.Add("EnglishName", Result.EnglishName);
                            ResultObj.Add("ArabicName", Result.ArabicName);
                        }
                    }
                    else if (Result.ParentId == 1
                        || Result.ParentId == 3
                        || Result.ParentId == 5
                       )
                    {
                        ResultObj.Add("Href", "#/content/" + Result.ParentId);
                        ResultObj.Add("EnglishName", "(" + Result.Content2.EnglishName + ") " + Result.EnglishName);
                        ResultObj.Add("ArabicName", "(" + Result.Content2.ArabicName + ") " + Result.ArabicName);
                    }
                    else if (Result.ParentId == 8
                          || Result.ParentId == 9
                          || Result.ParentId == 24
                          || Result.ParentId == 26
                          || Result.ParentId == 25
                      )
                    {
                        ResultObj.Add("Href", "#/content/" + Result.Content2.ParentId);
                        ResultObj.Add("EnglishName", "(" + Result.Content2.EnglishName + ") " + Result.EnglishName);
                        ResultObj.Add("ArabicName", "(" + Result.Content2.ArabicName + ") " + Result.ArabicName);
                    }
                    else
                    {
                        ResultObj.Add("Href", "#/content/" + Result.ContentId);
                        ResultObj.Add("EnglishName", Result.EnglishName);
                        ResultObj.Add("ArabicName", Result.ArabicName);

                    }
                    if (ResultObj["Href"] != null)
                    {
                        this.Results.Add(ResultObj);

                    }
                }
            }
        }

        protected override bool DoValidate()
        {
            return Keyword != null;
        }
    }
}

﻿using ezdan_business.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ezdan_business.Commands.Survey
{
    public class GetSurveyQuestionsPerLocation : BaseCommand
    {
        public int surveyId { get; set; }
        public String sessionKey { get; set; }
        public String jsonResponse { get; set; }
        protected override void DoAction()
        {
            String url = SettingHelper.GetAppSettingValue("SurveyAPI") + "Survey/" + surveyId + "?session_key=" + sessionKey;
            WebClient client = new WebClient();
            client.Headers[HttpRequestHeader.ContentType] = "application/json";
            client.Headers.Add("Authorization: Basic " + Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(SettingHelper.GetAppSettingValue("SurveyUserName"))));
            client.Encoding = System.Text.Encoding.UTF8;
            jsonResponse = client.DownloadString(url);

        }

        protected override bool DoValidate()
        {
            return true;
        }
    }
}

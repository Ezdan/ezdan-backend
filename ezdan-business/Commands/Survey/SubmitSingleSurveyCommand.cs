﻿using ezdan_business.Helper;
using ezdan_entities;
using ezdan_logger;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;


namespace ezdan_business.Commands.Survey
{
    //check how the data will be sent from the client then tokenize the data and send it to the API.
    public class SubmitSingleSurveyCommand : BaseCommand
    {
        DataHelper db;
        public string userId { get; set; }
        public JObject jsonWithSurveyData { get; set; }
        public bool saveResult { get; set; }
        public SubmitSingleSurveyCommand()
        {
            db = new DataHelper();
        }
        protected override void DoAction()
        {
            
            try
            {
                string url = SettingHelper.GetAppSettingValue("SurveyAPI") + "Survey";

                LogManager.Log(LogType.trace, jsonWithSurveyData.ToString());
                LogManager.Log(LogType.trace, url);

                WebClient client = new WebClient();
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                client.Headers.Add("Authorization: Basic " + Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(SettingHelper.GetAppSettingValue("SurveyUserName"))));
                var data = JsonConvert.DeserializeObject(jsonWithSurveyData.ToString()) + "";
                //data = data.Replace("\"","'");

                object result = null;
                try
                {
                    result = client.UploadString(url, "post", data);
                }
                catch(Exception ex)
                {
                    saveResult = false;
                    LogManager.Log(LogType.error, "error uploading survey data. " + ex.Message, ex);
                    return;
                }
                

                //if condition
                if (result != null)
                {
                    UserTakenSurvey userSurvey = new UserTakenSurvey();
                    var Json = (JObject)JsonConvert.DeserializeObject(data.ToString());
                    int surveyId = Convert.ToInt16(Json["survey_id"]);
                    JToken otherInfo = Json["other_info"];
                    int locationId = Convert.ToInt16(otherInfo["location_id"]);
                    userSurvey.UserId = userId;
                    userSurvey.SurveyId = surveyId.ToString();
                    userSurvey.LocationId = locationId.ToString();
                    db.DbEntities.UserTakenSurveys.Add(userSurvey);
                    saveResult = db.SaveChanges();
                }
            }
            catch(Exception e)
            {
                saveResult = false;
                LogManager.Log(LogType.error, e.Message, e);
            }
        }

        protected override bool DoValidate()
        {
            return true;
        }
    }
}

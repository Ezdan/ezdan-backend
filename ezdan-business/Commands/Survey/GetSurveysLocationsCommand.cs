﻿using ezdan_business.Helper;
using System;
using System.Configuration;
using System.Net;

namespace ezdan_business.Commands.Survey
{
    public class GetSurveysLocationsCommand : BaseCommand
    {
        public string SurveyResult { get; set; }
            
        protected override void DoAction()
        {

            string url = SettingHelper.GetAppSettingValue("SurveyAPI") + "Login";
            WebClient client = new WebClient();
            client.Headers[HttpRequestHeader.ContentType] = "application/json";
            var data = "{ 'username': 'user',  'password': 'pass' }";
            var result = client.UploadString(url, "post", data);
            SurveyResult = result;
        }

        protected override bool DoValidate()
        {
            return true;
        }
    }
}
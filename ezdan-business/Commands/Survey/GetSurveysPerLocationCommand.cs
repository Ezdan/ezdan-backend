﻿using ezdan_business.Helper;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;

using System.Net;
namespace ezdan_business.Commands.Survey
{
    public class GetSurveysPerLocationCommand : BaseCommand
    {
        DataHelper db;
        public String surveys { get; set; }
        public String userId { get; set; }
        public int? locationId { get; set; }
        public String sessionKey { get; set; }
        public JObject x { get; set; }

        public GetSurveysPerLocationCommand()
        {
            db = new DataHelper();
        }
        protected override void DoAction()
        {
            String url = SettingHelper.GetAppSettingValue("SurveyAPI") + "Location/" + locationId;
            WebClient client = new WebClient();
            client.Headers[HttpRequestHeader.ContentType] = "application/json";
            var data = "{'session_key':" + sessionKey + "}";
            client.Encoding = System.Text.Encoding.UTF8;
            var result = client.UploadString(url, "post", data);
            JObject returnObj = JObject.Parse(result);
            JToken resSurveys = returnObj["surveys"];
            JArray jsonArrayOfSurveys = new JArray();
            foreach (JObject survey in resSurveys)
            {
                string surveyID = survey["id"].ToString();
                var userSurveys = db.DbEntities.UserTakenSurveys.Where(u => u.UserId == userId && u.SurveyId == surveyID && u.LocationId == locationId.ToString()).FirstOrDefault();
                bool taken = userSurveys != null;
                survey.Add("taken", taken);

                jsonArrayOfSurveys.Add(survey);
            }
             x = new JObject(new JProperty("surveys", jsonArrayOfSurveys));
        }

        protected override bool DoValidate()
        {
            return true;
        }
    }
}

﻿using ezdan_business.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using ezdan_entities;
using Newtonsoft.Json.Linq;
using System.Diagnostics;
using System.Dynamic;
using ezdan_entities.Helper;

namespace ezdan_business.Commands.CMS
{
    public class GetContentCommand : BaseCommand
    {
        public int contentId;
        public  AppLanguage language;
        public JObject result;


        private DataHelper db;

        public GetContentCommand()
        {
            db = new DataHelper();
        }
        protected override void DoAction()
        {
            Content content = db.DbEntities.Contents.Where(x => x.ContentId == contentId).FirstOrDefault();
            List<Content> children = content.Content1.ToList();
            Content parent = content.Content2;
            result = ContentHelper.CreateContent(parent, content, children, language);
        }

        protected override bool DoValidate()
        {
            return true;
        }
    }
}

﻿using ezdan_business.Helper;
using ezdan_entities.Helper;
using ezdan_logger;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;

namespace ezdan_business.Commands.News
{
    public class GetNewsCommand : BaseCommand
    {
        public JObject allNewsResult { get; set; }
        public int id { get; set; }
        public int lang;

        protected override void DoAction()
        {
            string html = string.Empty;
            string JSONresult = string.Empty;
            //Regex yourRegex = new Regex(@"<[^>]+>|&nbsp");
            Regex yourRegex = new Regex(@"<(?!br[\x20/>])[^<>]+>");
            //Regex yourRegex = new Regex(@"<[^>]+>");
            //Regex yourRegex = new Regex(@"<pre>|</pre>");
            string url = SettingHelper.GetAppSettingValue("NewsAPI") + lang + "&id=" + id;

            try {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.AutomaticDecompression = DecompressionMethods.GZip;
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    html = reader.ReadToEnd();
                    JSONresult = yourRegex.Replace(html, "");
                    allNewsResult = JObject.Parse(JSONresult);
                }
            } catch (Exception ex)
            {
                LogManager.Log(LogType.error, ex.Message, ex);
                allNewsResult = null;
            }

        }

        protected override bool DoValidate()
        {
            return true;
        }
    }
}

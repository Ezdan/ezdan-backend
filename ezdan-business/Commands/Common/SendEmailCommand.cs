﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ezdan_entities.Models;
using System.Net.Mail;
using System.Net;
using ezdan_logger;

namespace ezdan_business.Commands.Common
{
    public class SendEmailCommand : BaseCommand
    {
        public EmailNotification Email { get; internal set; }
        public bool SendResult { get; internal set; }
        protected override void DoAction()
        {
            try
            {
                Attachment att = null;
                var message = new MailMessage(Email.Configuration.MailSender,
                    Email.EmailTo, Email.Subject, Email.Body)
                { IsBodyHtml = true };
                var smtp = new SmtpClient(Email.Configuration.Host, Email.Configuration.Port);


                if (Email.Configuration.SMTPEmail.Length > 0 && Email.Configuration.SMTPPassword.Length > 0)
                {
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new NetworkCredential(Email.Configuration.MailSender, Email.Configuration.SMTPPassword);
                    smtp.EnableSsl = Email.Configuration.EnableSSL;
                }

                smtp.Send(message);

                if (att != null)
                    att.Dispose();
                message.Dispose();
                smtp.Dispose();
                SendResult = true;
            }

            catch (Exception ex)
            {
                LogManager.Log(LogType.error, "Sending Email : " + ex.Message, ex);
                SendResult = false;
            }
        }

        protected override bool DoValidate()
        {
            return Email.Configuration != null;
        }
        
        
    }
}

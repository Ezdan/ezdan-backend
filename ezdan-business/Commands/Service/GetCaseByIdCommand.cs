﻿using ezdan_business.Helper;
using ezdan_business.PITMobileIntergration;
using System;


namespace ezdan_business.Commands.Service
{
    class GetCaseByIdCommand : BaseCommand
    {
        private string SVCUsername;
        private string SVCPassword;

        PITCaseServiceClient client;
        CallContext context;

        public GetCaseByIdCommand()
        {
            SVCUsername = SettingHelper.GetAppSettingValue("SVCUsername").ToString();
            SVCPassword = SettingHelper.GetAppSettingValue("SVCPassword").ToString();

            client = new PITCaseServiceClient();
            context = new CallContext();
            context.Company = "ERE";
            client.ClientCredentials.Windows.ClientCredential.UserName = SVCUsername;
            client.ClientCredentials.Windows.ClientCredential.Password = SVCPassword;
        }

        protected override void DoAction()
        {
            PITCaseEntity[] cases = client.ListCase(context, "CaseId");
            foreach (PITCaseEntity item in cases)
            {
                Console.WriteLine(item.parmCaseId + " " + item.parmParty);
            }
        }

        protected override bool DoValidate()
        {
            return true;
        }
    }
}

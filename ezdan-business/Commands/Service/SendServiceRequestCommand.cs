﻿using ezdan_business.Helper;
using ezdan_business.PITMobileIntergration;
using ezdan_logger;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ezdan_business.Commands.Service
{
    public class SendServiceRequestCommand : BaseCommand
    {
        public long parmCategoryRecId { get; set; }
        public long? parmParty { get; set; }
        public string parmDescription { get; set; }
        public string parmEmail { get; set; }
        public string parmPhoneNumber { get; set; }
        public string caseId { get; set; }
        public string parmPITBuildingUnitId { get; set; }
        public DateTime parmPITAvailableFrom { get; set; }
        public DateTime parmPITAvailableTo { get; set; }
        public int language;
        public JObject JSONcaseId;

        public JArray files;
        public string filename;
        public string foldername;
        public string userId;
        private string DBImageURL;

        private string SVCUsername;
        private string SVCPassword;
        
        PITCaseServiceClient client;
        CallContext context;

        public SendServiceRequestCommand()
        {
            SVCUsername = SettingHelper.GetAppSettingValue("SVCUsername").ToString();
            SVCPassword = SettingHelper.GetAppSettingValue("SVCPassword").ToString();

            client = new PITCaseServiceClient();
            context = new CallContext();
            JSONcaseId = new JObject();
            parmCategoryRecId = -1;
            files = null;
            DBImageURL = null;

            context.Company = "ERE";
            client.ClientCredentials.Windows.ClientCredential.UserName = SVCUsername;
            client.ClientCredentials.Windows.ClientCredential.Password = SVCPassword;

        }

        protected override void DoAction()
        {
            try {
                PITCaseEntity caseObj = new PITCaseEntity();
                caseObj.parmCategoryRecId = parmCategoryRecId;
                caseObj.parmParty = parmParty.HasValue ? parmParty.Value : 0;
                caseObj.parmEmailId = parmEmail;
                caseObj.parmPITPhoneNumber = parmPhoneNumber;
                caseObj.parmDescription = parmDescription;
                caseObj.parmPITBuildingUnitId = parmPITBuildingUnitId;
                caseObj.parmPITAvailableFrom = parmPITAvailableFrom;
                caseObj.parmPITAvailableTo = parmPITAvailableTo;
                //What other fields need to be initialized???
                if(files != null && files.Count > 0)
                {
                    for(var i = 0; i < files.Count; i++)
                    {
                        switch(i)
                        {
                            case 0:
                                caseObj.parmFirstImage = files[i].ToString();
                                break;
                            case 1:
                                caseObj.parmSecondImage = files[i].ToString();
                                break;
                            case 2:
                                caseObj.parmThirdImage = files[i].ToString();
                                break;
                        }
                    }
                    //DBImageURL = ImageHelper.SaveImage(file, filename, foldername, userId);
                    //if(DBImageURL != null)
                    //{
                    //    //caseObj.parmPITImagesURL = DBImageURL;
                    //}
                }
                caseId = client.CreateCase(context, caseObj);
                JSONcaseId.Add("caseObj", JsonConvert.SerializeObject(caseObj));
                JSONcaseId.Add("caseId", caseId);
                //JSONcaseId.Add("imageURL", DBImageURL != null ? DBImageURL : null);
                //add case to UserServiceRequest
                string content = language == 1 ? SettingHelper.GetAppSettingValue("SMSCaseEnglish") : SettingHelper.GetAppSettingValue("SMSCaseArabic");
                SMSHelper.SendSMS(SettingHelper.GetAppSettingValue("SMSExtension") + parmPhoneNumber, content  + caseId); 
            } catch (Exception ex)
            {
                LogManager.Log(LogType.error, ex.Message, ex);
                JSONcaseId = null;
            }
        }

        protected override bool DoValidate()
        {
            return (!long.Equals(parmCategoryRecId , - 1) 
                && !string.IsNullOrEmpty(parmDescription)
                && parmPITAvailableFrom != DateTime.MinValue
                && parmPITAvailableFrom > DateTime.Now
                && parmPITAvailableTo != DateTime.MinValue
                && parmPITAvailableTo > DateTime.Now
                && parmPITAvailableTo > parmPITAvailableFrom
            );
        }

    }
}

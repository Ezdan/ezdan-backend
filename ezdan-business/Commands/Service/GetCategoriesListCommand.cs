﻿using ezdan_business.Helper;
using ezdan_business.PITMobileIntergration;
using ezdan_logger;
using Newtonsoft.Json.Linq;
using System;

namespace ezdan_business.Commands.Service
{
    public class GetCategoriesListCommand : BaseCommand
    {
        //public JObject categories { get; set; }
        public static JArray JSONCategoriesArray;
        private string SVCUsername;
        private string SVCPassword;
        private static DateTime _nextCatsSyncDateTime;

        PITCaseServiceClient client;
        CallContext context;

        public GetCategoriesListCommand()
        {
            SVCUsername = SettingHelper.GetAppSettingValue("SVCUsername").ToString();
            SVCPassword = SettingHelper.GetAppSettingValue("SVCPassword").ToString();

            client = new PITCaseServiceClient();
            context = new CallContext();

            context.Company = "ERE";
            client.ClientCredentials.Windows.ClientCredential.UserName = SVCUsername;
            client.ClientCredentials.Windows.ClientCredential.Password = SVCPassword;
                        
        }


        protected override void DoAction()
        {
            try {
                //if (_nextCatsSyncDateTime != null && _nextCatsSyncDateTime < DateTime.Now)
                //{
                    LogManager.Log(LogType.trace, "loading from there.");
                    _nextCatsSyncDateTime = DateTime.Now.AddHours(1);
                    LogManager.Log(LogType.trace, _nextCatsSyncDateTime.ToString());
                    JSONCategoriesArray = new JArray();

                    PITCaseCategoryEntity[] categories = client.ListCaseCategory(context, "");
                    foreach (PITCaseCategoryEntity item in categories)
                    {
                        JObject category = new JObject();
                        category.Add("caseCategory", item.parmCaseCategory);
                        category.Add("categoryType", item.parmCategoryType.ToString());
                        category.Add("description", item.parmDescription);
                        category.Add("parentRecordId", item.parmParentRecId);
                        category.Add("recordId", item.parmRecId);
                        JSONCategoriesArray.Add(category);
                    }
                    LogManager.Log(LogType.trace, JSONCategoriesArray.Count.ToString());
                //}
            }
            catch (Exception ex)
            {
                LogManager.Log(LogType.error, "cannot get categories" + ex.Message, ex);
                _nextCatsSyncDateTime = DateTime.Now;
                //JSONCategoriesArray = null;
            }
        }

        protected override bool DoValidate()
        {
            return true;
        }
    }
}


      

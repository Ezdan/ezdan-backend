﻿using System;

namespace ezdan_business.Commands
{
    public delegate void actionEventHandler(dynamic actionState, EventArgs e);

    public abstract class BaseCommand
    {
        protected event Action PostExecute;
        protected event Action PreExecute;
        public BaseCommand()
        {
        }

        protected abstract void DoAction();
        /// <summary>
        /// to make sure the model if valid befor run the Command
        /// </summary>
        /// <returns>if false the command will not executed</returns>
        protected abstract bool DoValidate();
        public void Execute()
        {
            PreExecute?.Invoke();
            if (DoValidate())
            {
                DoAction();
                PostExecute?.Invoke();
            }
        }
    }
}

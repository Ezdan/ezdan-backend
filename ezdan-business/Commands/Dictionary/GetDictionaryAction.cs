﻿using ezdan_business.Helper;
using ezdan_entities;
using ezdan_logger;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ezdan_business.Commands.Dictionay
{
    public class GetDictionaryAction : BaseCommand
    {
        
        public List<Dictionary> Result = new List<Dictionary>();
        DataHelper db;
        public GetDictionaryAction() :base()
        {
            db = new DataHelper();
        }
        protected override void DoAction()
        {
            try
            {
                Result = db.DbEntities.Dictionaries.ToList();
            }
            catch (Exception ex) {
                LogManager.Log(LogType.error, ex.Message, ex);
                Result = null;
            }
        }

        protected override bool DoValidate()
        {
            return true;
        }
    }
}

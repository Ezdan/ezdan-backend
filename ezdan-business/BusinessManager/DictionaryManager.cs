﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ezdan_business.BusinessManager
{
    public class DictionaryManager
    {
        private readonly ezdan_entities.Entities db;
        public DictionaryManager()
        {
            db = new ezdan_entities.Entities();
        }
        public IQueryable<ezdan_entities.Dictionary> GetAll()
        {
            return db.Dictionaries;
        }
    }
}

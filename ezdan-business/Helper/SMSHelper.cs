﻿using ezdan_logger;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Text;
using System.Threading.Tasks;

namespace ezdan_business.Helper
{
    public class SMSHelper
    {
        private static string _url;
        private static string _username;
        private static string _password;
        private static string _mask;
        private static string _source;
        static SMSHelper()
        {
            _url = SettingHelper.GetAppSettingValue("SMSURL"); 
            _username = SettingHelper.GetAppSettingValue("SMSUsername");
            _password = SettingHelper.GetAppSettingValue("SMSPassword");
            _mask = SettingHelper.GetAppSettingValue("SMSMask");
            _source = SettingHelper.GetAppSettingValue("SMSSource");
        }

        public static string SendSMS(string destination, string content)
        {

            string url = _url + "?application=" + _username + "&password=" + _password + "&content=" + HttpUtility.UrlEncode(content) + "&destination=" + destination + "&source=" + _source + "&mask=" + _mask;

            try
            {
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.AutomaticDecompression = DecompressionMethods.GZip;
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    return reader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                LogManager.Log(LogType.error, ex.Message, ex);
                return null;
            }
        }
    }
}

﻿using ezdan_logger;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;


namespace ezdan_business.Helper
{
    public class ImageHelper
    {
        private static string _imageRootPath;
        private static string _dbImageRootPath;
        private static string _dbImageSubPath;
        private static string[] expectedExts;

        static ImageHelper()
        {
            _imageRootPath = SettingHelper.GetAppSettingValue("imagesRootPath");
            _dbImageRootPath = SettingHelper.GetAppSettingValue("DBImageRootPath");
            _dbImageSubPath = SettingHelper.GetAppSettingValue("DBImageSubPath");
            expectedExts = new string[5] {"jpg", "jpeg", "png", "bmp", "gif"};
        }

        public static string SaveImage(HttpPostedFile file, string filename, string foldername, string userId)
        {
            string virtauDirName = _imageRootPath + "/" + foldername;
            string fullDirPath = HttpContext.Current.Server.MapPath(virtauDirName);
            var contentDir = System.IO.Directory.CreateDirectory(fullDirPath);
            string uniqueTimeStamp = string.Format("{0:yyyy-MM-dd_hh-mm-ss-ffff}", DateTime.Now);
            string Fname = !String.IsNullOrWhiteSpace(Path.GetFileNameWithoutExtension(filename)) && Path.GetFileNameWithoutExtension(filename).Length >= 5
                ? Path.GetFileNameWithoutExtension(filename).Substring(0, 5)
                : Path.GetFileNameWithoutExtension(filename);
            Fname = Fname.Replace(" ", "_");
            Random rnd = new Random();
            string rnd_number = rnd.Next(1, 100000).ToString();
            string extension = Path.GetExtension(filename);
            string fullpath = HttpContext.Current.Server.MapPath(virtauDirName + "/" + Fname + "_" + userId + "_" + uniqueTimeStamp + extension);
            string DBImageURL = _dbImageRootPath + "/" + _dbImageSubPath + "/" + foldername + "/" + Fname + "_" + userId + "_" + uniqueTimeStamp + extension;

            file.SaveAs(fullpath);
            return DBImageURL;
        }

        public static string DownloadImage(string url, string foldername, string filename)
        {
            string virtauDirName = _imageRootPath + "/" + foldername + "/";
            string fullDirPath = HttpContext.Current.Server.MapPath(virtauDirName);
            string ext = Path.GetExtension(url);
            ext = expectedExts.Contains(ext) ? ext : "jpg";
            string fullPath = fullDirPath + filename + "." + ext;
            var contentDir = Directory.CreateDirectory(fullDirPath);
            WebClient client = new WebClient();
            try
            {
                client.DownloadFile(url, fullPath);
            }
            catch(Exception ex)
            {
                LogManager.Log(LogType.error, ex.Message, ex);
                return string.Empty;
            }
            return _dbImageRootPath + "/" + _dbImageSubPath + "/" + foldername + "/" + filename + "." + ext; ;
        }

        public static void DeleteTempImages(string tempImagesPath, string propertyId)
        {
            string[] tempImages = Directory.GetFiles(tempImagesPath);
            foreach(string imageFilename in tempImages)
            {
                if(imageFilename.Contains(propertyId))
                {
                    try
                    {
                        File.Delete(imageFilename);
                    }
                    catch (Exception ex)
                    {
                        LogManager.Log(LogType.error, "Cannot delete " + imageFilename + "\n\n" + ex.Message, ex);
                        continue;
                    }
                }
            }
        }
    }
}

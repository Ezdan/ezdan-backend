﻿using ezdan_business.Commands;
using ezdan_business.Commands.Common;
using ezdan_entities;
using ezdan_entities.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ezdan_business.Helper
{
   public static class MailHelper
    {
        //private Entities db = new Entities();

        private static EmailConfiguration _configuration;

        static MailHelper()
        {
            //read data from configuration
            _configuration = new EmailConfiguration();
            _configuration.Host = SettingHelper.GetAppSettingValue("Host");
            _configuration.SMTPEmail = SettingHelper.GetAppSettingValue("SMTPEmail");
            _configuration.SMTPPassword = SettingHelper.GetAppSettingValue("SMTPPassword");
            _configuration.EnableSSL = Convert.ToBoolean(SettingHelper.GetAppSettingValue("EnableSSL"));
            _configuration.Port = Convert.ToInt32(SettingHelper.GetAppSettingValue("Post"));
            _configuration.MailSender = SettingHelper.GetAppSettingValue("MailSender");
        }

        public static bool SendEmail(EmailNotification email)
        {
            var adminMailSender = new Entities().ContentProperties.Where(cps => cps.ContentId == 6 && cps.PropertyId == 34).Select(cps => cps.EnglishValue).FirstOrDefault();
            if(adminMailSender != null)
            {
                _configuration.MailSender = adminMailSender;
            }
            var adminMailPassword = new Entities().ContentProperties.Where(cps => cps.ContentId == 6 && cps.PropertyId == 39).Select(cps => cps.EnglishValue).FirstOrDefault();
            if (adminMailPassword != null)
            {
                _configuration.SMTPPassword = adminMailPassword;
            }
            email.Configuration = _configuration;
            SendEmailCommand _command = new SendEmailCommand();
            _command.Email = email;
            _command.Execute();
            return _command.SendResult;
        }


    }
}

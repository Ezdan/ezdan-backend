﻿using ezdan_entities;
using ezdan_logger;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ezdan_business.Helper
{
    public  class DataHelper
    {
        private  Entities _DbEntities;
        public  Entities DbEntities
        {
            get
            {
                if (_DbEntities == null)
                    _DbEntities = new Entities();
                return _DbEntities;
            }
        }
        public  bool SaveChanges()
        {
            bool saveResult = false;
            try
            {
                _DbEntities.SaveChanges();
                saveResult = true;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    LogManager.Log(LogType.error,string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        LogManager.Log(LogType.error, string.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage));
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.Log(LogType.error, "Saving Changes : " + ex.Message, ex);
                saveResult = false;
            }
            return saveResult;                
        }
    }
}

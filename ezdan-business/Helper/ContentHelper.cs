﻿using ezdan_entities;
using ezdan_entities.Helper;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ezdan_business.Helper
{
    public class ContentHelper
    {
        public static JObject CreateContent(Content parent, Content content, List<Content> childContents, AppLanguage lang)
        {
            JObject result = new JObject();

            if (parent != null)
            {
                var parentTitle = lang == AppLanguage.Arabic ? parent.ArabicName : parent.EnglishName;
                JObject parentJobj = new JObject();
                parentJobj.Add("id", parent.ContentId);
                parentJobj.Add("title", parentTitle);
                parentJobj.Add("type", parent.ContentType);
                parentJobj.Add("blocked", parent.Blocked);
                parentJobj.Add("reported", parent.Reported);
                parentJobj.Add("created_on", parent.CreatedOn);
                result.Add("parent", parentJobj);
            }

            var contentTitle = lang == AppLanguage.Arabic ? content.ArabicName : content.EnglishName;
            JObject contentObj = new JObject();
            contentObj.Add("id", content.ContentId);
            contentObj.Add("title", contentTitle);
            contentObj.Add("type", content.ContentType);
            contentObj.Add("blocked", content.Blocked);
            contentObj.Add("reported", content.Reported);
            contentObj.Add("created_on", content.CreatedOn);
            JObject contentJProps = new JObject();
            JArray contentimages = new JArray();
            JArray contentListItems = new JArray();

            #region Get All Content Properties
            foreach (var prop in content.ContentProperties)
            {
                string key = null;
                var propValue = (lang == AppLanguage.English) ? prop.EnglishValue : prop.ArabicValue;

                if (prop.Property.Name.Equals("image"))
                {
                    JObject image = new JObject();
                    image.Add("ContentPropertyId", prop.ContentPropertyId);
                    image.Add("PropertyId", prop.PropertyId);
                    image.Add("src", propValue);
                    contentimages.Add(image);
                }
                else if (prop.Property.Name.Equals("listItem"))
                {
                    JObject item = new JObject();
                    item.Add("ContentPropertyId", prop.ContentPropertyId);
                    item.Add("PropertyId", prop.PropertyId);
                    item.Add("value", propValue);
                    contentListItems.Add(item);
                }else
                    key = prop.Property.Name;

                JObject propObj = new JObject();
                propObj.Add("ContentPropertyId", prop.ContentPropertyId);
                propObj.Add("PropertyId", prop.PropertyId);
                propObj.Add("value", propValue);

                if (key != null)
                    contentJProps.Add(key, propObj);
            }
            //if (contentimages.Count > 0)
                contentJProps.Add("images", contentimages);

            //if (contentListItems.Count > 0)
                contentJProps.Add("listItems", contentListItems);

            if (content.CreatedBy != null)
                contentJProps.Add("posted_by", content.AspNetUser.FullName);

            contentJProps.Add("posted_on", content.CreatedOn);
     
            #endregion

            contentObj.Add("properties", contentJProps);

            #region Getting Content Children
            JArray children = new JArray();
            if (childContents.Count > 0)
            {
                //JArray sub_children = new JArray();
                foreach (var childContent in childContents)
                {
                    bool pastEvent = false;
                    JObject child = new JObject();
                    List<Content> childsChildren = childContent.Content1.ToList();
                    if (childsChildren.Count > 0 && content.ContentId != parent.ContentId)
                    {
                        JObject x = ContentHelper.CreateContent(content, childContent, childsChildren, lang);
                        children.Add(x);
                    }
                    else
                    {
                        JObject jProps = new JObject();
                        JArray images = new JArray();
                        JArray listItems = new JArray();
                        child.Add("id", childContent.ContentId);
                        child.Add("type", childContent.ContentType);
                        child.Add("blocked", childContent.Blocked);
                        var chldContentNameValue = lang == AppLanguage.Arabic ? childContent.ArabicName : childContent.EnglishName;
                        child.Add("title", chldContentNameValue);

                        foreach (var prop in childContent.ContentProperties)
                        {
                            string key = null;
                            var propValue = (lang == AppLanguage.English) ? prop.EnglishValue : prop.ArabicValue;
                            if (prop.Property.Name.Equals("eventEndingDate")
                                || prop.Property.Name.Equals("eventStartingDate"))
                            {
                                DateTime timestamp;
                                DateTime.TryParse(propValue.ToString(), out timestamp);
                                if (timestamp != null && DateTime.Compare(DateTime.Now, timestamp) > 0)
                                {
                                    pastEvent = true;
                                    break;
                                }
                                JObject item = new JObject();
                                item.Add("ContentPropertyId", prop.ContentPropertyId);
                                item.Add("PropertyId", prop.PropertyId);
                                item.Add("value", propValue.ToString());
                                listItems.Add(item);
                            }
                            if (prop.Property.Name.Equals("image"))
                            {
                                JObject image = new JObject();
                                image.Add("ContentPropertyId", prop.ContentPropertyId);
                                image.Add("PropertyId", prop.PropertyId);
                                image.Add("src", propValue);
                                images.Add(image);
                            }
                            else if (prop.Property.Name.Equals("listItem"))
                            {
                                JObject item = new JObject();
                                item.Add("ContentPropertyId", prop.ContentPropertyId);
                                item.Add("PropertyId", prop.PropertyId);
                                item.Add("value", propValue);
                                listItems.Add(item);
                            }
                            else if (prop.Property.Name.Equals("date"))
                            {
                                JObject item = new JObject();
                                item.Add("ContentPropertyId", prop.ContentPropertyId);
                                item.Add("PropertyId", prop.PropertyId);
                                item.Add("value", propValue.ToString());
                                listItems.Add(item);
                            }
                            else
                            {
                                key = prop.Property.Name;
                            }
                            JObject propObj = new JObject();
                            propObj.Add("ContentPropertyId", prop.ContentPropertyId);
                            propObj.Add("PropertyId", prop.PropertyId);
                            propObj.Add("value", propValue);

                            if (key != null)
                                jProps.Add(key, propObj);
                        }

                        if (pastEvent)
                        {
                            continue;
                        }

                        if (childContent.CreatedBy != null)
                        jProps.Add("posted_by", childContent.AspNetUser.FullName);

                        if (images.Count > 0)
                            jProps.Add("images", images);
                        if (listItems.Count > 0)
                            jProps.Add("listItems", listItems);

                        jProps.Add("posted_on", childContent.CreatedOn);

                        child.Add("properties", jProps);
                        JObject con = new JObject();
                        con.Add("parent", contentObj);
                        con.Add("content", child);
                        children.Add(con);
                    }
                    
                }
                result.Add("children", children);
                #endregion
            }
            result.Add("content", contentObj);
            return result;
        }
    }
}

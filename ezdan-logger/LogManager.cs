﻿using NLog;
using System;

namespace ezdan_logger
{
    public static class LogManager
    {
        private static readonly Logger _logManager = NLog.LogManager.GetCurrentClassLogger();
        public static void Log(LogType logtype, string message, Exception ex =null)
        {
            switch (logtype)
            {
                case LogType.error:
                    _logManager.Log(LogLevel.Error, ex, message);
                    break;
                case LogType.trace:
                    _logManager.Log(LogLevel.Trace, ex, message);
                    break;
                case LogType.info:
                    _logManager.Log(LogLevel.Info, ex, message);
                    break;
                default:
                    break;
            }
            if(ex != null && ex.InnerException != null)
            {
                Log(logtype, "Inner Exception : " + ex.InnerException.Message, ex.InnerException);
            }
        }

        public static void Log()
        {
            throw new NotImplementedException();
        }
    }
    public enum LogType
    {
        error,trace,info
    }
}

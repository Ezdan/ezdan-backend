namespace ezdan_backend.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateuser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "UnitId", c => c.String());
            AddColumn("dbo.AspNetUsers", "PartyRecId", c => c.Long());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "PartyRecId");
            DropColumn("dbo.AspNetUsers", "UnitId");
        }
    }
}

﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(ezdan_backend.Startup))]

namespace ezdan_backend
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

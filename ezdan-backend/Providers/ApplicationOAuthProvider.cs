﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using System.Web.Http.Cors;
using ezdan_backend.Models;
using ezdan_logger;
using ezdan_business.Commands.User;
using ezdan_business.Helper;

namespace ezdan_backend.Providers
{
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
        private readonly string _publicClientId;

        public ApplicationOAuthProvider(string publicClientId)
        {
            if (publicClientId == null)
            {
                throw new ArgumentNullException("publicClientId");
            }

            _publicClientId = publicClientId;
        }

        //[EnableCors(origins: "*", headers: "*", methods: "*")]
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();

            ApplicationUser user = await userManager.FindAsync(context.UserName, context.Password);

            if (user == null)
            {
                context.SetError("invalid_grant", "The user name or password is incorrect.");
                return;
            }

            string ClientId = context.OwinContext.Get<string>("ClientId");
            //check for the contract id
            GetAxUserCommand axUserCommand = new GetAxUserCommand
            { ContractNumber = user.ContractNumber, AccountNumber = user.AccountId, isLogin = true };
            axUserCommand.Execute();
            if ((axUserCommand.IsValid.HasValue && axUserCommand.IsValid.Value) || !Convert.ToBoolean(SettingHelper.GetAppSettingValue("IsEnableIntegration")))
            {
                ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(userManager,
                   OAuthDefaults.AuthenticationType);
                ClaimsIdentity cookiesIdentity = await user.GenerateUserIdentityAsync(userManager,
                    CookieAuthenticationDefaults.AuthenticationType);

                Dictionary<string, string> dicData = new Dictionary<string, string>
                {
                    {"UserName",user.UserName },
                    {"FullName",user.FullName },
                    {"Language",user.UserLanguage == 1 ? "English" : "Arabic"},
                    {"Title",user.Title },
                    {"Mobile",user.PhoneNumber },
                    {"Location",user.Address },
                    {"UnitId",user.UnitId },
                    {"ImageURL",user.ImageUrl != null ? user.ImageUrl : ""},
                    {"Notification",user.EnableNotification.ToString()}
                };
                AuthenticationProperties properties = CreateProperties(dicData);
                AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, properties);
                context.Validated(ticket);
                context.Request.Context.Authentication.SignIn(cookiesIdentity);
            }
            else if (!axUserCommand.IsValid.HasValue)
            {
                //exception Hanppened
                context.SetError("Ax Exception", "Error retrieving data from AX");
            }
            else if (!axUserCommand.IsValid.Value)
            {
                context.SetError("ezdna-error", "the contract number is expired or invalid");
            }
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }
        //public override Task ValidateTokenRequest(OAuthValidateTokenRequestContext context)
        //{
        //    context.Request.Body.Position = 0;
        //    var reader = new StreamReader(context.Request.Body);
        //    var body = reader.ReadToEnd();
        //    return base.ValidateTokenRequest(context);
        //}
        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            // Resource owner password credentials does not provide a client ID.
            if (context.ClientId == null)
            {
                context.Validated();
            }
            //string ClientId = context.Parameters.Where(f => f.Key == "ClientId").Select(f => f.Value).SingleOrDefault()[0];
            //context.OwinContext.Set<string>("ClientId", ClientId);
            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
        {
            if (context.ClientId == _publicClientId)
            {
                Uri expectedRootUri = new Uri(context.Request.Uri, "/");

                if (expectedRootUri.AbsoluteUri == context.RedirectUri)
                {
                    context.Validated();
                }
            }

            return Task.FromResult<object>(null);
        }

        public static AuthenticationProperties CreateProperties(Dictionary<string, string> properties)
        {
            //IDictionary<string, string> data = new Dictionary<string, string>();
            //{
            //    { "userName", userName }
            //};
            return new AuthenticationProperties(properties);
        }
    }
}
﻿using ezdan_backend.ApiHelper;
using ezdan_business.Commands.Service;
using ezdan_entities;
using ezdan_logger;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace ezdan_backend.Controllers
{
    public class ServiceRequestController : BaseController
    {
        [Authorize]
        [HttpGet]
        [Route("api/servicerequest/getcategorieslist")]
        public HttpResponseMessage GetCategoryList()
        {
            JArray result = new JArray();
            try
            {
                GetCategoriesListCommand getCategoriesListCommand = new GetCategoriesListCommand();
                getCategoriesListCommand.Execute();
                result = GetCategoriesListCommand.JSONCategoriesArray;
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch(Exception ex)
            {
                LogManager.Log(LogType.error, ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }


        /*Sample JSON Object sent in body:
         *
          {
	       "parmCategoryRecId": "5637152843",
	       "parmDescription": "This is a trial service",
           "parmPITAvailableFrom": "2012-03-19T07:22Z",
           "parmPITAvailableTo": "2012-03-19T07:22Z"
          }
         *
         */
        [Authorize]
        [HttpPost]
        [Route("api/servicerequest/sendservicerequest")]
        public HttpResponseMessage PostServiceRequest(JObject requestParams)
        {
            AspNetUser currentUser = User.Identity.GetAspNetUser();
            SendServiceRequestCommand sendServiceRequestCmd = new SendServiceRequestCommand();

            var Json = (JObject)JsonConvert.DeserializeObject(requestParams.ToString());
            long parmCategoryRecId = Convert.ToInt64(Json["parmCategoryRecId"]);
            string parmDescription = Convert.ToString(Json["parmDescription"]);
            DateTime parmPITAvailableFrom = Convert.ToDateTime(Json["parmPITAvailableFrom"]);
            DateTime parmPITAvailableTo = Convert.ToDateTime(Json["parmPITAvailableTo"]);
            if(Json["parmPITImages"] != null)
            {
                sendServiceRequestCmd.files = (JArray)Json["parmPITImages"];
            }

            sendServiceRequestCmd.parmEmail = currentUser.Email; //From DB
            sendServiceRequestCmd.language= currentUser.UserLanguage;
            sendServiceRequestCmd.parmPhoneNumber = currentUser.PhoneNumber; //From DB
            sendServiceRequestCmd.parmParty = currentUser.PartyRecId; //Party record id from customer table
            sendServiceRequestCmd.parmPITBuildingUnitId = currentUser.UnitId; //Party record id from customer table

            sendServiceRequestCmd.parmCategoryRecId = parmCategoryRecId; //category record id from case category table
            sendServiceRequestCmd.parmDescription = parmDescription; //case description
            sendServiceRequestCmd.parmPITAvailableFrom = parmPITAvailableFrom;
            sendServiceRequestCmd.parmPITAvailableTo = parmPITAvailableTo;

            sendServiceRequestCmd.Execute();

            JObject result = sendServiceRequestCmd.JSONcaseId;
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [Authorize]
        [HttpPost]
        [Route("api/servicerequest/uploadrequestimages")]
        public HttpResponseMessage uploadRequestImages()
        {
            AspNetUser currentUser = User.Identity.GetAspNetUser();
            HttpPostedFile file = HttpContext.Current.Request.Files["file"];
            string dir = HttpContext.Current.Request.Params["dir"];
            UploadImagesCommand uploadServiceRequestImgsCmd = new UploadImagesCommand(file, dir, UserId);
            uploadServiceRequestImgsCmd.Execute();
            if(uploadServiceRequestImgsCmd.imageUploaded)
            {
                JObject result = uploadServiceRequestImgsCmd.resp;
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            return Request.CreateResponse(HttpStatusCode.InternalServerError);
        }
    }
}
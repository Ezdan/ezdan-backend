﻿using ezdan_backend.ApiHelper;
using ezdan_business.Commands.Property;
using ezdan_business.Helper;
using ezdan_logger;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace ezdan_backend.Controllers
{
    public class PropertyController : BaseController
    {
        public int PageSize { get; set; }
        public PropertyController()
        {
             PageSize = Convert.ToInt32(SettingHelper.GetAppSettingValue("ListPageSize"));
        }
        public HttpResponseMessage Get(int? pageNumber)
        {
            int start = GetStartValue(pageNumber, PageSize);
            LoadPropertyCommand loadPropertyCmd = new LoadPropertyCommand();
            loadPropertyCmd.Execute();
            var Result = LoadPropertyCommand.EzdanProperties.Skip(start).Take(PageSize);
            if (Result == null)
                return Request.CreateResponse(HttpStatusCode.NotFound);
            return Request.CreateResponse(HttpStatusCode.OK, new {
                result = Result,
                noMore = Result.Count() < PageSize
            });
        }
        [HttpGet]
        public HttpResponseMessage Find(int? pageNumber ,string Bedrooms,string City,string PropertyType,string IsFurnished)
        {
            int start = GetStartValue(pageNumber, PageSize);
            FindPropertyCommand findPropertyCommand = new FindPropertyCommand() { Bedrooms = Bedrooms, City = City , PropertyType = PropertyType,IsFurnished = IsFurnished };
            findPropertyCommand.Execute();
            var result = findPropertyCommand.FilteredList.Skip(start).Take(PageSize);
            if (result == null)
                return Request.CreateResponse(HttpStatusCode.NotFound);
            return Request.CreateResponse(HttpStatusCode.OK, new
            {
                result = result,
                noMore = result.Count() < PageSize
            });
        }
        [HttpGet]
        public HttpResponseMessage GetFilters()
        {
            GetPropertyFiltersCommand getFiltersCmd = new GetPropertyFiltersCommand();
            getFiltersCmd.Execute();
            var filters = GetPropertyFiltersCommand.Filters;
            if (filters == null)
                return Request.CreateResponse(HttpStatusCode.NotFound);
            return Request.CreateResponse(HttpStatusCode.OK, filters);
        }

        public HttpResponseMessage GetPropertyById(string buildingNumber)
        {
            GetPropertyCommand getPropertyCmd = new GetPropertyCommand() { buildingNumber = buildingNumber };
            getPropertyCmd.Execute();

            var property = getPropertyCmd.Property;
            if (property == null)
                return Request.CreateResponse(HttpStatusCode.NotFound);
            return Request.CreateResponse(HttpStatusCode.OK, property);

        }

        [HttpGet]
        public HttpResponseMessage GetPropertyTempImages(string id, string uuid)
        {
            if(uuid == null)
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            GetPropertyCommand getPropertyCmd = new GetPropertyCommand() { buildingNumber = id, deviceId = uuid };
            getPropertyCmd.Execute();
            var images = getPropertyCmd.Property.PropertyImages;
            if (images == null)
                return Request.CreateResponse(HttpStatusCode.NotFound);
            return Request.CreateResponse(HttpStatusCode.OK, images);

        }

        [HttpGet]
        public async Task<HttpResponseMessage> DeletePropertyTempImages(string uuid)
        {
            if (uuid == null)
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            string tempImageRootPath = HttpContext.Current.Server.MapPath(SettingHelper.GetAppSettingValue("imagesTempRootPath"));
            await Task.Run(() => ImageHelper.DeleteTempImages(tempImageRootPath, uuid));
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}

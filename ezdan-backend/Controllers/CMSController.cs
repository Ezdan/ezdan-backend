﻿using ezdan_backend.ApiHelper;
using ezdan_business.Commands.CMS;
using ezdan_entities.Helper;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ezdan_backend.Controllers
{
    [RoutePrefix("api/cms")]
    public class CMSController : BaseController
    {

        [HttpGet]
        [Route("getcontent")]
        public HttpResponseMessage GetContent(int id, string lang)
        {
            GetContentCommand getContentCmd = new GetContentCommand();
            getContentCmd.language = lang == "Arabic" ? AppLanguage.Arabic : AppLanguage.English;
            getContentCmd.contentId = id;
            getContentCmd.Execute();
            JObject result = getContentCmd.result;
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
    }
}
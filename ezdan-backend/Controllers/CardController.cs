﻿using ezdan_backend.ApiHelper;
using ezdan_business.Commands.Card;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ezdan_backend.Controllers
{
    [RoutePrefix("api/card")]
    public class CardController : BaseController
    {
        public object GetAllCardOffersCmd { get; private set; }

        [Route("edOffers")]
        public HttpResponseMessage GetAllOffers()
        {
            GetAllCardOffersCommand GetAllCardOffersCmd = new GetAllCardOffersCommand();
            GetAllCardOffersCmd.Execute();
            var Result = GetAllCardOffersCommand.EDCOffers;
            return Request.CreateResponse(HttpStatusCode.OK, Result);
        }
        [Route("limitedOffers")]
        public HttpResponseMessage GetAllLimitedOffers()
        {
            GetAllLimitedOffersCommand GetAllLimitedOffersCmd = new GetAllLimitedOffersCommand();
            GetAllLimitedOffersCmd.Execute();
            var Result = GetAllLimitedOffersCommand.LimitedOffers;
            return Request.CreateResponse(HttpStatusCode.OK, Result);
        }
        [Route("partnerOffers")]
        public HttpResponseMessage GetAllPartnerOffers()
        {
            GetAllPartnerOffersCommand GetAllPartnerOffersCmd = new GetAllPartnerOffersCommand();
            GetAllPartnerOffersCmd.Execute();
            var Result = GetAllPartnerOffersCommand.PartnerOffers;
            return Request.CreateResponse(HttpStatusCode.OK, Result);
        }
        [HttpPost]
        [Route("enrollDiscountCard")]
        [Authorize]
        public HttpResponseMessage EnrollDiscountCard(JObject obj)
        {
            var Json = (JObject)JsonConvert.DeserializeObject(obj.ToString());
            string contactNumber = Json["contactNumber"].ToString();
            string category = Json["category"].ToString();
            EnrollDiscountCardCommand enrollDiscountCardCmd = new EnrollDiscountCardCommand() { UserId = UserId, ContactNumber = contactNumber, Category = category };
            enrollDiscountCardCmd.Execute();
            return Request.CreateResponse(HttpStatusCode.OK, new { Added = enrollDiscountCardCmd.Result });
        }
    }
}

﻿using ezdan_backend.ApiHelper;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ezdan_business.Commands.Photogallery;

namespace ezdan_backend.Controllers
{
    public class PhotogalleryController : BaseController
    {
        [HttpGet]
        [Route("api/photogallery/getalbums")]
        public HttpResponseMessage GetAlbums(string lang/*="1"*/)
        {
            GetAlbumsCommand getAlbumsCommand = new GetAlbumsCommand();
            getAlbumsCommand.lang = lang;
            getAlbumsCommand.Execute();
            JObject result = getAlbumsCommand.allAlbumsResult;
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [HttpGet]
        [Route("api/photogallery/getalbumphotos")]
        public HttpResponseMessage GetAlbumPhotos(string albumid, string lang/* = "1"*/)
        {
            GetAlbumPhotosCommand getAlbumPhotosCommand = new GetAlbumPhotosCommand();
            getAlbumPhotosCommand.lang = lang;
            getAlbumPhotosCommand.albumid = albumid;
            getAlbumPhotosCommand.Execute();
            JObject result = getAlbumPhotosCommand.allAlbumsResult;
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

    }
}

﻿using ezdan_backend.ApiHelper;
using ezdan_business.Commands.Survey;
using ezdan_business.Helper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Web.Http;
using System.Web.Helpers;
using System.Text.RegularExpressions;
using ezdan_logger;

namespace ezdan_backend.Controllers
{

    public class SurveyController : BaseController
    {
        DataHelper db;
        public SurveyController()
        {
            db = new DataHelper();
        }
        [HttpPost]
        [Route("api/survey/getsurveylocations")]
        [Authorize]
        public IHttpActionResult getSurveyLocations()
        {
            GetSurveysLocationsCommand surveyLocations = new GetSurveysLocationsCommand();
            surveyLocations.Execute();
            JObject result = JObject.Parse(surveyLocations.SurveyResult);
            JObject finalResult = new JObject();
            JArray locationsInfo = new JArray();
            foreach (var surveyLocation in result["locationinfo"])
            {
                var surveyLocationId = Convert.ToInt16(surveyLocation["id"]);
                JObject surveyLocationObject = (JObject)surveyLocation;
                JObject surveyLocationParams = new JObject();
                surveyLocationParams.Add("locationId", surveyLocationId);
                surveyLocationParams.Add("sessionKey", "");
                var surveyLocationResult = getSurveysPerLocationObject(surveyLocationParams);
                var surveyLocationResultTotal = ((JArray)surveyLocationResult["surveys"]).Count;
                surveyLocationObject.Add("surveysTotal", surveyLocationResultTotal);
                locationsInfo.Add(surveyLocationObject);
            }
            finalResult.Add("locationinfo", locationsInfo);
            finalResult.Add("session_key", "");
            return Ok(finalResult);
        }
        private JObject getSurveysPerLocationObject(JObject JsonObject)
        {
            int locationId;
            string sessionKey;
            try
            {
                var Json = (JObject)JsonConvert.DeserializeObject(JsonObject.ToString());
                locationId = Convert.ToInt16(Json["locationId"]);
                sessionKey = Convert.ToString(Json["sessionKey"]);
            }
            catch (Exception ex)
            {
                return null;
            }
            if (locationId != 0)
            {
                GetSurveysPerLocationCommand surveysPerLocation = new GetSurveysPerLocationCommand();
                surveysPerLocation.sessionKey = sessionKey;
                surveysPerLocation.locationId = locationId;
                surveysPerLocation.userId = UserId;
                surveysPerLocation.Execute();
                return surveysPerLocation.x;
            }
            return null;
        }
        [HttpPost]
        [Route("api/survey/getsurveysperlocation")]
        [Authorize]
        public IHttpActionResult getSurveysPerLocation(JObject JsonObject)
        {
            int locationId;
            string sessionKey;
            try
            {
                var Json = (JObject)JsonConvert.DeserializeObject(JsonObject.ToString());
                locationId = Convert.ToInt16(Json["locationId"]);
                sessionKey = Convert.ToString(Json["sessionKey"]);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
            if (locationId != 0)
            {
                GetSurveysPerLocationCommand surveysPerLocation = new GetSurveysPerLocationCommand();
                surveysPerLocation.sessionKey = sessionKey;
                surveysPerLocation.locationId = locationId;
                surveysPerLocation.userId = UserId;
                surveysPerLocation.Execute();
                return Ok(surveysPerLocation.x);
            }
            return Ok();
        }
        [HttpPost]
        [Authorize]
        public IHttpActionResult getSurveyQuestionsPerLocation(JObject jsonInput)
        {
            var Json = (JObject)JsonConvert.DeserializeObject(jsonInput.ToString());
            string surveyId = Json["surveyId"].ToString();

            String sessionKey = Convert.ToString(Json["sessionKey"]);
            GetSurveyQuestionsPerLocation surveyQuestions = new GetSurveyQuestionsPerLocation();
            surveyQuestions.surveyId = Convert.ToInt16(surveyId);
            surveyQuestions.sessionKey = sessionKey;
            surveyQuestions.Execute();
            JObject result = JObject.Parse(surveyQuestions.jsonResponse);
            return Ok(result);

        }
        [Route("api/survey/submitSurvey")]
        [HttpPost]
        [Authorize]
        public IHttpActionResult submitSurvey(JObject json)
        {
            LogManager.Log(LogType.trace, UserId);
            SubmitSingleSurveyCommand submit = new SubmitSingleSurveyCommand();
            submit.jsonWithSurveyData = json;
            submit.userId = UserId;
            submit.Execute();
            LogManager.Log(LogType.trace, "submission done. " + submit.saveResult.ToString());
            bool result = submit.saveResult;
            if (result)
                return Ok();
            else
                return null;
        }

    }
}
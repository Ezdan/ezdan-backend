﻿using ezdan_backend.ApiHelper;
using ezdan_business.Commands.Dictionay;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace ezdan_backend.Controllers
{
    public class DictionaryController : BaseController
    {
        public DictionaryController()
        {

        }
        [HttpGet]
        [AllowAnonymous]
        [ResponseType(typeof(ezdan_entities.Dictionary))]
        public HttpResponseMessage Get()
        {
            GetDictionaryAction getDicAction = new GetDictionaryAction();
            getDicAction.Execute();
            var result = getDicAction.Result;
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using ezdan_entities;
using Newtonsoft.Json.Linq;
using System.Web;
using Newtonsoft.Json;
using ezdan_logger;
using ezdan_business.Helper;
using ezdan_backend.ApiHelper;

namespace ezdan_backend.Controllers
{
    public class AppointmentsController : BaseController
    {
        private Entities db = new Entities();
        
        #region Create an appointment
        [HttpPost]
        [Route("api/Appointments/SaveAppointment")]
        public JObject SaveAppointment(JObject obj)
        {
            string subject, date, name, email, title, mobile, location, unit = "";
            var Json = (JObject)JsonConvert.DeserializeObject(obj.ToString());
            subject = Convert.ToString(Json["subject"]);
            date = Convert.ToString(Json["date"]);
            name = Convert.ToString(Json["name"]);
            title = Convert.ToString(Json["title"]);
            email = Convert.ToString(Json["email"]);
            mobile = Convert.ToString(Json["mobile"]);
            location = Convert.ToString(Json["location"]);
            unit = Convert.ToString(Json["unit"]);
            Appointment app = new Appointment();
            app.StartDate = DateTime.Parse(date);
            app.EndDate = DateTime.Parse(date).AddHours(1);
            app.Title = string.IsNullOrEmpty(title) ? "No title" : title;
            app.UserId = UserId;
            app.Email = email;
            app.Mobile = mobile;
            db.Appointments.Add(app);
            JObject result = new JObject();
            try
            {
                db.SaveChanges();
                string filename = HttpContext.Current.Server.MapPath("~/Views/MailTemplates/test.html");
                string mailbody = System.IO.File.ReadAllText(filename);
                mailbody = mailbody.Replace("##NAME##", name);
                mailbody = mailbody.Replace("##EMAIL##", email);
                mailbody = mailbody.Replace("##MOBILE##", mobile);
                mailbody = string.IsNullOrEmpty(location) ? mailbody.Replace("##LOCATION_ELEM##", "") : mailbody.Replace("##LOCATION_ELEM##", "<p style='color: white'>Location: <span style='color:#f17077'>" + location + "</span></p>");
                mailbody = string.IsNullOrEmpty(unit) ? mailbody.Replace("##UNIT_ELEM##", "") : mailbody.Replace("##UNIT_ELEM##", "<p style='color: white'>Unit: <span style='color:#f17077'>" + unit + "</span></p>");
                mailbody = mailbody.Replace("##DATE##", date);
                mailbody = mailbody.Replace("##SUBJECT##", subject);
                MailHelper.SendEmail(new ezdan_entities.Models.EmailNotification { EmailTo = email, Subject = subject, Body = mailbody });
                string salesDptEmail = db.ContentProperties.Where(cps => cps.ContentId == 6 && cps.PropertyId == 36).Select(cps => cps.EnglishValue).FirstOrDefault();
                if(string.IsNullOrEmpty(salesDptEmail)) {
                    salesDptEmail = SettingHelper.GetAppSettingValue("SalesDptEmail");
                }
                string salesDptTemplate = HttpContext.Current.Server.MapPath("~/Views/MailTemplates/appointmentSalesDeptMailTemplate.html");
                string salesDptMailBody = System.IO.File.ReadAllText(salesDptTemplate);
                salesDptMailBody = salesDptMailBody.Replace("##NAME##", name);
                salesDptMailBody = salesDptMailBody.Replace("##EMAIL##", email);
                salesDptMailBody = salesDptMailBody.Replace("##MOBILE##", mobile);
                salesDptMailBody = string.IsNullOrEmpty(location) ? salesDptMailBody.Replace("##LOCATION_ELEM##", "") : salesDptMailBody.Replace("##LOCATION_ELEM##", "<p style='color: white'>Location: <span style='color:#f17077'>" + location + "</span></p>");
                salesDptMailBody = string.IsNullOrEmpty(unit) ? salesDptMailBody.Replace("##UNIT_ELEM##", "") : salesDptMailBody.Replace("##UNIT_ELEM##", "<p style='color: white'>Unit: <span style='color:#f17077'>" + unit + "</span></p>");
                salesDptMailBody = salesDptMailBody.Replace("##DATE##", date);
                salesDptMailBody = salesDptMailBody.Replace("##SUBJECT##", subject);
                MailHelper.SendEmail(new ezdan_entities.Models.EmailNotification { EmailTo = salesDptEmail, Subject = "New Appiontment Reservation", Body = salesDptMailBody });
                result.Add("response", true);
                return result;
            }
            catch (Exception ex)
            {
                result.Add("response", false);
                return result;
            }
        }
        #endregion

        #region Cancel an appointment
        [HttpPost]
        [Route("api/Appointments/CancelAppointment")]
        public JObject CancelAppointment(JObject obj)
        {
            int id; string name = ""; string email = ""; string Subject = "";
            var Json = (JObject)JsonConvert.DeserializeObject(obj.ToString());
            id = Convert.ToInt32(Json["id"]);
            Subject = Convert.ToString(Json["Subject"]);
            name = Convert.ToString(Json["name"]);
            email = Convert.ToString(Json["email"]);
            Appointment app = db.Appointments.Where(x => x.AppointmentId == id).SingleOrDefault();
            JObject result = new JObject();
            try
            {
                db.Appointments.Remove(app);
                db.SaveChanges();
                string filename = HttpContext.Current.Server.MapPath("~/Views/MailTemplates/test.html");
                string mailbody = System.IO.File.ReadAllText(filename);
                mailbody = mailbody.Replace("##NAME##", name);
                MailHelper.SendEmail(new ezdan_entities.Models.EmailNotification { EmailTo = email, Subject = Subject, Body = "Appointment has been cancelled" });
                //SmtpClient SmtpServer = new SmtpClient(ConfigurationManager.AppSettings["Host"], int.Parse(ConfigurationManager.AppSettings["Port"]));
                //SmtpServer.UseDefaultCredentials = false;
                //SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                //SmtpServer.EnableSsl = true;
                //SmtpServer.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["UserName"], ConfigurationManager.AppSettings["Password"]);

                //MailMessage Mail = new MailMessage();
                //Mail.From = new MailAddress(ConfigurationManager.AppSettings["UserName"]);
                //Mail.To.Add(email);
                //Mail.IsBodyHtml = true;      
                //Mail.Subject = Subject;
                //Mail.Body = mailbody;
                //SmtpServer.Send(Mail);
                result.Add("response", true);
                return result;
            }
            catch (Exception ex)
            {
                result.Add("response", false);
                return result;
            }
        }
        #endregion

        #region Get weekends
        public List<int> getWeekEnds()
        {
            string first = db.AppointmentsSettings.FirstOrDefault().FirstWeekEnd;
            string second = db.AppointmentsSettings.FirstOrDefault().SecondWeekEnd;
            List<int> weekEnds = new List<int>();
            weekEnds.Add(int.Parse(first));
            weekEnds.Add(int.Parse(second));
            return weekEnds;
        }
        #endregion

        #region Get business hours
        public List<string> getBusinessHours()
        {
            var setting = db.AppointmentsSettings.FirstOrDefault();
            string start = setting.StartWorkingHours;
            string end = setting.EndWorkingHours;
            string breakHour = setting.BreakHour;
            string afterBreak = setting.BreakTo;//DateTime.Parse(breakHour).AddHours(1).ToString("HH:mm:ss");
            string holidayFrom = setting.HolidayFrom;
            string holidayTo = setting.HolidayTo;
            List<string> hours = new List<string>();
            hours.Add(start);
            hours.Add(end);
            hours.Add(breakHour);
            hours.Add(afterBreak);
            hours.Add(holidayFrom);
            hours.Add(holidayTo);
            return hours;
        }

        public JArray getHolidays()
        {
            var holidayEntries = db.Holidays.ToList();
            JArray holidays = new JArray();
            foreach (var entry in holidayEntries)
            {
                JObject x = new JObject();
                x.Add("start", String.Format("{0:yyyy-MM-dd}", entry.HolidayStart));
                x.Add("end", String.Format("{0:yyyy-MM-dd}", entry.HolidayEnd));
                holidays.Add(x);
            }
            return holidays;
        }
        #endregion

        #region Reschedule an appointment
        [HttpPost]
        [Route("api/Appointments/EditAppointment")]
        public JObject EditAppointment(JObject obj)
        {
            int id; string subject, date, name, email, mobile, location, unit = "";
            var Json = (JObject)JsonConvert.DeserializeObject(obj.ToString());
            id = Convert.ToInt32(Json["id"]);
            subject = Convert.ToString(Json["subject"]);
            date = Convert.ToString(Json["date"]);
            Appointment app = db.Appointments.Where(x => x.AppointmentId == id).SingleOrDefault();
            name = app.AspNetUser.FullName;
            email = app.AspNetUser.Email;
            mobile = app.AspNetUser.PhoneNumber;
            location = app.AspNetUser.Address;
            unit = app.AspNetUser.UnitId;
            JObject result = new JObject();
            try
            {
                app.StartDate = DateTime.Parse(date);
                app.EndDate = DateTime.Parse(date).AddHours(1);
                app.StatusColor = null;
                db.SaveChanges();
                string filename = HttpContext.Current.Server.MapPath("~/Views/MailTemplates/test.html");
                string mailbody = System.IO.File.ReadAllText(filename);
                mailbody = mailbody.Replace("##NAME##", name);
                mailbody = mailbody.Replace("##EMAIL##", email);
                mailbody = mailbody.Replace("##MOBILE##", mobile);
                mailbody = string.IsNullOrEmpty(location) ? mailbody.Replace("##LOCATION_ELEM##", "") : mailbody.Replace("##LOCATION_ELEM##", "<p style='color: white'>Location: <span style='color:#f17077'>" + location + "</span></p>");
                mailbody = string.IsNullOrEmpty(unit) ? mailbody.Replace("##UNIT_ELEM##", "") : mailbody.Replace("##UNIT_ELEM##", "<p style='color: white'>Unit: <span style='color:#f17077'>" + unit + "</span></p>");
                mailbody = mailbody.Replace("##DATE##", date);
                mailbody = mailbody.Replace("##SUBJECT##", subject);
                MailHelper.SendEmail(new ezdan_entities.Models.EmailNotification { EmailTo = app.AspNetUser.Email, Subject = subject, Body = mailbody });

                string salesDptEmail = SettingHelper.GetAppSettingValue("SalesDptEmail");
                string salesDptTemplate = HttpContext.Current.Server.MapPath("~/Views/MailTemplates/appointmentSalesDeptMailTemplate.html");
                string salesDptMailBody = System.IO.File.ReadAllText(salesDptTemplate);
                salesDptMailBody = salesDptMailBody.Replace("##NAME##", name);
                salesDptMailBody = salesDptMailBody.Replace("##EMAIL##", email);
                salesDptMailBody = salesDptMailBody.Replace("##MOBILE##", mobile);
                salesDptMailBody = string.IsNullOrEmpty(location) ? salesDptMailBody.Replace("##LOCATION_ELEM##", "") : salesDptMailBody.Replace("##LOCATION_ELEM##", "<p style='color: white'>Location: <span style='color:#f17077'>" + location + "</span></p>");
                salesDptMailBody = string.IsNullOrEmpty(unit) ? salesDptMailBody.Replace("##UNIT_ELEM##", "") : salesDptMailBody.Replace("##UNIT_ELEM##", "<p style='color: white'>Unit: <span style='color:#f17077'>" + unit + "</span></p>");
                salesDptMailBody = salesDptMailBody.Replace("##DATE##", date);
                salesDptMailBody = salesDptMailBody.Replace("##SUBJECT##", subject);
                MailHelper.SendEmail(new ezdan_entities.Models.EmailNotification { EmailTo = salesDptEmail, Subject = "Appiontment Reservation Edit", Body = salesDptMailBody });
                result.Add("response", true);
                return result;
            }
            catch (Exception ex)
            {
                LogManager.Log(LogType.error, ex.Message, ex);
                result.Add("response", false);
                return result;
            }
        }
        #endregion

        #region Get user appointments 
        [Authorize]
        [HttpPost]
        [Route("api/Appointments/GetDiaryEvents")]
        public JObject GetDiaryEvents()
        {
            var u = UserId;
            var ApptListForDate = db.Appointments.AsNoTracking().Where(s=>s.UserId==UserId).OrderBy(s=>s.StartDate).ToList();
            JArray appointmentsArray = new JArray();
            foreach (var app in ApptListForDate)
            {
                JObject appointmentObject = new JObject();
                appointmentObject.Add("id", app.AppointmentId);
                appointmentObject.Add("title", app.Title);
                appointmentObject.Add("start", app.StartDate);
                appointmentObject.Add("end", app.EndDate);
                appointmentObject.Add("color", app.StatusColor);
                appointmentObject.Add("email", app.Email);
                appointmentObject.Add("mobile", app.Mobile);
                appointmentsArray.Add(appointmentObject);
            }
            List<int> weekEnds = getWeekEnds();
            List<string> businessHours = getBusinessHours();
            JArray weekEndsArray = new JArray();
            weekEndsArray.Add(weekEnds);
            JArray businessHoursArray = new JArray();
            businessHoursArray.Add(businessHours);
            JArray holidaysArray = getHolidays();
            JObject Calendar = new JObject();
            Calendar.Add("events", appointmentsArray);
            Calendar.Add("weekEnds", weekEndsArray);
            Calendar.Add("businessHours", businessHoursArray);
            Calendar.Add("holidays", holidaysArray);
            return Calendar;
        }
        #endregion

        #region Guests 
        [HttpPost]
        [Route("api/Appointments/GetGuestCalendar")]
        public JObject GetGuestCalendar()
        {
 
            List<int> weekEnds = getWeekEnds();
            List<string> businessHours = getBusinessHours();
            JArray weekEndsArray = new JArray();
            weekEndsArray.Add(weekEnds);
            JArray businessHoursArray = new JArray();
            businessHoursArray.Add(businessHours);
            JArray holidaysArray = getHolidays();
            JObject Calendar = new JObject();
            Calendar.Add("weekEnds", weekEndsArray);
            Calendar.Add("businessHours", businessHoursArray);
            Calendar.Add("holidays", holidaysArray);
            return Calendar;
        }

        [HttpPost]
        [Route("api/Appointments/SaveGuestAppointment")]
        public JObject SaveGuestAppointment(JObject obj)
        {
            string Subject = ""; string date = ""; string name = ""; string email = ""; string title = ""; string mobile = "";
            var Json = (JObject)JsonConvert.DeserializeObject(obj.ToString());
            Subject = Convert.ToString(Json["Subject"]);
            date = Convert.ToString(Json["date"]);
            name = Convert.ToString(Json["name"]);
            title = Convert.ToString(Json["title"]);
            email = Convert.ToString(Json["email"]);
            mobile = Convert.ToString(Json["mobile"]);
            Appointment app = new Appointment();
            app.StartDate = DateTime.Parse(date);
            app.EndDate = DateTime.Parse(date).AddHours(1);
            app.Title = title;
            app.Email = email;
            app.Mobile = mobile;
            db.Appointments.Add(app);
            JObject result = new JObject();
            try
            {
                db.SaveChanges();
                string filename = HttpContext.Current.Server.MapPath("~/Views/MailTemplates/test.html");
                string mailbody = System.IO.File.ReadAllText(filename);
                mailbody = mailbody.Replace("##NAME##", name);
                mailbody = mailbody.Replace("##title##", title);
                mailbody = mailbody.Replace("##DATE##", date);
                MailHelper.SendEmail(new ezdan_entities.Models.EmailNotification { EmailTo = email, Subject = Subject, Body = "Please wait for a confirmation email!" });
                //SmtpClient SmtpServer = new SmtpClient(ConfigurationManager.AppSettings["Host"], int.Parse(ConfigurationManager.AppSettings["Port"]));
                //SmtpServer.UseDefaultCredentials = false;
                //SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                //SmtpServer.EnableSsl = true;
                //SmtpServer.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["UserName"], ConfigurationManager.AppSettings["Password"]);
                //MailMessage Mail = new MailMessage();
                //Mail.From = new MailAddress(ConfigurationManager.AppSettings["UserName"]);
                //Mail.To.Add(email);
                //Mail.IsBodyHtml = true;     
                //Mail.Subject = Subject;
                //Mail.Body = mailbody;
                //SmtpServer.Send(Mail);
                result.Add("response", true);
                return result;
            }
            catch (Exception ex)
            {
                result.Add("response", false);
                return result;
            }
        }
        #endregion

    }
}

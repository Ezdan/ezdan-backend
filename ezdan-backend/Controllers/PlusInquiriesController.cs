﻿using System.Net;
using System.Net.Http;
using ezdan_entities;
using ezdan_business.Commands.PlusInquiry;
using ezdan_backend.ApiHelper;

namespace ezdan_backend.Controllers
{
    public class PlusInquiriesController : BaseController
    {
        public HttpResponseMessage  Post(PlusInquiry plusInquiry)
        {
            if (plusInquiry != null) plusInquiry.UserId = UserId;
            AddPlusInquiryCommand PlusinquiryCmd = new AddPlusInquiryCommand() { plusInquiry = plusInquiry };
            PlusinquiryCmd.Execute();
            return Request.CreateResponse(HttpStatusCode.OK, new { Added = PlusinquiryCmd.Added });

        }

    }
}
﻿using ezdan_backend.ApiHelper;
using ezdan_business.Commands.Feedback;
using ezdan_business.Commands.Service;
using ezdan_entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace ezdan_backend.Controllers
{
    public class FeedbackController : BaseController
    {
        public HttpResponseMessage Post(JObject obj)
        {
            AddFeedbackCommand addFeedbackCommand = new AddFeedbackCommand();
            var Json = (JObject)JsonConvert.DeserializeObject(obj.ToString());

            addFeedbackCommand.userId = UserId;

            addFeedbackCommand.title = Convert.ToString(Json["title"]);
            addFeedbackCommand.contactNumber = Convert.ToString(Json["contactNumber"]);
            addFeedbackCommand.message = Convert.ToString(Json["message"]);
            addFeedbackCommand.rate = Convert.ToInt32(Json["rating"]);
            addFeedbackCommand.type = Convert.ToByte(Json["type"]);
            if (Json["images"] != null)
            {
                addFeedbackCommand.imageUrl = Convert.ToString(((JArray)Json["images"])[0]);
            }

            addFeedbackCommand.Execute();

            return Request.CreateResponse(HttpStatusCode.OK, new { Added = addFeedbackCommand.IsAdded });
        }

        [HttpPost]
        [Route("api/feedback/uploadfeedbackimages")]
        public HttpResponseMessage uploadFeedbackImages()
        {
            AspNetUser currentUser = User.Identity.GetAspNetUser();
            HttpPostedFile file = HttpContext.Current.Request.Files["file"];
            string dir = HttpContext.Current.Request.Params["dir"];
            UploadImagesCommand uploadFeedbackImagesCmd = new UploadImagesCommand(file, dir, UserId);
            uploadFeedbackImagesCmd.Execute();
            if (uploadFeedbackImagesCmd.imageUploaded)
            {
                JObject result = uploadFeedbackImagesCmd.resp;
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            return Request.CreateResponse(HttpStatusCode.InternalServerError);
        }

    }
}

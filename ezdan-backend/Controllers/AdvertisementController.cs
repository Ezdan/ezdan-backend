﻿using ezdan_backend.ApiHelper;
using ezdan_business.Commands.Advertisement;
using ezdan_business.Helper;
using ezdan_entities;
using ezdan_entities.Helper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace ezdan_backend.Controllers
{
    [Authorize]
    [RoutePrefix("api/advertisement")]
    public class AdvertisementController : BaseController
    {

        /* Sample json request
            {
            "price": 200,
            "arabic_name":"الاسم العربي للاعلان",
            "english_name": "English name for the advertisement",
            "category_id": 22,
            "english_description": "Arabic and English descriptions as test when no arabic is entered"
            }
        */
        //phone and email missing, change english and arabic to 1 value
        [HttpPost]
        [Route("createadvertisement")]
        public HttpResponseMessage CreateAdvertisement(JObject requestParams)
        {
            var Json = (JObject)JsonConvert.DeserializeObject(requestParams.ToString());
            string price = Convert.ToString(Json["price"]);
            string itemName = Convert.ToString(Json["name"]);
            int categoryId = Convert.ToInt32(Json["category"]);
            string description = Convert.ToString(Json["description"]);
            string phone = Convert.ToString(Json["phone"]);
            string email = Convert.ToString(Json["email"]);

            bool reported = false;
            bool blocked = false;

            CreateNewAdvertisementCommand createNewAdvertisementCmd = new CreateNewAdvertisementCommand();
            createNewAdvertisementCmd.EnglishName = createNewAdvertisementCmd.ArabicName = itemName;
            createNewAdvertisementCmd.price = price;
            createNewAdvertisementCmd.categoryId = categoryId;
            createNewAdvertisementCmd.postedBy = UserId;
            createNewAdvertisementCmd.postedOn = DateTime.Now;
            createNewAdvertisementCmd.englishDescription = createNewAdvertisementCmd.arabicDescription = description;
            createNewAdvertisementCmd.ifSold = "False";
            createNewAdvertisementCmd.reported = reported;
            createNewAdvertisementCmd.blocked = blocked;
            createNewAdvertisementCmd.phone = phone;
            createNewAdvertisementCmd.email = email;

            createNewAdvertisementCmd.Execute();

            JObject result = createNewAdvertisementCmd.result;
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [HttpGet]
        [Route("getcategories")]
        public HttpResponseMessage GetCategories(string lang)
        {
            GetCategoriesListCommand getCategoriesListCmd = new GetCategoriesListCommand();
            getCategoriesListCmd.language = lang == "Arabic" ? AppLanguage.Arabic : AppLanguage.English;
            getCategoriesListCmd.Execute();
            JArray result = getCategoriesListCmd.result;

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        
        [HttpGet]
        [Route("getuseradvertisements")]
        public HttpResponseMessage GetUserAdvertisements(string lang)
        {
            GetUserAdvertisementsCommand getUserAdvertisementsCmd = new GetUserAdvertisementsCommand();
            getUserAdvertisementsCmd.language = lang == "Arabic" ? AppLanguage.Arabic : AppLanguage.English;
            getUserAdvertisementsCmd.UId = UserId;
            getUserAdvertisementsCmd.Execute();
            JArray result = getUserAdvertisementsCmd.result;

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [HttpGet]
        [Route("reportadvertisement")]
        public HttpResponseMessage ReportAdvertisement(int id)
        {
            ReportAdvertisementCommand reportAdvertisementCmd = new ReportAdvertisementCommand();
            reportAdvertisementCmd.advId = id;
            reportAdvertisementCmd.UId = UserId;
            reportAdvertisementCmd.Execute();
            JObject result = reportAdvertisementCmd.result;

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }


        [HttpPost]
        [Route("saveimage")]
        public HttpResponseMessage saveImage()
        {
            HttpPostedFile file = HttpContext.Current.Request.Files["file"];

            AddAdvertImageCommand addAdvertImageCmd = new AddAdvertImageCommand();
            addAdvertImageCmd.classifiedId = Convert.ToInt32(HttpContext.Current.Request.Params["classifiedId"]);
            addAdvertImageCmd.file = file;
            addAdvertImageCmd.filename = file.FileName;
            addAdvertImageCmd.foldername = "advertisements";
            addAdvertImageCmd.userId = UserId;
            addAdvertImageCmd.Execute();

            JObject result = addAdvertImageCmd.result;

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [HttpPost]
        [Route("editclassified")]
        public HttpResponseMessage editClassified(JObject classifiedModels)
        {

            var Json = (JObject)JsonConvert.DeserializeObject(classifiedModels.ToString());
            JavaScriptSerializer jss = new JavaScriptSerializer();
            EditClassifiedCommand editClassifiedCmd = new EditClassifiedCommand();

            Content content = jss.Deserialize<Content>(Json["content"].ToString());
            content.CreatedBy = UserId;
            editClassifiedCmd.content = content;
            editClassifiedCmd.contentProps = jss.Deserialize<List<ContentProperty>>(Json["contentProps"].ToString());
            editClassifiedCmd.deletedImages = jss.Deserialize<List<ContentProperty>>(Json["deletedImages"].ToString());

            editClassifiedCmd.Execute();

            JObject result = editClassifiedCmd.result;

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

    }
}

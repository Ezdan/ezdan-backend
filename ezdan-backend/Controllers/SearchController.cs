﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using ezdan_business.Commands.Search;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ezdan_backend.Controllers
{
    public class SearchController : ApiController
    {
        [HttpPost]
        [Route("api/search")]
        public HttpResponseMessage GetIt(JObject obj)
        {
            var Json = (JObject)JsonConvert.DeserializeObject(obj.ToString());
            string keyword = Json["keyword"].ToString();
            string userType = Json["userType"].ToString();

            SearchCommand SearchCommand = new SearchCommand() { Keyword = keyword, usertType = userType };
            SearchCommand.Execute();
            return Request.CreateResponse(HttpStatusCode.OK, new {
                Results = SearchCommand.Results,
                SearchedInEnglish = SearchCommand.SearchedInEnglish
            });

        }
    }
}

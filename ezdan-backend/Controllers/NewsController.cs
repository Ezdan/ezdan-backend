﻿using ezdan_backend.ApiHelper;
using ezdan_business.Commands.News;
using ezdan_entities.Helper;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ezdan_backend.Controllers
{
    [RoutePrefix("api/news")]
    public class NewsController : BaseController
    {
        [HttpGet]
        [Route("getnews")]
        public HttpResponseMessage GetNews(string lang, int id)
        {
            GetNewsCommand getNewsCommand = new GetNewsCommand();
            getNewsCommand.lang = lang == "Arabic" ? (int)AppLanguage.Arabic : (int)AppLanguage.English;
            getNewsCommand.id = id;
            getNewsCommand.Execute();
            JObject result = getNewsCommand.allNewsResult;
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
    }
}

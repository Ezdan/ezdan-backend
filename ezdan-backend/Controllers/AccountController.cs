﻿using ezdan_backend.ApiHelper;
using ezdan_backend.Models;
using ezdan_backend.Providers;
using ezdan_backend.Results;
using ezdan_business.Commands;
using ezdan_business.Commands.User;
using ezdan_business.Helper;
using ezdan_entities;
using ezdan_entities.Helper;
using ezdan_entities.Models;
using ezdan_logger;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Linq;

namespace ezdan_backend.Controllers
{
    [Authorize]
    [RoutePrefix("api/Account")]
    public class AccountController : BaseController
    {
        private const string LocalLoginProvider = "Local";
        private ApplicationUserManager _userManager;
        private Entities db = new Entities();

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }

        // GET api/Account/UserInfo
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("userinfo")]
        public UserInfoViewModel GetUserInfo()
        {
            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);
            return new UserInfoViewModel
            {
                ImageUrl = User.Identity.GetAspNetUser().ImageURL != null ? User.Identity.GetAspNetUser().ImageURL : null,
                Name = User.Identity.GetAspNetUser().FullName,
                Email = User.Identity.GetAspNetUser().Email,
                Mobile = User.Identity.GetAspNetUser().PhoneNumber,
                HasRegistered = externalLogin == null,
                LoginProvider = externalLogin != null ? externalLogin.LoginProvider : null
            };
        }

        [HttpPost]
        [Route("saveimage")]
        public HttpResponseMessage saveImage()
        {
            HttpPostedFile file = HttpContext.Current.Request.Files["file"];

            AddUserProfileImageCommand addUserProfileImageCmd = new AddUserProfileImageCommand();
            addUserProfileImageCmd.file = file;
            addUserProfileImageCmd.filename = file.FileName;
            addUserProfileImageCmd.foldername = "profiles";
            addUserProfileImageCmd.userId = UserId;
            addUserProfileImageCmd.Execute();

            JObject result = addUserProfileImageCmd.result;

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }


        [HttpPost]
        [Route("editprofile")]
        public HttpResponseMessage editProfile(JObject profileModel)
        {
            var Json = (JObject)JsonConvert.DeserializeObject(profileModel.ToString());
            string email = Convert.ToString(Json["email"]);
            string mobile = Convert.ToString(Json["mobile"]);
            if (IsExistEmail(UserId, email))
            {
                return Request.CreateResponse(HttpStatusCode.Conflict);
            }
            UpdateUserProfileCommand updateUserProfileCmd = new UpdateUserProfileCommand() { userModel = profileModel, UserId = UserId };

            updateUserProfileCmd.Execute();

            var result = updateUserProfileCmd.UpdateStatus;

            return Request.CreateResponse(HttpStatusCode.OK, new { Updated = result, UpdatedInfo = updateUserProfileCmd.UpdatedInfo });
        }

        // POST api/Account/Logout
        [Route("Logout")]
        public IHttpActionResult Logout()
        {
            Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
            return Ok();
        }

        // GET api/Account/ManageInfo?returnUrl=%2F&generateState=true
        [Route("ManageInfo")]
        public async Task<ManageInfoViewModel> GetManageInfo(string returnUrl, bool generateState = false)
        {
            IdentityUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            if (user == null)
            {
                return null;
            }

            List<UserLoginInfoViewModel> logins = new List<UserLoginInfoViewModel>();

            foreach (IdentityUserLogin linkedAccount in user.Logins)
            {
                logins.Add(new UserLoginInfoViewModel
                {
                    LoginProvider = linkedAccount.LoginProvider,
                    ProviderKey = linkedAccount.ProviderKey
                });
            }

            if (user.PasswordHash != null)
            {
                logins.Add(new UserLoginInfoViewModel
                {
                    LoginProvider = LocalLoginProvider,
                    ProviderKey = user.UserName,
                });
            }

            return new ManageInfoViewModel
            {
                LocalLoginProvider = LocalLoginProvider,
                Email = user.UserName,
                Logins = logins,
                ExternalLoginProviders = GetExternalLogins(returnUrl, generateState)
            };
        }

        // POST api/Account/ChangePassword
        [Route("ChangePassword")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword,
                model.NewPassword);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        [Route("FindAccount")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> FindAccount([FromBody] string userEmail)
        {
            ApplicationUser user = await UserManager.FindByEmailAsync(userEmail);
            if (user != null)
                return Ok(new { found = true });
            return Ok(new { found = false });
        }

        // POST api/Account/SetPassword
        [Route("SetPassword")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> SetPassword(SetPasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var userId = UserManager.FindByEmail(model.Email).Id;
            IdentityResult removeResult = UserManager.RemovePassword(userId);

            if (!removeResult.Succeeded)
            {
                return GetErrorResult(removeResult);
            }
            IdentityResult result = UserManager.AddPassword(userId, model.NewPassword);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok(new { updated = "true" });
        }

        // POST api/Account/AddExternalLogin
        [Route("AddExternalLogin")]
        public async Task<IHttpActionResult> AddExternalLogin
            (AddExternalLoginBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);

            AuthenticationTicket ticket = AccessTokenFormat.Unprotect(model.ExternalAccessToken);

            if (ticket == null || ticket.Identity == null || (ticket.Properties != null
                && ticket.Properties.ExpiresUtc.HasValue
                && ticket.Properties.ExpiresUtc.Value < DateTimeOffset.UtcNow))
            {
                return BadRequest("External login failure.");
            }

            ExternalLoginData externalData = ExternalLoginData.FromIdentity(ticket.Identity);

            if (externalData == null)
            {
                return BadRequest("The external login is already associated with an account.");
            }

            IdentityResult result = await UserManager.AddLoginAsync(User.Identity.GetUserId(),
                new UserLoginInfo(externalData.LoginProvider, externalData.ProviderKey));

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // POST api/Account/RemoveLogin
        [Route("RemoveLogin")]
        public async Task<IHttpActionResult> RemoveLogin(RemoveLoginBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result;

            if (model.LoginProvider == LocalLoginProvider)
            {
                result = await UserManager.RemovePasswordAsync(User.Identity.GetUserId());
            }
            else
            {
                result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(),
                    new UserLoginInfo(model.LoginProvider, model.ProviderKey));
            }

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // GET api/Account/ExternalLogin
        [OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalCookie)]
        [AllowAnonymous]
        [Route("ExternalLogin", Name = "ExternalLogin")]
        public async Task<IHttpActionResult> GetExternalLogin(string provider, string error = null)
        {
            if (error != null)
            {
                return Redirect(Url.Content("~/") + "#error=" + Uri.EscapeDataString(error));
            }

            if (!User.Identity.IsAuthenticated)
            {
                return new ChallengeResult(provider, this);
            }

            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

            if (externalLogin == null)
            {
                return InternalServerError();
            }

            if (externalLogin.LoginProvider != provider)
            {
                Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);
                return new ChallengeResult(provider, this);
            }

            ApplicationUser user = await UserManager.FindAsync(new UserLoginInfo(externalLogin.LoginProvider,
                externalLogin.ProviderKey));

            bool hasRegistered = user != null;

            if (hasRegistered)
            {
                Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);

                ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(UserManager,
                   OAuthDefaults.AuthenticationType);
                ClaimsIdentity cookieIdentity = await user.GenerateUserIdentityAsync(UserManager,
                    CookieAuthenticationDefaults.AuthenticationType);

                AuthenticationProperties properties = ApplicationOAuthProvider.CreateProperties(new Dictionary<string, string>() { { "UserName", user.UserName } });
                Authentication.SignIn(properties, oAuthIdentity, cookieIdentity);
            }
            else
            {
                IEnumerable<Claim> claims = externalLogin.GetClaims();
                ClaimsIdentity identity = new ClaimsIdentity(claims, OAuthDefaults.AuthenticationType);
                Authentication.SignIn(identity);
            }

            return Ok();
        }

        // GET api/Account/ExternalLogins?returnUrl=%2F&generateState=true
        [AllowAnonymous]
        [Route("ExternalLogins")]
        public IEnumerable<ExternalLoginViewModel> GetExternalLogins(string returnUrl, bool generateState = false)
        {
            IEnumerable<AuthenticationDescription> descriptions = Authentication.GetExternalAuthenticationTypes();
            List<ExternalLoginViewModel> logins = new List<ExternalLoginViewModel>();

            string state;

            if (generateState)
            {
                const int strengthInBits = 256;
                state = RandomOAuthStateGenerator.Generate(strengthInBits);
            }
            else
            {
                state = null;
            }

            foreach (AuthenticationDescription description in descriptions)
            {
                ExternalLoginViewModel login = new ExternalLoginViewModel
                {
                    Name = description.Caption,
                    Url = Url.Route("ExternalLogin", new
                    {
                        provider = description.AuthenticationType,
                        response_type = "token",
                        client_id = Startup.PublicClientId,
                        redirect_uri = new Uri(Request.RequestUri, returnUrl).AbsoluteUri,
                        state = state
                    }),
                    State = state
                };
                logins.Add(login);
            }

            return logins;
        }

        // POST api/Account/Register
        [AllowAnonymous]
        [Route("Register")]
        public async Task<IHttpActionResult> Register(RegisterBindingModel model)
        {
            LogManager.Log(LogType.trace, model.ToString());
            if (!ModelState.IsValid)
            {
                LogManager.Log(LogType.trace, string.Format("INVALID model with email {0}", model.Email));
                return BadRequest(ModelState);
            }
            var user = new ApplicationUser()
            {
                UserName = model.Email,
                Email = model.Email,
                ContractNumber = model.ContractNumber,
                Address = string.IsNullOrEmpty(model.Location) ? null : model.Location,
                PhoneNumber = model.PhoneNumber,
                ImageUrl = null,
                FullName = model.FullName,
                Title = model.Title,
                UnitId = string.IsNullOrEmpty(model.Unit) ? null : model.Unit,
                AccountId = string.IsNullOrEmpty(model.Account) ? null : model.Account,
                PartyRecId = model.PartyRecId == null ? null : model.PartyRecId,
                UserLanguage = model.Language == "English" ? Convert.ToInt32(AppLanguage.English) : Convert.ToInt32(AppLanguage.Arabic),
                EnableNotification = true,
                RegisteredOn = DateTime.Now
            };
            try
            {

                IdentityResult result = await UserManager.CreateAsync(user, model.Password);
                if (result == null)
                {
                    LogManager.Log(LogType.error, "Result is null from If Condition");
                    return GetErrorResult(result);
                }
                if (!result.Succeeded)
                {
                    return GetErrorResult(result);

                }
                LogManager.Log(LogType.trace, string.Format("SUCCESSED model with email {0}", model.Email));
                return Ok(GetToken(model.Email));
            }
            catch (Exception ex)
            {
                LogManager.Log(LogType.error, "Create User " + ex.Message, ex);
                return BadRequest();
            }


        }

        [AllowAnonymous]
        [HttpPost]
        [Route("authorizephone")]
        public HttpResponseMessage validatePhoneNumber(phoneAuthBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new {
                    error = ModelState.Values.Select(msvs => msvs.Errors[0]).First().ErrorMessage
                });
            }
            if (IsExistEmail(null, model.Email))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "duplicateEmail" });
            }
            if (phoneExists(null, model.PhoneNumber))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "duplicatePhone" });
            }
            if (IsExistContractNumber(model.ContractNumber))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "duplicateContract" });
            }
            GetAxUserCommand userCmd = new GetAxUserCommand() { ContractNumber = model.ContractNumber, PhoneNumber = model.PhoneNumber };
            userCmd.Execute();
            if (userCmd.IsValid == null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "AXDisconnection" });
            }
            if (userCmd.IsValid.HasValue && !userCmd.IsValid.Value)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "invalidContract" });
            }
            if ((bool)userCmd.InvalidPhone)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = "phoneMismatch" });
            }
            var phoneNum = new string(model.PhoneNumber.Where(c => char.IsDigit(c)).ToArray());
            GenerateResetPasswordCodeCommand phoneAuthCodeCommand = new GenerateResetPasswordCodeCommand();
            phoneAuthCodeCommand.UserId = null;
            phoneAuthCodeCommand.phoneAuth = true;
            phoneAuthCodeCommand.Execute();
            phoneNum = new string(phoneNum.Where(c => char.IsDigit(c)).ToArray());
            string SMSResponse = SMSHelper.SendSMS(phoneNum, "Registration verification code:\n\n" + phoneAuthCodeCommand.phoneAuthCode);
            if (SMSResponse == null || string.IsNullOrEmpty(SMSResponse))
            {
                db.ResetVerifications.Remove(db.ResetVerifications.Find(phoneAuthCodeCommand.addedEntryId));
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { error = "SMSError" });
            }
            return Request.CreateResponse(HttpStatusCode.OK, new
            {
                phoneAuthId = phoneAuthCodeCommand.addedEntryId,
                AXUser = userCmd.axUser
            });
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("cancelphoneauth")]
        public HttpResponseMessage cancelPhoneValidation(JObject phoneAuthIdObj)
        {
            int phoneAuthId;
            var Json = (JObject)JsonConvert.DeserializeObject(phoneAuthIdObj.ToString());
            try
            {
                phoneAuthId = (int)Convert.ToDecimal(Json["id"]);
            }
            catch (Exception ex)
            {
                LogManager.Log(LogType.error, ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Phone number is missing.");
            }
            db.ResetVerifications.Remove(db.ResetVerifications.Find(phoneAuthId));
            try
            {
                db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                LogManager.Log(LogType.error, ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "An error has occured");
            }
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("resendphoneauth")]
        public HttpResponseMessage resendPhoneValidation(JObject phoneAuthIdObj)
        {
            int phoneAuthId;
            string phoneNum;
            var Json = (JObject)JsonConvert.DeserializeObject(phoneAuthIdObj.ToString());
            try
            {
                phoneAuthId = (int)Convert.ToDecimal(Json["id"]);
                phoneNum = Convert.ToString(Json["phoneNumber"]);
            }
            catch (Exception ex)
            {
                LogManager.Log(LogType.error, ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
            db.ResetVerifications.Remove(db.ResetVerifications.Find(phoneAuthId));
            GenerateResetPasswordCodeCommand phoneAuthCodeCommand = new GenerateResetPasswordCodeCommand();
            phoneAuthCodeCommand.UserId = null;
            phoneAuthCodeCommand.phoneAuth = true;
            phoneAuthCodeCommand.Execute();
            phoneNum = new string(phoneNum.Where(c => char.IsDigit(c)).ToArray());
            string SMSResponse = SMSHelper.SendSMS(phoneNum, "Registration verification code:\n\n" + phoneAuthCodeCommand.phoneAuthCode);
            if (SMSResponse == null || string.IsNullOrEmpty(SMSResponse))
            {
                db.ResetVerifications.Remove(db.ResetVerifications.Find(phoneAuthCodeCommand.addedEntryId));
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { error = "SMSError" });
            }
            try
            {
                db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, new { phoneAuthId = phoneAuthCodeCommand.addedEntryId });
            }
            catch (Exception ex)
            {
                LogManager.Log(LogType.error, ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "An error has occured");
            }
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("submitphoneauth")]
        public HttpResponseMessage submitPhoneValidation(JObject phoneAuthObj)
        {
            int phoneAuthId;
            string phoneAuthCode;
            LogManager.Log(LogType.trace, phoneAuthObj.ToString());
            var Json = (JObject)JsonConvert.DeserializeObject(phoneAuthObj.ToString());
            try
            {
                phoneAuthId = (int)Convert.ToDecimal(Json["id"]);
                phoneAuthCode = Convert.ToString(Json["code"]);
                LogManager.Log(LogType.trace, 
                    string.Format("phoneAuthId :{0}, phoneAuthCode:{1}", phoneAuthId, phoneAuthCode));
            }
            catch (Exception ex)
            {
                LogManager.Log(LogType.error, ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Phone auth is missing parameters.");
            }
            ResetVerification phoneAuthCodeRecord = db.ResetVerifications.Find(phoneAuthId);
            if (phoneAuthCodeRecord == null || phoneAuthCodeRecord.Code != phoneAuthCode)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { message = "mismatch" });
            }
            try
            {
                phoneAuthCodeRecord.IsUsed = true;
                db.Entry(phoneAuthCodeRecord).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, new { message = "success" });
            }
            catch (Exception ex)
            {
                LogManager.Log(LogType.error, ex.Message, ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "An error has occured");
            }
        }

        [Authorize]
        [Route("setting")]
        [ResponseType(typeof(UserSetting))]
        public HttpResponseMessage GetUserSetting()
        {
            GetUserSettingCommand userSettingCmd = new GetUserSettingCommand(UserId);
            userSettingCmd.Execute();
            var UserSettings = userSettingCmd.UserSetting;

            return Request.CreateResponse(HttpStatusCode.OK, UserSettings);
        }


        [AllowAnonymous]
        [HttpPost]
        [Route("generateResetPasswordCode")]
        public HttpResponseMessage GenerateResetPasswordCode(RegisterBindingModel model)
        {
            ApplicationUser user = UserManager.FindByEmail(model.Email);
            if (user != null)
            {
                GenerateResetPasswordCodeCommand generateResetPasswordCodeCommand = new GenerateResetPasswordCodeCommand() { UserId = user.Id, Email = user.Email };
                generateResetPasswordCodeCommand.Execute();
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("verifyResetPasswordCode")]
        public HttpResponseMessage VerifyResetPasswordCode(VerifyResetPasswordCodeModel model)
        {
            ApplicationUser user = UserManager.FindByEmail(model.Email);
            if (user != null)
            {
                VerifyResetPasswordCodeCommand verifyResetPasswordCodeCommand = new VerifyResetPasswordCodeCommand() { UserId = user.Id, Email = user.Email, VerificationCode = model.Code };
                verifyResetPasswordCodeCommand.Execute();
                if (verifyResetPasswordCodeCommand.codeMatched)
                    return Request.CreateResponse(HttpStatusCode.OK);
            }
            return Request.CreateResponse(HttpStatusCode.NotFound);
        }

        [Authorize]
        [Route("updatesetting")]
        public HttpResponseMessage UpdateSetting(JObject updatedvalue)
        {
            UpdateUserSettingCommand updateCmd = new UpdateUserSettingCommand() { UpdatedValue = updatedvalue, UserId = UserId };
            updateCmd.Execute();
            var updateResult = updateCmd.UpdateStatus;
            return Request.CreateResponse(HttpStatusCode.OK, new { Updated = updateResult });
        }

        [Authorize]
        [HttpGet]
        [Route("validatecontract")]
        public HttpResponseMessage ValidateContract()
        {
            AspNetUser currentUser = User.Identity.GetAspNetUser();

            GetAxUserCommand axUserCommand = new GetAxUserCommand
            { ContractNumber = currentUser.ContractNumber, AccountNumber = currentUser.AccountId, isLogin = true };
            axUserCommand.Execute();
            if ((axUserCommand.IsValid.HasValue && axUserCommand.IsValid.Value) || !Convert.ToBoolean(SettingHelper.GetAppSettingValue("IsEnableIntegration")))
            {
                JObject response = new JObject();

                JObject userObj = new JObject();
                userObj.Add("Language", currentUser.UserLanguage == 1 ? "English" : "Arabic");
                userObj.Add("Notification", currentUser.EnableNotification);
                userObj.Add("FullName", currentUser.FullName);
                userObj.Add("Title", currentUser.Title);
                userObj.Add("PhoneNumber", currentUser.PhoneNumber);
                userObj.Add("ImageUrl", currentUser.ImageURL != null ? currentUser.ImageURL : null);
                userObj.Add("Email", currentUser.Email);

                response.Add("user", userObj);
                response.Add("valid", axUserCommand.IsValid.Value);

                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            else if (!axUserCommand.IsValid.HasValue)
            {
                JObject error = new JObject();
                error.Add("error", "Ax Exception");
                error.Add("description", "Error retrieving data from AX");
                return Request.CreateResponse(HttpStatusCode.OK, error);
            }
            else
            {
                JObject error = new JObject();
                error.Add("error", "ezdna-error");
                error.Add("description", "the contract number is expired");
                return Request.CreateResponse(HttpStatusCode.OK, error);
            }
        }

        [AllowAnonymous]
        public bool IsExistContractNumber(string contractNumber)
        {
            ValidContractNumberCommand validContractNumberCmd = new ValidContractNumberCommand() { ContractNumber = contractNumber };
            validContractNumberCmd.Execute();
            return validContractNumberCmd.IsExist;
        }

        [AllowAnonymous]
        public bool IsExistEmail(string id, string email)
        {
            ValidEmailCommand validEmailCmd = new ValidEmailCommand() { Id = id, Email = email };
            validEmailCmd.Execute();
            return validEmailCmd.IsExist;
        }

        public bool phoneExists(string id, string phoneNum)
        {
            return db.AspNetUsers.Any(u => u.PhoneNumber == phoneNum && u.Id != id);
        }

        // POST api/Account/RegisterExternal
        [OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("RegisterExternal")]
        public async Task<IHttpActionResult> RegisterExternal(RegisterExternalBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var info = await Authentication.GetExternalLoginInfoAsync();
            if (info == null)
            {
                return InternalServerError();
            }

            var user = new ApplicationUser() { UserName = model.Email, Email = model.Email };

            IdentityResult result = await UserManager.CreateAsync(user);
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            result = await UserManager.AddLoginAsync(user.Id, info.Login);
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }
            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }

        #region Helpers

        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                LogManager.Log(LogType.trace, "NULL RESEULT");
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        LogManager.Log(LogType.trace, " User Model Error : " + error);
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        private class ExternalLoginData
        {
            public string LoginProvider { get; set; }
            public string ProviderKey { get; set; }
            public string UserName { get; set; }

            public IList<Claim> GetClaims()
            {
                IList<Claim> claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.NameIdentifier, ProviderKey, null, LoginProvider));

                if (UserName != null)
                {
                    claims.Add(new Claim(ClaimTypes.Name, UserName, null, LoginProvider));
                }

                return claims;
            }

            public static ExternalLoginData FromIdentity(ClaimsIdentity identity)
            {
                if (identity == null)
                {
                    return null;
                }

                Claim providerKeyClaim = identity.FindFirst(ClaimTypes.NameIdentifier);

                if (providerKeyClaim == null || String.IsNullOrEmpty(providerKeyClaim.Issuer)
                    || String.IsNullOrEmpty(providerKeyClaim.Value))
                {
                    return null;
                }

                if (providerKeyClaim.Issuer == ClaimsIdentity.DefaultIssuer)
                {
                    return null;
                }

                return new ExternalLoginData
                {
                    LoginProvider = providerKeyClaim.Issuer,
                    ProviderKey = providerKeyClaim.Value,
                    UserName = identity.FindFirstValue(ClaimTypes.Name)
                };
            }
        }

        private static class RandomOAuthStateGenerator
        {
            private static RandomNumberGenerator _random = new RNGCryptoServiceProvider();

            public static string Generate(int strengthInBits)
            {
                const int bitsPerByte = 8;

                if (strengthInBits % bitsPerByte != 0)
                {
                    throw new ArgumentException("strengthInBits must be evenly divisible by 8.", "strengthInBits");
                }

                int strengthInBytes = strengthInBits / bitsPerByte;

                byte[] data = new byte[strengthInBytes];
                _random.GetBytes(data);
                return HttpServerUtility.UrlTokenEncode(data);
            }
        }

        #endregion Helpers
    }
}
﻿using ezdan_logger;
using System.Web.Http.ExceptionHandling;

namespace ezdan_backend.ApiHelper
{
    public class APIExceptionLogger :ExceptionLogger
    {
        public override void Log(ExceptionLoggerContext context)
        {
            LogManager.Log(LogType.error, "unhandled exception",
                context.ExceptionContext.Exception);
        }
    }
}
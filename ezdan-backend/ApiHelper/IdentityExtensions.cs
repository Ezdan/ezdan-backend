﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using Microsoft.AspNet.Identity;
using ezdan_entities;

namespace ezdan_backend.ApiHelper
{
    public static class IdentityExtensions
    {
        public static AspNetUser GetAspNetUser(this IIdentity identity)
        {
            var userId = identity.GetUserId();
            using (var context = new ezdan_entities.Entities())
            {
                var user = context.AspNetUsers.FirstOrDefault(u => u.Id == userId);
                return user;
            }
        }
    }
}
﻿using System.Web.Http;
using Microsoft.AspNet.Identity;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using System.Security.Claims;
using Microsoft.Owin.Security;
using System;
using System.Net.Http;
using ezdan_backend.Models;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using System.Collections.Generic;
using ezdan_logger;

namespace ezdan_backend.ApiHelper
{
    public class BaseController : ApiController
    {
        public string UserId
        {
            get
            {
                return User.Identity.GetUserId();
            }
        }
        public int GetStartValue(int? pageNumber, int PageSize)
        {
            int start = pageNumber.HasValue ? PageSize * (pageNumber.Value - 1) : 0;
            start = start < 0 ? 0 : start;
            return start;
        }
        public JObject GetToken(string Email)
        {
            try
            {
                LogManager.Log(LogType.trace, "Getting Token for Email : " + Email);

                var userManager = Request.GetOwinContext().GetUserManager<ApplicationUserManager>();

                ApplicationUser user = userManager.FindByEmail(Email);
                ClaimsIdentity oAuthIdentity = new ClaimsIdentity(Startup.OAuthOptions.AuthenticationType);

                oAuthIdentity.AddClaim(new Claim(ClaimTypes.Name, user.Email));
                oAuthIdentity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.Id));

                AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, new AuthenticationProperties());

                DateTime currentUtc = DateTime.UtcNow;
                ticket.Properties.IssuedUtc = currentUtc;
                ticket.Properties.ExpiresUtc = currentUtc.Add(TimeSpan.FromDays(14));

                string accessToken = Startup.OAuthOptions.AccessTokenFormat.Protect(ticket);
                Request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", accessToken);


                JObject token = new JObject(
                        new JProperty("Language", user.UserLanguage == 1 ? "English" : "Arabic"),
                        new JProperty("Notification", user.EnableNotification.ToString()),
                        new JProperty("UserName", user.UserName),
                        new JProperty("FullName", user.FullName),
                        new JProperty("Title", user.Title),
                        new JProperty("PhoneNumber", user.PhoneNumber),
                        new JProperty("Location", user.Address),
                        new JProperty("UnitId", user.UnitId),
                        new JProperty("ImageUrl", user.ImageUrl != null ? user.ImageUrl : null),
                        new JProperty("access_token", accessToken),
                        new JProperty("token_type", "bearer"),
                        new JProperty("expires_in", TimeSpan.FromDays(365).TotalSeconds.ToString()),
                        new JProperty(".issued", currentUtc.ToString("ddd, dd MMM yyyy HH':'mm':'ss 'GMT'")),
                        new JProperty(".expires", currentUtc.Add(TimeSpan.FromDays(365)).ToString("ddd, dd MMM yyyy HH:mm:ss 'GMT'"))
                    //new JProperty("oo", user.)
                    );

                return token;
            }
            catch (Exception ex)
            {
                LogManager.Log(LogType.error, "Getting Token : " + ex.Message, ex);
                return null;
            }

        }
    }
}
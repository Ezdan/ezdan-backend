﻿using System.Collections.Generic;

namespace ezdan_entities.Models
{
    public class Property
    {
        public string _DocumnetHash { get; set; }
        public string PropertyGroup { get; set; }
        public string PropertyBuilding { get; set; }
        public string PropertyUnit { get; set; }
        public string Furnished { get; set; }
        public string PropertyType { get; set; }
        public string Location { get; set; }
        
        public string thumb { get; set; }
        public string Price { get; set; }
        public string PriceFor { get; set; }
        public int NumberOfBedrooms { get; set; }
        public string City { get; set; }
        public string MarketingName { get; set; }
        public List<string> PropertyImages { get; set; }
    }
}

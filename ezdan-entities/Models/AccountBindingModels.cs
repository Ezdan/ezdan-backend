﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ezdan_entities.Models
{
    // Models used as parameters to AccountController actions.

    public class AddExternalLoginBindingModel
    {
        [Required]
        [Display(Name = "External access token")]
        public string ExternalAccessToken { get; set; }
    }

    public class ChangePasswordBindingModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [RegularExpression(@"^(((?=.*[a-z])(?=.*[A-Z])(?=.*\d)).+$)", ErrorMessage = "passwordStrength")]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class RegisterBindingModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [RegularExpression(@"^(((?=.*[a-z])(?=.*[A-Z])(?=.*\d)).+$)", ErrorMessage = "passwordStrength")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
        [Required]
        public string Location { get; set; }
        [Required]
        public string Unit { get; set; }
        [Required]
        public string Account { get; set; }
        [Required]
        public Nullable<long> PartyRecId { get; set; }
        [Required]
        public string ContractNumber { get; set; }
        [Required]
        public string FullName { get; set; }
        [Required]
        public string Language { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        public override string ToString()
        {
            return string.Format("email :{0}, full name:{1}, PhoneNumber:{2}, contract number:{3}, unitId:{4}, accountId:{5}",
                Email, FullName, PhoneNumber,ContractNumber,Unit,Account);
        }
    }

    public class phoneAuthBindingModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "passwordStrength", MinimumLength = 6)]
        [RegularExpression(@"^(((?=.*[a-z])(?=.*[A-Z])(?=.*\d)).+$)", ErrorMessage = "passwordStrength")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
        //[Required]
        //public string Address { get; set; }
        [Required]
        public string ContractNumber { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
    }

    public class RegisterExternalBindingModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class RemoveLoginBindingModel
    {
        [Required]
        [Display(Name = "Login provider")]
        public string LoginProvider { get; set; }

        [Required]
        [Display(Name = "Provider key")]
        public string ProviderKey { get; set; }
    }

    public class SetPasswordBindingModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [RegularExpression(@"^(((?=.*[a-z])(?=.*[A-Z])(?=.*\d)).+$)", ErrorMessage = "passwordStrength")]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
        [Required]
        public string Email { get; set; }
    }

    public class VerifyResetPasswordCodeModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
    }
}

﻿using ezdan_entities.Helper;

namespace ezdan_entities.Models
{
    public class UserSetting
    {
        public AppLanguage UserLanguage { get; set; }
        public bool IsEnableNotification { get; set; }
    }
}

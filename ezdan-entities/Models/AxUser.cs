﻿using System;

namespace ezdan_entities.Models
{
    public class AxUser
    {
        public string AccountNumber { get; set; }
        public string ContractNumber { get; set; }
        public string PhoneNumber { get; set; }
        public long? PartyRecId { get; set; }
        public string City { get; set; }
        public string  UnitId { get; set; }
    }
}

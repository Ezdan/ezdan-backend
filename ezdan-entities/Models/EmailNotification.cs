﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ezdan_entities.Models
{
    public class EmailNotification
    {
        public string EmailTo { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public EmailConfiguration Configuration { get; set; }
    }
    public class EmailConfiguration
    {
        public string Host { get; set; }
        public string SMTPEmail { get; set; }
        public string SMTPPassword { get; set; }
        public bool EnableSSL { get; set; }
        public int Port { get; set; }
        public string MailSender { get; set; }
    }
}

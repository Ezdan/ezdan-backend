﻿using System.Data.Entity;

namespace ezdan_entities.Helper
{
    public enum AppLanguage
    {
        English = 1,
        Arabic = 2
    }
}
